/**
 * Creates Bash arrays intended to be pasted into mk-audio.command
 * for the creation of MP3 files. To use, paste audio asset nodes into
 * the data array then select "Create bash audio" from the menu.
 **/

import React, { Component } from "react";

import { audioNodeToBashArray } from "../../util/toBash";

let data = [
  {
    key: "my_friend_honest_tells_truth_what",
    path: "my_friend_honest_tells_truth_what.mp3",
    transcript:
      "My friend is honest and tells the truth. What word describes my friend?"
  },
  {
    key: "your_friend_honest_tells_truth_what",
    path: "your_friend_honest_tells_truth_what.mp3",
    transcript:
      "Your friend is honest and tells the truth. What word describes my friend?"
  }
];

export default class CreateAudioNodeArrays extends Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
        <div style={{ width: "350px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Filenames Array</h3>
          <textarea
            id="outputData"
            rows="30"
            style={{ width: "100%" }}
            defaultValue={audioNodeToBashArray(data, "key", "filenames")}
          />
        </div>
        <div style={{ width: "350px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Transcripts Array</h3>
          <textarea
            id="outputConfig"
            rows="30"
            style={{ width: "100%" }}
            defaultValue={audioNodeToBashArray(
              data,
              "transcript",
              "transcripts"
            )}
          />
        </div>
      </div>
    );
  }
}
