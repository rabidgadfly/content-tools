import React, { Component } from "react";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../util/filenameGenerator";
import { audioNodeToBashArrays } from "../../util/toBash";

const audioArr = [
  "Eloise Greenfield was among millions of African Americans who were part of the Great Migration - the journey from the southern United States for better opportunities in northern cities. Her poem about this experience is divided into sections, each narrated by a different speaker with a different point of view."
];

const processTranscripts = trans =>
  trans.reduce((acc, tr) => {
    return [...acc, getAudioNode(tr)];
  }, []);

const getAudioNode = tr => {
  let fileName = filenameGenerator(tr);
  return {
    key: fileName,
    path: fileName + ".mp3",
    transcript: tr
  };
};

export default class CreateAudioNodes extends Component {
  render() {
    const nodes = processTranscripts(audioArr);
    const bashArrays = audioNodeToBashArrays(nodes);

    return (
      <div style={{ display: "flex" }}>
        <div style={{ width: "350px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Nodes</h3>
          <textarea
            id="output"
            rows="50"
            style={{ width: "100%" }}
            defaultValue={js_beautify(
              JSON.stringify(processTranscripts(audioArr))
            )}
          />
        </div>
        <div style={{ width: "350px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Bash arrays</h3>
          <textarea
            id="output"
            rows="50"
            style={{ width: "100%" }}
            defaultValue={bashArrays}
          />
        </div>
      </div>
    );
  }
}
