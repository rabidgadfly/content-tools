import React, { Component } from "react";
import js_beautify from "js-beautify";

let assetData = [];

const getResult = () => {
  let newAssetData = assetData.reduce((acc, node) => {
    let match = acc.findIndex(n => n.key === node.key);
    return match === -1 ? [...acc, node] : acc;
  }, []);

  return newAssetData.sort((a, b) => {
    let x = a.key.toLowerCase();
    let y = b.key.toLowerCase();
    if (x < y) {
      return -1;
    }
    if (x > y) {
      return 1;
    }
    return 0;
  });
};

export default class AlphaAudioNodes extends Component {
  componentDidMount = () => {
    this.setState({ result: getResult() });
  };

  render() {
    if (this.state && this.state.result) {
      return (
        <div>
          <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
          <textarea
            id="output"
            rows="50"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(this.state.result))}
          />
        </div>
      );
    }
    return <div />;
  }
}
