import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import {filenameGenerator} from "../../../util/filenameGenerator";

const spotlightHint = [
"Ryan is doing a math problem about a dog who eats pie. He has to take one number away from another. What is this math process called?",
"Ryan is doing a maths problem about a dog who eats pie. He has to take one number away from another. What is this maths process called?",
"Every country has a flag that is a symbol of that country. What word describes a country's very own flag?",
"Every country has a flag that is a symbol of that country. What word describes a country's very own flag?",
"Rowan asks his grandmother, \"Why do I have to go to school?\" His grandmother replies, \"People go to school to learn. Everyone needs an ___.\"",
"Rowan asks his grandmother, \"Why do I have to go to school?\" His grandmother replies, \"People go to school to learn. Everyone needs an ___.\"",
"Our class is putting on a show! The show has lots of songs for us to sing and perform. What word could describe this show?",
"Our class is putting on a show! The show has lots of songs for us to sing and perform. What word could describe this show?",
"Emily wants to be able to remember all of the song's words in her favorite song. Emily wants to ___ the song.",
"Emily wants to be able to remember all of the song's words in her favourite song. Emily wants to ___ the song.",
"I'm reading a book about Lonnie Johnson, the inventor of the Super Soaker™ water toy. I wish I could ask him, \"How did you get the idea to make that ___.\"",
"Mikaela took her cousin's video game. Her uncle said, \"Mikaela, don't take something without asking. Please ___ by saying you are sorry.\"",
"Mikaela took her cousin's video game. Her uncle said, \"Mikaela, don't take something without asking. Please ___ by saying you are sorry.\"",
"The sky turned dark as smoke and lava came out of the volcano. The ___ could be seen for miles and miles.",
"Pollution on land, in the air, and in the earth's water doesn't only affect one community or one country. Instead, pollution is a ___ problem.",
];

const wordTranscript = [
"subtraction",
    "subtraction",
  "national",
  "national",
  "education",
  "education",
  "musical",
  "musical",
  "memorize",
  "memorise",
  "invention",
  "apologize",
  "apologise",
  "eruption",
  "global",
];

const hintTranscript = [
"",
"",
  "having to do with a nation",
"having to do with a nation",
"the process of being educated",
"the process of being educated",
"having the characteristics of music",
"having the characteristics of music",
"to make stay in your memory",
"to make stay in your memory",
"a thing that is invented",
"to make an apology",
"to make an apology",
"the process of erupting",
"having to do with the globe",
];

const dropZones = [
  [
    { dropZone: "subtract", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "subtract", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "nation", label: "word" },
    { dropZone: "al", label: "suffix" }
  ],
  [
    { dropZone: "nation", label: "word" },
    { dropZone: "al", label: "suffix" }
  ],
  [
    { dropZone: "educate", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "educate", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "music", label: "word" },
    { dropZone: "al", label: "suffix" }
  ],
  [
    { dropZone: "music", label: "word" },
    { dropZone: "al", label: "suffix" }
  ],
  [
    { dropZone: "memory", label: "word" },
    { dropZone: "ize", label: "suffix" }
  ],
  [
    { dropZone: "memory", label: "word" },
    { dropZone: "ise", label: "suffix" }
  ],
  [
    { dropZone: "invent", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "apology", label: "word" },
    { dropZone: "ize", label: "suffix" }
  ],
  [
    { dropZone: "apology", label: "word" },
    { dropZone: "ise", label: "suffix" }
  ],
  [
    { dropZone: "erupt", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "globe", label: "word" },
    { dropZone: "al", label: "suffix" }
  ]
];

const choices = [
  ["subtract*",	"magic",	"tion*",	"al"],
  ["subtract*",	"magic",	"tion*",	"al"],
  ["nation*",	"planet",	"al*",	"ize"],
  ["nation*",	"planet",	"al*",	"ise"],
  ["educate*",	"arrive",	"tion*",	"ize"],
  ["educate*",	"arrive",	"tion*",	"ise"],
  ["music*",	"energy",	"ize",	"al*"],
  ["music*",	"energy",	"ise",	"al*"],
  ["memory*",	"actress",	"al",	"ize*"],
  ["memory*",	"actress",	"al",	"ise*"],
  ["invent*",	"science",	"tion*",	"al"],
  ["apology*",	"quick",	"al",	"ize*"],
  ["apology*",	"quick",	"al",	"ise*"],
  ["erupt*",	"fire",	"tion*",	"al"],
  ["globe*",	"trash",	"tion",	"al*"],
];

const kind = [
"",
"",
  "suffix_al2",
  "suffix_al2",
  "suffix_tion1",
  "suffix_tion1",
  "suffix_al1",
  "suffix_al1",
  "suffix_ize1",
  "suffix_ize1",
  "suffix_tion2",
  "suffix_ize2",
  "suffix_ize2",
  "suffix_tion3",
  "suffix_al3",
];

const regions = [
  "US",
  "UK",
  "US",
  "UK",
  "US",
  "UK",
  "US",
  "UK",
  "US",
  "UK",
  "all",
  "US",
  "UK",
"all",
"all"
];


export default class CombiningVocStrat extends Component {
  componentDidMount() {
    const taskData = this.makeTaskData();

    let config = {
      type: "Combining",
      params: {
        contentId: taskData.map(({ contentId }) => contentId),
        borderedDropZone: true,
        layout: "evalOnSubmitFixed",
        showSpotlightHint: true,
        stylePrefix: "vocstrat_cmb",
        shuffleChoices: true,
        useSubmitButton: true,
        submitPosition: "side",
        suppressCorrectDefault: true,
        suppressIncorrectDefault: true
      }
    };

    this.setState({taskData, config});
  }

  makeTaskData() {

    return dropZones.map((dzs, i) => {
      let root = '';
      let dzones = dzs.map(dz => {
        return {
          dropZone: dz.dropZone,
          label: dz.label,
          audio: `common_vocstrat.suffix_${dz.dropZone}`
        }
      });

      let hintSuffix = '';

      let theChoices = choices[i].map(choice => {
        let choiceMinusToken = choice.replace("*", "");
        if(dropZones.findIndex(dz => dz.dropZone === choiceMinusToken) > -1) {
          root = choice;
        }
        return {
          contentId: uuid.v4(),
          choice: choiceMinusToken,
          kind: choiceMinusToken
        }
      });

      let task = {
        type: "COMBINING",
        contentId: uuid.v4()
      };
      if(regions && ["us","uk"].includes(regions[i].toLowerCase())) {
        task.region = [regions[i].toLowerCase()]
      }

      if(kind[i] && kind[i].length) {
        task.kind = kind[i];
      }

      let staticTaskProps = {
        kind: kind[i],
        spotlightHint: [{ text: spotlightHint[i] }],
        spotlightText: [{ text: wordTranscript[i] }],
        dropZones: dzones,
        choices: theChoices,
        solved: dropZones[i].map(({ dropZone }) => dropZone).join(""),
        audio: [
          { type: "word", path: `words.${wordTranscript[i]}` }
        ]
      };

      if(hintTranscript[i] && hintTranscript[i].length) {
        staticTaskProps.audio.push({
          type: "hint",
          path: `common_vocstrat.${filenameGenerator(hintTranscript[i])}${hintSuffix}`
        })
      }
      return Object.assign({},task,staticTaskProps);

    });
  }

  render() {
    return this.state && this.state.taskData
        ? (
          <div style={{ display: "flex" }}>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
              <textarea
                  id="outputData"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
              />
            </div>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
              <textarea
                  id="outputConfig"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.config))}
              />
            </div>
          </div>
      )
        : <div/>
  }
}