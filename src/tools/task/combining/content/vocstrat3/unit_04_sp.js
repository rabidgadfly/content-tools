import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import {filenameGenerator} from "../../../util/filenameGenerator";

const spotlightHint = [
"Divya loves playing sports of all kinds and is always developing her skills. What word describes Divya?",
"Omar loves drawing and painting. His skills at creating artwork are remarkable. What word describes Omar?",
"Few people used to eat kale, the dark lettuce in the cabbage family. However, recently, it has grown in ______ and now many people eat it.",
"When Julius made cookies, he added flour, sugar, butter, and eggs to the batter. Then he used a spoon to combine them. Julius created a ______.",
"Superheroes can do different things. Some are super strong, some can be invisible, and some can heal others. If you were a superhero, what ______ would you want?",
"We swam at the public swimming pool all summer. In the fall, the lifeguard announced the pool's ______. She stated that the pool was no longer open.",
"We swam at the public swimming pool all summer. In the autumn, the lifeguard announced the pool's ______. She stated that the pool was no longer open.",
"My refrigerator is shiny and silver, and sometimes I stick magnets to it. What word describes my refrigerator?",
"There are many volcanoes in Hawaii. What is one word to describe Hawaii?",
"Jabari is always asking questions about the world. Why is the sky blue? How do cars move? Jabari shows a lot of ______.",
];

const wordTranscript = [
  "",
"artistic",
"popularity",
"mixture",
"ability",
"closure",
"closure",
"metallic",
"volcanic",
"curiosity",
];

const hintTranscript = [
"",
  "having the qualities of an artist",
  "the state of being popular",
  "a thing that is mixed",
  "the state of being able",
  "the process of closing",
  "the process of closing",
  "having metal",
  "having volcanoes",
  "the state of being curious",
];

const dropZones = [
  [
    { dropZone: "athlete", label: "word" },
    { dropZone: "ic", label: "suffix" }
  ],
  [
    { dropZone: "artist", label: "word" },
    { dropZone: "ic", label: "suffix" }
  ],
  [
    { dropZone: "popular", label: "word" },
    { dropZone: "ity", label: "suffix" }
  ],
  [
    { dropZone: "mix", label: "word" },
    { dropZone: "ture", label: "suffix" }
  ],
  [
    { dropZone: "able", label: "word" },
    { dropZone: "ity", label: "suffix" }
  ],
  [
    { dropZone: "close", label: "word" },
    { dropZone: "sure", label: "suffix" }
  ],
  [
    { dropZone: "close", label: "word" },
    { dropZone: "sure", label: "suffix" }
  ],
  [
    { dropZone: "metal", label: "word" },
    { dropZone: "ic", label: "suffix" }
  ],
  [
    { dropZone: "volcano", label: "word" },
    { dropZone: "ic", label: "suffix" }
  ],
  [
    { dropZone: "curious", label: "word" },
    { dropZone: "ity", label: "suffix" }
  ]
];

const choices = [
  ["athlete*",	"fail",	"ic*",	"ure"],
  ["depart",	"artist*",	"ic*",	"ity"],
  ["press",	"popular*",	"ic",	"ity*"],
  ["mix*",	"class",	"ity",	"ture*"],
  ["normal",	"able*",	"ity*",	"ture"],
  ["humid",	"close*",	"ic",	"ure*"],
  ["humid",	"close*",	"ic",	"ure*"],
  ["metal*",	"warm",	"ic*",	"ure"],
  ["volcano*",	"local",	"ic*",	"ity"],
  ["curious*",	"comedy",	"ity*",	"ture"],
];

const kind = [
"",
  "suffix_ic3",
  "suffix_ity1",
  "suffix_ure1",
  "suffix_ity2",
  "suffix_ure2",
  "suffix_ure2",
  "suffix_ic1",
  "suffix_ic2",
  "suffix_ity3",
];

const regions = [
  "all",
  "all",
  "all",
  "all",
  "all",
  "US",
  "UK",
  "all",
  "all",
  "all",
];


export default class CombiningVocStrat extends Component {
  componentDidMount() {
    const taskData = this.makeTaskData();

    let config = {
      type: "Combining",
      params: {
        contentId: taskData.map(({ contentId }) => contentId),
        borderedDropZone: true,
        layout: "evalOnSubmitFixed",
        showSpotlightHint: true,
        stylePrefix: "vocstrat_cmb",
        shuffleChoices: true,
        useSubmitButton: true,
        submitPosition: "side",
        suppressCorrectDefault: true,
        suppressIncorrectDefault: true
      }
    };

    this.setState({taskData, config});
  }

  makeTaskData() {

    return dropZones.map((dzs, i) => {
      let root = '';
      let dzones = dzs.map(dz => {
        return {
          dropZone: dz.dropZone,
          label: dz.label,
          audio: `common_vocstrat.suffix_${dz.dropZone}`
        }
      });

      let hintSuffix = '';

      let theChoices = choices[i].map(choice => {
        let choiceMinusToken = choice.replace("*", "");
        if(dropZones.findIndex(dz => dz.dropZone === choiceMinusToken) > -1) {
          root = choice;
        }
        return {
          contentId: uuid.v4(),
          choice: choiceMinusToken,
          kind: choiceMinusToken
        }
      });

      let task = {
        type: "COMBINING",
        contentId: uuid.v4()
      };
      if(regions && ["us","uk"].includes(regions[i].toLowerCase())) {
        task.region = [regions[i].toLowerCase()]
      }

      if(kind[i] && kind[i].length) {
        task.kind = kind[i];
      }

      let staticTaskProps = {
        kind: kind[i],
        spotlightHint: [{ text: spotlightHint[i] }],
        spotlightText: [{ text: wordTranscript[i] }],
        dropZones: dzones,
        choices: theChoices,
        solved: dropZones[i].map(({ dropZone }) => dropZone).join(""),
        audio: [
          { type: "word", path: `words.${wordTranscript[i]}` }
        ]
      };

      if(hintTranscript[i] && hintTranscript[i].length) {
        staticTaskProps.audio.push({
          type: "hint",
          path: `common_vocstrat.${filenameGenerator(hintTranscript[i])}${hintSuffix}`
        })
      }
      return Object.assign({},task,staticTaskProps);

    });
  }

  render() {
    return this.state && this.state.taskData
        ? (
          <div style={{ display: "flex" }}>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
              <textarea
                  id="outputData"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
              />
            </div>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
              <textarea
                  id="outputConfig"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.config))}
              />
            </div>
          </div>
      )
        : <div/>
  }
}