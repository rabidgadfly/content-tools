import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import {filenameGenerator} from "../../../util/filenameGenerator";

const spotlightHint = [
  "The beluga whale has a round head with a bump at the top that helps it communicate with its family. The bump is on the whale's ______.",
  "Jaxon did not finish his homework because he got distracted by playing video games. What word describes Jaxon's homework?",
  "Skyler and her family are taking an airplane over the Atlantic Ocean, from the United States to England. What kind of flight will they take?",
  "Skyler and her family are taking an aeroplane over the Atlantic Ocean, from the United States to England. What kind of flight will they take?",
  "Before the story's hero entered the forest, the wizard told her there was danger ahead. He advised, \"Be ______. Threats lurk in this forest.\"",
  "If Alexa could have any superpower, she would want to make herself unable to be seen. Alexa wants the power to be ______.",
  "The students spent lots of time planning during the weeks before the class's end-of-year party. The students gave the party a lot of ______.",
  "It's amazing how bodies can change food into the energy needed to move, grow, and live. Bodies ______ food into energy.",
  "Kai's grandmother assured him that he didn't need to spend a lot of money for her birthday. A simple, ______ gift would be perfect.",
  "The fortune-teller says he knows what will happen before events occur. \"I can ______ the future, he claims.\"",
];

const wordTranscript = [
  "",
  "incomplete",
  "transatlantic",
  "transatlantic",
  "forewarned",
  "invisible",
  "forethought",
  "transform",
  "inexpensive",
  "foretell",
];

const hintTranscript = [
"",
  "not complete",
  "across the Atlantic",
  "across the Atlantic",
  "warned before",
  "not visible",
  "thought before",
  "change the form",
  "not expensive",
  "tell before",
];

const dropZones = [
  [
    { dropZone: "fore", label: "prefix" },
    { dropZone: "head", label: "word" }
  ],
  [
    { dropZone: "in", label: "prefix" },
    { dropZone: "complete", label: "word" }
  ],
  [
    { dropZone: "trans", label: "prefix" },
    { dropZone: "Atlantic", label: "word" }
  ],
  [
    { dropZone: "trans", label: "prefix" },
    { dropZone: "Atlantic", label: "word" }
  ],
  [
    { dropZone: "fore", label: "prefix" },
    { dropZone: "warned", label: "word" }
  ],
  [
    { dropZone: "in", label: "prefix" },
    { dropZone: "visible", label: "word" }
  ],
  [
    { dropZone: "fore", label: "prefix" },
    { dropZone: "thought", label: "word" }
  ],
  [
    { dropZone: "trans", label: "prefix" },
    { dropZone: "form", label: "word" }
  ],
  [
    { dropZone: "in", label: "prefix" },
    { dropZone: "expensive", label: "word" }
  ],
  [
    { dropZone: "fore", label: "prefix" },
    { dropZone: "tell", label: "word" }
  ]
];

const choices = [
  ["fore*",	"in",	"head*",	"correct"],
  ["in*",	"trans",	"complete*",	"thought"],
  ["trans*",	"fore",	"Atlantic*",	"expense"],
  ["trans*",	"fore",	"Atlantic*",	"expense"],
  ["in",	"fore*",	"warned*",	"direct"],
  ["in*",	"trans",	"visible*",	"action"],
  ["in",	"fore*",	"thought*",	"energy"],
  ["in",	"trans*",	"form*",	"tell"],
  ["in*",	"trans",	"expensive*",	"celebration"],
  ["in",	"fore*",	"tell*",	"accurate"],
];

const kind = [
"",
  "prefix_in",
  "prefix_trans",
  "prefix_trans",
  "prefix_fore",
  "prefix_in",
  "prefix_fore",
  "prefix_trans",
  "prefix_in",
  "prefix_fore",
];

const regions = [
  "all",
  "all",
  "US",
  "UK",
  "all",
  "all",
  "all",
  "all",
  "all",
  "all",
];


export default class CombiningVocStrat extends Component {
  componentDidMount() {
    const taskData = this.makeTaskData();

    let config = {
      type: "Combining",
      params: {
        contentId: taskData.map(({ contentId }) => contentId),
        borderedDropZone: true,
        layout: "evalOnSubmitFixed",
        showSpotlightHint: true,
        stylePrefix: "vocstrat_cmb",
        shuffleChoices: true,
        useSubmitButton: true,
        submitPosition: "side",
        suppressCorrectDefault: true,
        suppressIncorrectDefault: true
      }
    };

    this.setState({taskData, config});
  }

  makeTaskData() {

    return dropZones.map((dzs, i) => {
      let root = '';
      let dzones = dzs.map(dz => {
        return {
          dropZone: dz.dropZone,
          label: dz.label,
          audio: `common_vocstrat.suffix_${dz.dropZone}`
        }
      });

      let hintSuffix = '';

      let theChoices = choices[i].map(choice => {
        let choiceMinusToken = choice.replace("*", "");
        if(dropZones.findIndex(dz => dz.dropZone === choiceMinusToken) > -1) {
          root = choice;
        }
        return {
          contentId: uuid.v4(),
          choice: choiceMinusToken,
          kind: choiceMinusToken
        }
      });

      let task = {
        type: "COMBINING",
        contentId: uuid.v4()
      };
      if(regions && ["us","uk"].includes(regions[i].toLowerCase())) {
        task.region = [regions[i].toLowerCase()]
      }

      if(kind[i] && kind[i].length) {
        task.kind = kind[i];
      }

      let staticTaskProps = {
        kind: kind[i],
        spotlightHint: [{ text: spotlightHint[i] }],
        spotlightText: [{ text: wordTranscript[i] }],
        dropZones: dzones,
        choices: theChoices,
        solved: dropZones[i].map(({ dropZone }) => dropZone).join(""),
        audio: [
          { type: "word", path: `words.${wordTranscript[i]}` }
        ]
      };

      if(hintTranscript[i] && hintTranscript[i].length) {
        staticTaskProps.audio.push({
          type: "hint",
          path: `common_vocstrat.${filenameGenerator(hintTranscript[i])}${hintSuffix}`
        })
      }
      return Object.assign({},task,staticTaskProps);

    });
  }

  render() {
    return this.state && this.state.taskData
        ? (
          <div style={{ display: "flex" }}>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
              <textarea
                  id="outputData"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
              />
            </div>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
              <textarea
                  id="outputConfig"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.config))}
              />
            </div>
          </div>
      )
        : <div/>
  }
}