import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import {filenameGenerator} from "../../../util/filenameGenerator";

const spotlightHint = [
"Ryan is doing a math problem about a dog who eats pie. He has to take one number away from another. What is this math process called?",
"Ryan is doing a maths problem about a dog who eats pie. He has to take one number away from another. What is this maths process called?",
"Saloni knew that she had not watered her tomato plants enough when the tomatoes changed from red to brown. What word describes the tomato plant?",
"Saloni knew that she had not watered her tomato plants enough when the tomatoes changed from red to brown. What word describes the tomato plant?",
"On a hot July day, Anurag tried to turn on his fan, but nothing happened. Then he saw the fan was no longer plugged into the wall. What word describes why the fan won't work?",
"Anurag plugged his fan into the wall and it immediately started blowing cool air on him. What word describes why the fan works?",
"Mahina is always coming up with creative solutions. She makes new and useful tools that no one has seen before. How would you describe Mahina?",
"Sam built a completely new tool that will solve a tough problem. What word describes a tool created for the first time?",
"Before the first day of school, Mr. Gonzalez wants to know the name of every single student in his class. He hopes to ___ his entire class roster.",
"Before the first day of school, Mr Gonzalez wants to know the name of every single student in his class. He hopes to ___ his entire class register.",
"The school held a ceremony at the end of the year for the students who completed all of the grade levels. What kind of ceremony is it?",
"My grandfather always told me that the most important thing I can do is learn as much as I can. What does he think is important?",
];

const wordTranscript = [
"subtraction",
"subtraction",
  "discolored",
  "discoloured",
  "disconnected",
  "reconnected",
  "inventive",
  "invention",
  "memorize",
  "memorise",
  "graduation",
  "education",
];

const hintTranscript = [
"",
"",
  "not colored correctly",
  "not coloured correctly",
  "not connected",
  "connected again",
  "having the ability to invent",
  "something that is invented",
  "to make stay in memory",
  "to make stay in memory",
  "the process of graduating",
  "the process of becoming educated",
];

const dropZones = [
  [
    { dropZone: "subtract", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "subtract", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "dis", label: "prefix" },
    { dropZone: "colored", label: "word" }
  ],
  [
    { dropZone: "dis", label: "prefix" },
    { dropZone: "coloured", label: "word" }
  ],
  [
    { dropZone: "dis", label: "prefix" },
    { dropZone: "connected", label: "word" }
  ],
  [
    { dropZone: "re", label: "prefix" },
    { dropZone: "connected", label: "word" }
  ],
  [
    { dropZone: "invent", label: "word" },
    { dropZone: "ive", label: "suffix" }
  ],
  [
    { dropZone: "invent", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "memory", label: "word" },
    { dropZone: "ize", label: "suffix" }
  ],
  [
    { dropZone: "memory", label: "word" },
    { dropZone: "ise", label: "suffix" }
  ],
  [
    { dropZone: "graduate", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ],
  [
    { dropZone: "educate", label: "word" },
    { dropZone: "tion", label: "suffix" }
  ]
];

const choices = [
  ["subtract*",	"magic",	"tion*",	"al"],
  ["subtract*",	"magic",	"tion*",	"al"],
  ["trans",	"dis*",	"colored*",	"establish"],
  ["trans",	"dis*",	"coloured*",	"establish"],
  ["dis*",	"re",	"connected*",	"presented"],
  ["re*",	"mis",	"connected*",	"presented"],
  ["science",	"invent*",	"tion",	"ive*"],
  ["distract",	"invent*",	"tion*",	"ive"],
  ["memory*",	"capital",	"ive",	"ize*"],
  ["memory*",	"capital",	"ive",	"ise*"],
  ["graduate*",	"subtract",	"tion*",	"ive"],
  ["invent",	"educate*",	"tion*",	"ive"],
];

const kind = [
"",
"",
  "prefix_dis2",
  "prefix_dis2",
  "prefix_dis1",
  "prefix_re",
  "suffix_ive",
  "suffix_tion3",
  "suffix_ize",
  "suffix_ize",
  "suffix_tion1",
  "suffix_tion2",
];

const regions = [
  "US",
  "UK",
  "US",
  "UK",
  "all",
  "all",
  "all",
  "all",
  "US",
  "UK",
  "all",
  "all",
];


export default class CombiningVocStrat extends Component {
  componentDidMount() {
    const taskData = this.makeTaskData();

    let config = {
      type: "Combining",
      params: {
        contentId: taskData.map(({ contentId }) => contentId),
        borderedDropZone: true,
        layout: "evalOnSubmitFixed",
        showSpotlightHint: true,
        stylePrefix: "vocstrat_cmb",
        shuffleChoices: true,
        useSubmitButton: true,
        submitPosition: "side",
        suppressCorrectDefault: true,
        suppressIncorrectDefault: true
      }
    };

    this.setState({taskData, config});
  }

  makeTaskData() {

    return dropZones.map((dzs, i) => {
      let root = '';
      let dzones = dzs.map(dz => {
        return {
          dropZone: dz.dropZone,
          label: dz.label,
          audio: `common_vocstrat.suffix_${dz.dropZone}`
        }
      });

      let hintSuffix = '';

      let theChoices = choices[i].map(choice => {
        let choiceMinusToken = choice.replace("*", "");
        if(dropZones.findIndex(dz => dz.dropZone === choiceMinusToken) > -1) {
          root = choice;
        }
        return {
          contentId: uuid.v4(),
          choice: choiceMinusToken,
          kind: choiceMinusToken
        }
      });

      let task = {
        type: "COMBINING",
        contentId: uuid.v4()
      };
      if(regions && ["us","uk"].includes(regions[i].toLowerCase())) {
        task.region = [regions[i].toLowerCase()]
      }

      if(kind[i] && kind[i].length) {
        task.kind = kind[i];
      }

      let staticTaskProps = {
        kind: kind[i],
        spotlightHint: [{ text: spotlightHint[i] }],
        spotlightText: [{ text: wordTranscript[i] }],
        dropZones: dzones,
        choices: theChoices,
        solved: dropZones[i].map(({ dropZone }) => dropZone).join(""),
        audio: [
          { type: "word", path: `words.${wordTranscript[i]}` }
        ]
      };

      if(hintTranscript[i] && hintTranscript[i].length) {
        staticTaskProps.audio.push({
          type: "hint",
          path: `common_vocstrat.${filenameGenerator(hintTranscript[i])}${hintSuffix}`
        })
      }
      return Object.assign({},task,staticTaskProps);

    });
  }

  render() {
    return this.state && this.state.taskData
        ? (
          <div style={{ display: "flex" }}>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
              <textarea
                  id="outputData"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
              />
            </div>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
              <textarea
                  id="outputConfig"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.config))}
              />
            </div>
          </div>
      )
        : <div/>
  }
}