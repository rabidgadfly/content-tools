import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import {filenameGenerator} from "../../../util/filenameGenerator";

const spotlightHint = [
"Reginaldo worked at his computer throughout the night. Now, Reginaldo is fighting the feeling of ___.",
"The volcano has not erupted in fifty years. Scientists say that there might not be any action in the volcano for another fifty years. How would you describe the volcano?",
"The museum has an exhibit about volcanoes. Visitors go inside a model of a volcano and play with liquid that represents lava. How would you describe the model of the volcano?",
"When my family orders pizza, we often get one large pie to share. Sometimes, my dad gets small, single pizzas for everyone. What word describes one of those individual pizzas?",
"My brother eats his whole pizza in three giant bites. My little sister takes tiny nibbles of her pizza. They each eat their pizza according to their character and ___.",
"Every country sends a team of athletes to the Olympics. What word describes one country's team?",
"At the opening ceremony of the Olympics, there are many people from many different countries. What word describes the Olympics?",
"Sunshine the dog is always happy to do whatever her owners tell her to do. She'll come, sit, roll over, and shake. What quality does Sunshine have?",
"Luna the cat won't listen to anything her owners say. She does whatever she wants, whenever she wants. What quality does Luna have?",
];

const wordTranscript = [
"",
  "inactive",
  "interactive",
  "personal",
  "personality",
  "national",
  "international",
  "willingness",
  "unwillingness",
];

const hintTranscript = [
"",
"not active",
"active together",
"having to do with one person",
"the personal way each individual is",
"having to do with a nation",
"nations together",
"the quality of being willing",
"not having willingness",
];

const dropZones = [
  [
    { dropZone: "sleepy", label: "word" },
    { dropZone: "ness", label: "suffix" }
  ],
  [
    { dropZone: "in", label: "prefix" },
    { dropZone: "active", label: "word" }
  ],
  [
    { dropZone: "inter", label: "prefix" },
    { dropZone: "active", label: "word" }
  ],
  [
    { dropZone: "person", label: "word" },
    { dropZone: "al", label: "suffix" }
  ],
  [
    { dropZone: "personal", label: "word" },
    { dropZone: "ity", label: "suffix" }
  ],
  [
    { dropZone: "nation", label: "word" },
    { dropZone: "al", label: "suffix" }
  ],
  [
    { dropZone: "inter", label: "prefix" },
    { dropZone: "national", label: "word" }
  ],
  [
    { dropZone: "willing", label: "word" },
    { dropZone: "ness", label: "suffix" }
  ],
  [
    { dropZone: "un", label: "prefix" },
    { dropZone: "willingness", label: "word" }
  ]
];

const choices = [
  ["sleep*",	"danger",	"ness*",	"ous"],
  ["in*",	"inter",	"active*",	"explosive"],
  ["in",	"inter*",	"active*",	"hollow"],
  ["person*",	"separate",	"al*",	"ity"],
  ["personal*",	"hunger",	"ity*",	"ness"],
  ["nation*",	"sport",	"al*",	"ness"],
  ["inter*",	"un",	"national*",	"joyful"],
  ["willing*",	"playing",	"ness*",	"al"],
  ["un*",	"inter",	"willingness*",	"crankiness"],
];

const kind = [
"",
  "prefix_in",
  "prefix_inter1",
  "suffix_al1",
  "suffix_ity",
  "suffix_al2",
  "prefix_inter2",
  "suffix_ness",
  "prefix_un",
];

const regions = [
  "all",
  "all",
  "all",
  "all",
  "all",
  "all",
  "all",
  "all",
  "all",
];


export default class CombiningVocStrat extends Component {
  componentDidMount() {
    const taskData = this.makeTaskData();

    let config = {
      type: "Combining",
      params: {
        contentId: taskData.map(({ contentId }) => contentId),
        borderedDropZone: true,
        layout: "evalOnSubmitFixed",
        showSpotlightHint: true,
        stylePrefix: "vocstrat_cmb",
        shuffleChoices: true,
        useSubmitButton: true,
        submitPosition: "side",
        suppressCorrectDefault: true,
        suppressIncorrectDefault: true
      }
    };

    this.setState({taskData, config});
  }

  makeTaskData() {

    return dropZones.map((dzs, i) => {
      let root = '';
      let dzones = dzs.map(dz => {
        return {
          dropZone: dz.dropZone,
          label: dz.label,
          audio: `common_vocstrat.suffix_${dz.dropZone}`
        }
      });

      let hintSuffix = '';

      let theChoices = choices[i].map(choice => {
        let choiceMinusToken = choice.replace("*", "");
        if(dropZones.findIndex(dz => dz.dropZone === choiceMinusToken) > -1) {
          root = choice;
        }
        return {
          contentId: uuid.v4(),
          choice: choiceMinusToken,
          kind: choiceMinusToken
        }
      });

      let task = {
        type: "COMBINING",
        contentId: uuid.v4()
      };
      if(regions && ["us","uk"].includes(regions[i].toLowerCase())) {
        task.region = [regions[i].toLowerCase()]
      }

      if(kind[i] && kind[i].length) {
        task.kind = kind[i];
      }

      let staticTaskProps = {
        kind: kind[i],
        spotlightHint: [{ text: spotlightHint[i] }],
        spotlightText: [{ text: wordTranscript[i] }],
        dropZones: dzones,
        choices: theChoices,
        solved: dropZones[i].map(({ dropZone }) => dropZone).join(""),
        audio: [
          { type: "word", path: `words.${wordTranscript[i]}` }
        ]
      };

      if(hintTranscript[i] && hintTranscript[i].length) {
        staticTaskProps.audio.push({
          type: "hint",
          path: `common_vocstrat.${filenameGenerator(hintTranscript[i])}${hintSuffix}`
        })
      }
      return Object.assign({},task,staticTaskProps);

    });
  }

  render() {
    return this.state && this.state.taskData
        ? (
          <div style={{ display: "flex" }}>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
              <textarea
                  id="outputData"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
              />
            </div>
            <div style={{ width: "400px", padding: "10px" }}>
              <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
              <textarea
                  id="outputConfig"
                  rows="40"
                  style={{ width: "100%" }}
                  defaultValue={js_beautify(JSON.stringify(this.state.config))}
              />
            </div>
          </div>
      )
        : <div/>
  }
}