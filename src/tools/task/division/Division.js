import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

const dataSource = [
"Every four years / Olympic athletes compete.",
"During the day / bats are inside the cave.",
"The next morning / June found an envelope.",
"In a bamboo forest / the plants are very tall.",
"The park was so vast / he couldn’t tour it all.",
"As she rolled down the hill / she felt dizzy.",
"Water returns to Earth / as part of a cycle.",
"When the man got to the stream / he lay down.",
"Mike looked out at the rain / and frowned.",
"The next day / the ant saw the same dove.",
"Squirrels bury seeds / for later eating.",
"As he looked about / he noticed two rocks."
];

const getWordChoice = word => {
	return {
    "choice": word,
    "kind": word,
    "isPrefill": true,
    "contentId": uuid.v4()
  }
}

const getDividerChoice = () => {
	return {
    "choice": "|",
    "kind": "|",
    "isPrefill": false,
    "image": "syldiv_graphic.syldiv_slider",
    "contentId": uuid.v4()
  }
}

const getChoices = sentenceArr => sentenceArr.reduce( (acc,word) => {
	if( word === "/" ) {
  	return [...acc,getDividerChoice(word)]
  } else {
  	return [...acc,getWordChoice(word)]
  }
},[])

const getDropZones = sentenceArr => sentenceArr.reduce( (acc,word,ind,arr) => {
	if( word === "/" ) {
  	return [...acc, {
              "dropZone": "|",
              "answer": true
            }]
	} else {
  	if( ind === arr.length-1 || arr[ind+1] === "/")  {
  		return [...acc,{
              "dropZone": word,
              "isPrefill": true,
              "answer": false
            }
      ]
    }

	  return [...acc,{
              "dropZone": word,
              "isPrefill": true,
              "answer": false
            },
            {
              "dropZone": "|",
              "answer": false
            }
  	]
  }
},[])

const getAudio = sentence => {
	let split = sentence.split("/");
  let regex = / /gi;

	return [{
        "type": "fragment1",
        "path": `sentence_fragment.${split[0].trim().replace(regex,"_").toLowerCase()}`
    }, {
        "type": "fragment2",
        "path": `sentence_fragment.${split[1].trim().replace(regex,"_").toLowerCase()}`
    }]
}


const getDivisionObj = sentence => {
	const sentenceSplit = sentence.split(' ');
	return {
  	"type": "DIVISION",
    "contentId": uuid.v4(),
		"choices": getChoices(sentenceSplit),
    "dropZones": getDropZones(sentenceSplit),
    "audio": getAudio(sentence)
  }
}

const generateDivision = sentencesArr => {
	return sentencesArr.map(sentence => getDivisionObj(sentence))
}


export default class Division extends Component {
  render() {
    return (
      <div>
          <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
          <textarea
		  	id="outputData"
			rows="50"
			style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(generateDivision(dataSource)))}
          />
      </div>
    );
  }
}
