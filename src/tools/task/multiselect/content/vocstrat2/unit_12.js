import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

import { makeSpotlightText } from "../../../util/makeSpotlightText";
export const branchingAudio = {
    verb: "a_noun_part_speech_names_person_verb",
    verb_present_tense: "a_past_tense_verb_usually_ends_us",
    pronouns_plural: "pronouns_take_the_place_of_nouns",
    noun: "a_noun_part_speech_names_person_verb",
    verb_past_tense: "a_past_tense_verb_usually_ends_us",
    possessive_noun: "a_possessive_noun_shows_belonging_has",
    adjective: "adjectives_tell_more_about_nouns",
    adverb: "adverbs_are_words_describe_verb_some",
    interjection: "an_interjection_word_shows_strong_feeling",
    conjunction: "conjunctions_show_ideas_sentence_are_related",
    conjunction_noun_verb: "conjunctions_like_words_but_so_show",
    verb_future_tense: "some_prepositional_phrases_tell_when_action",
    pronouns_singular: "pronouns_take_the_place_of_nouns",
    prepositional_phrase: "some_prepositional_phrases_tell_when_action",
    simple_subject_predicate: "the_simple_subject_noun_pronoun_tells_predicate",
    relative_pronoun: "the_words_which_who_whose_whom"
}

const affixTranscripts = [
    "tion",
    "tion",
    "ize",
    "ise",
    "al",
    "al",
    "ize",
    "ise",
    "ize",
    "ise",
    "al",
    "al",
    "tion",
    "al",
    "al",
    "al",
    "ize",
    "ise",
    "ize",
    "ise",
    "al",
    "al",
    "tion",
    "al",
    "al",
    "tion",
]

const definitionTranscripts = [
    "Education is the process of being educated.",
        "Education is the process of being educated.",
        "Finalize means to make final.",
        "Finalise means to make final.",
        "Musical means having the characterstics of music.",
        "Musical means having the characterstics of music.",
        "Finalize means to make final.",
        "Finalise means to make final.",
        "To memorize is to make stay in your memory.",
        "To memorise is to make stay in your memory.",
        "Magical describes something that has magic.",
        "Magical describes something that has magic.",
        "An invention is a thing that is invented.",
        "Magical describes something that has magic.",
        "National means having to do with a nation.",
    "National means having to do with a nation.",
    "Finalize means to make final.",
    "Finalise means to make final.",
    "To apologize is to make an apology.",
    "To apologise is to make an apology.",
    "Magical describes something that has magic.",
    "Magical describes something that has magic.",
    "Eruption means the process of erupting.",
    "Magical describes something that has magic.",
    "Global means having to do with the globe.",
    "Subtraction is the process of subtracting.",
];

const group1Choice1 = [
    "-tion|thing, action, or process*",
    "-tion|thing, action, or process*",
    "-ize|to make*",
    "-ise|to make*",
    "-al|having to do with*",
    "-al|having to do with*",
    "-al|having to do with",
    "-al|having to do with",
    "-ize|to make*",
    "-ise|to make*",
    "-ize|to make",
    "-ise|to make",
    "-tion|thing, action, or process*",
    "-tion|thing, action, or process",
    "-al|having to do with*",
    "-al|having to do with*",
    "-al|having to do with",
    "-al|having to do with",
    "-ize|to make*",
    "-ise|to make*",
    "-ize|to make",
    "-ise|to make",
    "-tion|thing, action, or process*",
    "-tion|thing, action, or process",
    "-al|having to do with*",
    "-al|having to do with",
];

const group1Choice2 = [
"-ize|to make",
"-ise|to make",
"-tion|thing, action, or process",
"-tion|thing, action, or process",
"-ize|to make",
"-ise|to make",
"-ize|to make*",
"-ise|to make*",
"-al|having to do with",
"-al|having to do with",
"-al|having to do with*",
"-al|having to do with*",
"-al|having to do with",
"-al|having to do with*",
"-ize|to make",
"-ise|to make",
"-ize|to make*",
"-ise|to make*",
"-al|having to do with",
"-al|having to do with",
"-al|having to do with*",
"-al|having to do with*",
"-al|having to do with",
"-al|having to do with*",
"-tion|thing, action, or process",
"-tion|thing, action, or process*",
];

const group2Choice1 = [
    "education*",
    "education*",
    "education",
    "education",
    "musical*",
    "musical*",
    "musical",
    "musical",
    "memorize*",
    "memorise*",
    "memorize",
    "memorise",
    "invention*",
    "invention",
    "national*",
    "national*",
    "national",
    "national",
    "apologize*",
    "apologise*",
    "apologize",
    "apologise",
    "eruption*",
    "eruption",
    "global*",
    "global",
]

const group2Choice2 = [
    "finalize",
    "finalise",
    "finalize*",
    "finalise*",
    "finalize",
    "finalise",
    "finalize*",
    "finalise*",
    "magical",
    "magical",
    "magical*",
    "magical*",
    "magical",
    "magical*",
    "finalize",
    "finalise",
    "finalize*",
    "finalise*",
    "magical",
    "magical",
    "magical*",
    "magical*",
    "magical",
    "magical*",
    "subtraction",
    "subtraction*",
]

const kinds = [
    "suffix_tion1",
    "suffix_tion1",
    "suffix_tion1",
    "suffix_tion1",
    "suffix_al1",
    "suffix_al1",
    "suffix_al1",
    "suffix_al1",
    "suffix_ize1",
    "suffix_ize1",
    "suffix_ize1",
    "suffix_ize1",
    "suffix_tion2",
    "suffix_tion2",
    "suffix_al2",
    "suffix_al2",
    "suffix_al2",
    "suffix_al2",
    "suffix_ize2",
    "suffix_ize2",
    "suffix_ize2",
    "suffix_ize2",
    "suffix_tion3",
    "suffix_tion3",
    "suffix_al3",
    "suffix_al3",
];

const regions = [
    "US",
    "UK",
    "US",
    "UK",
    "US",
    "UK",
    "US",
    "UK",
    "US",
    "UK",
    "US",
    "UK",
    "all",
    "all",
    "US",
    "UK",
    "US",
    "UK",
    "US",
    "UK",
    "US",
    "UK",
    "all",
    "all",
    "all",
    "all",
]

export default class VocStratCardMatching extends Component {
    componentDidMount = () => {
        const taskData = kinds.map((kind, i) => {

            let taskData = {type: "MULTISELECT"};
            if( ["us","uk"].includes(regions[i].toLowerCase()) ) {
                taskData.region = [regions[i].toLowerCase()]
            }
            return Object.assign({},
                taskData,
                {
                contentId: uuid.v4(),
                kind,
                "groups": [
                    {
                        "heading": "SUFFIX",
                        "group": 1
                    },
                    {
                        "heading": "EXAMPLE",
                        "group": 2
                    }
                ],
                choices: this.makeChoices(i),
                audio: [
                    {
                        type: "affix",
                        path: `common_vocstrat.suffix_${affixTranscripts[i]}`
                    },
                    {
                        type: "definition",
                        path: `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                    }
                ]
            });
        });
        let contentIds = taskData.map(({ contentId }) => contentId);
        const taskConfig = {
            type: "MultiSelect",
            params: {
                contentId: contentIds,
                "layout": "restricted",
                "stylePrefix": "vs1restricted",
                "shuffleChoices": true,
                "requiredClicks": 2,
                "isRestricted": true
            }
        };

        this.setState({
            taskData,
            taskConfig
        });
    };

    makeChoices = (ind) => {
        console.warn(ind,group1Choice1,group1Choice1[ind])
        return [
            {
                "choice": group1Choice1[ind].replace('*',''),
                "answer": group1Choice1[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group1Choice2[ind].replace('*',''),
                "answer": group1Choice2[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "dis",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice1[ind].replace('*',''),
                "answer": group2Choice1[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice2[ind].replace('*',''),
                "answer": group2Choice2[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "dis",
                "contentId": uuid.v4()
            }
        ]
    };

    makeNestedSpotlightText = (textArrays, audioKey) => {
        audioKey = audioKey ? audioKey + "." : "";
        return textArrays.map(textArr => {
            return textArr.map(t => {
                const slt = { text: t };
                return slt;
            });
        });
    };

    render() {
        if (this.state && this.state.taskData) {
            return (
                <div style={{ display: "flex" }}>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
                        <textarea
                            id="outputData"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
                        />
                    </div>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
                        <textarea
                            id="outputConfig"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
                        />
                    </div>
                </div>
            );
        }
        return <div />;
    }
}
