import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";


const wordTranscripts = [
    "dis",
    "re",
    "re",
    "mis",
    "memorize",
    "memorise",
    "Secretive",
    "Secretive",
    "Graduation",
    "Secretive",
    "education",
    "Secretive",
    "dis",
    "trans",
    "inventive",
    "subtraction",
    "invention",
    "Secretive",
]


const definitionTranscripts = [
"dis- means not",
"re- means again",
"re- means again",
"mis- means wrong",
"Memorize means to make stay in your memory.",
"Memorise means to make stay in your memory.",
"Secretive describes someone who often has secrets.",
"Secretive describes someone who often has secrets.",
"Graduation means the process of graduating.",
"Secretive describes someone who often has secrets.",
"Education is the process of becoming educated.",
"Secretive describes someone who often has secrets.",
"dis- means not",
"trans- means across or change",
"inventive means having the ability to invent",
"Subtraction is the process of subtracting.",
"An invention is something that is invented.",
"Secretive describes someone who often has secrets.",
]

const affixTranscripts = [
    "dis",
    "re",
    "re",
    "mis",
    "ize",
    "ise",
    "ive",
    "ive",
    "tion",
    "ive",
    "tion",
    "ive",
    "dis",
    "trans",
    "ive",
    "tion",
    "tion",
    "ive",
];

const group1Choice1 = [
    "dis-*",
    "dis-",
    "re-*",
    "re-",
    "-ize|to make*",
    "-ise|to make*",
    "-ize|to make",
    "-ise|to make",
    "-tion|thing, action, or process*",
    "-tion|thing, action, or process",
    "-tion|thing, action, or process*",
    "-tion|thing, action, or process",
    "dis-*",
    "dis-",
    "-ive|being a certain way*",
    "-ive|being a certain way",
    "-tion|thing, action, or process*",
    "-tion|thing, action, or process",
];

const group1Choice2 = [
    "re-",
    "re-*",
    "mis-",
    "mis-*",
    "-ive|being a certain way",
    "-ive|being a certain way",
    "-ive|being a certain way*",
    "-ive|being a certain way*",
    "-ive|being a certain way",
    "-ive|being a certain way*",
    "-ive|being a certain way",
    "-ive|being a certain way*",
    "trans-",
    "trans-*",
    "-tion|thing, action, or process",
    "-tion|thing, action, or process*",
    "-ive|being a certain way",
    "-ive|being a certain way*",
];
const group2Choice1 = [
    "not*",
    "not",
    "again*",
    "again",
    "memorize*",
    "memorise*",
    "memorize",
    "memorise",
    "graduation*",
    "graduation",
    "education*",
    "education",
    "wrong*",
    "wrong",
    "inventive*",
    "inventive",
    "invention*",
    "invention",
]

const group2Choice2 = [
    "again",
    "again*",
    "wrong",
    "wrong*",
    "secretive",
    "secretive",
    "secretive*",
    "secretive*",
    "secretive",
    "secretive*",
    "secretive",
    "secretive*",
    "across or change",
    "across or change*",
    "subtraction",
    "subtraction*",
    "secretive",
    "secretive*",
]

const kinds = [
    "prefix_dis1",
    "prefix_dis1",
    "prefix_re",
    "prefix_re",
    "suffix_ize",
    "suffix_ize",
    "suffix_ize",
    "suffix_ize",
    "suffix_tion1",
    "suffix_tion1",
    "suffix_tion2",
    "suffix_tion2",
    "prefix_dis2",
    "prefix_dis2",
    "suffix_ive",
    "suffix_ive",
    "suffix_tion3",
    "suffix_tion3",
];

const regions = [
    "all",
    "all",
    "all",
    "all",
    "US",
    "UK",
    "US",
    "UK",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
]

export default class VocStratCardMatching extends Component {
    componentDidMount = () => {
        const taskData = kinds.map((kind, i) => {
        const isPrefix = kinds[i].indexOf('prefix') > -1;
        const isSuffix = !isPrefix;
            let taskData = {type: "MULTISELECT"};
            if( ["us","uk"].includes(regions[i].toLowerCase()) ) {
                taskData.region = [regions[i].toLowerCase()]
            }
            return Object.assign({},
                taskData,
                {
                contentId: uuid.v4(),
                kind,
                "groups": [
                    {
                        "heading": kinds[i].indexOf('prefix') > -1 ? "PREFIX" : "SUFFIX",
                        "group": 1
                    },
                    {
                        "heading": kinds[i].indexOf('prefix') > -1 ? "MEANING" : "EXAMPLE",
                        "group": 2
                    }
                ],
                    /**
                     "prefix_dis1",
                     "prefix_re",
                     "suffix_ize",
                     "suffix_tion2",
                     "prefix_dis2",
                     "suffix_ive",
                     "suffix_tion3",
                     */
                choices: this.makeChoices(i),
                    correct: isPrefix
                        ? [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },{
                                    type: "sound",
                                    path: ['inter','un','in','ness','dis', 're','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                        ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                        : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                                }

                            ]
                        : [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },{
                                type: "sound",
                                path: ['inter','un','in','dis', 're','ize','ise','tion','ive','ness'].includes(wordTranscripts[i].toLowerCase())
                                    ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                    : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                            },
                            {
                                type: "sound",
                                path: "narrators.cmn_the_word"
                            },
                            {
                                type: "sound",
                                path: isPrefix
                                    ? `common_vocstrat.prefix_${wordTranscripts[i].toLowerCase()}`
                                    : ['ness','ize','ise','tion','ive','ness'].includes(wordTranscripts[i].toLowerCase())
                                        ? `common_vocstrat.suffix_${wordTranscripts[i].toLowerCase()}`
                                        : `words.${wordTranscripts[i].toLowerCase()}`
                            },
                            {
                                type: "sound",
                                path: "common_vocstrat.contains_the_suffix"
                            },
                            {
                                type: "sound",
                                path: `common_vocstrat.suffix_${affixTranscripts[i]}`
                            }

                        ],
                    solution:  isPrefix
                        ? [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },{
                                type: "sound",
                                path: ['inter','un','in','ness','dis', 're','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                    ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                    : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                            }
                        ]
                        : [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },
                            {
                                type: "sound",
                                path: ['inter','un','in','dis', 're','ize','ise','tion','ive','ness'].includes(wordTranscripts[i].toLowerCase())
                                    ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                    : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                            },
                            {
                                type: "sound",
                                path: "narrators.cmn_the_word"
                            },
                            {
                                type: "sound",
                                path: isPrefix
                                    ? `common_vocstrat.prefix_${wordTranscripts[i].toLowerCase()}`
                                    : ['ness','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                        ? `common_vocstrat.suffix_${wordTranscripts[i].toLowerCase()}`
                                        : `words.${wordTranscripts[i].toLowerCase()}`
                            },
                            {
                                type: "sound",
                                path: "common_vocstrat.contains_the_suffix"
                            },
                            {
                                type: "sound",
                                path: `common_vocstrat.suffix_${affixTranscripts[i]}`
                            }

                        ],
                audio: [
                    {
                        type: "affix",
                        path: isPrefix ? `common_vocstrat.prefix_${affixTranscripts[i]}` : `common_vocstrat.suffix_${affixTranscripts[i]}`
                    },
                    {
                        type: "definition",
                        // path: `common_vocstrat.${affixTranscripts[i]}_definition`
                        // path: `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                        path: ['inter','un','in','ness','dis', 're','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                            ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                            : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                    },
                    {
                        type: "word",
                        path: isPrefix
                            ? `common_vocstrat.prefix_${wordTranscripts[i].toLowerCase()}`
                            : ['ness','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                ? `common_vocstrat.suffix_${wordTranscripts[i].toLowerCase()}`
                                : `words.${wordTranscripts[i].toLowerCase()}`
                    },
                    {
                        type: "intro",
                        "path": `common_vocstrat.${
                            isPrefix
                                ? 'prefixes_give_clue_about_words_meaning'
                                : 'suffixes_give_clue_about_words_meaning'
                            }`
                    },
                    {
                        type: "prompt1",
                        "path": `common_vocstrat.${
                            isPrefix
                                ? 'choose_the_prefix'
                                : 'choose_the_suffix'
                            }`
                    },
                    {
                        type: "prompt2",
                        "path": `common_vocstrat.${
                            isPrefix
                                ? 'and_its_meaning'
                                : 'and_the_word_that_contains_it'
                            }`
                    }
                ]
            });
        });
        let contentIds = taskData.map(({ contentId }) => contentId);
        const taskConfig = {
            type: "MultiSelect",
            params: {
                contentId: contentIds,
                "layout": "restricted",
                "stylePrefix": "vs1restricted",
                "shuffleChoices": true,
                "requiredClicks": 2,
                "isRestricted": true
            }
        };

        this.setState({
            taskData,
            taskConfig
        });
    };

    makeChoices = (ind) => {
        console.warn(ind,group1Choice1,group1Choice1[ind])
        return [
            {
                "choice": group1Choice1[ind].replace('*',''),
                "answer": group1Choice1[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group1Choice2[ind].replace('*',''),
                "answer": group1Choice2[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "dis",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice1[ind].replace('*',''),
                "answer": group2Choice1[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice2[ind].replace('*',''),
                "answer": group2Choice2[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "dis",
                "contentId": uuid.v4()
            }
        ]
    };

    makeNestedSpotlightText = (textArrays, audioKey) => {
        audioKey = audioKey ? audioKey + "." : "";
        return textArrays.map(textArr => {
            return textArr.map(t => {
                const slt = { text: t };
                return slt;
            });
        });
    };

    render() {
        if (this.state && this.state.taskData) {
            return (
                <div style={{ display: "flex" }}>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
                        <textarea
                            id="outputData"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
                        />
                    </div>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
                        <textarea
                            id="outputConfig"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
                        />
                    </div>
                </div>
            );
        }
        return <div />;
    }
}
