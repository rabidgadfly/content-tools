import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

import { makeSpotlightText } from "../../../util/makeSpotlightText";
export const branchingAudio = {
    verb: "a_noun_part_speech_names_person_verb",
    verb_present_tense: "a_past_tense_verb_usually_ends_us",
    pronouns_plural: "pronouns_take_the_place_of_nouns",
    noun: "a_noun_part_speech_names_person_verb",
    verb_past_tense: "a_past_tense_verb_usually_ends_us",
    possessive_noun: "a_possessive_noun_shows_belonging_has",
    adjective: "adjectives_tell_more_about_nouns",
    adverb: "adverbs_are_words_describe_verb_some",
    interjection: "an_interjection_word_shows_strong_feeling",
    conjunction: "conjunctions_show_ideas_sentence_are_related",
    conjunction_noun_verb: "conjunctions_like_words_but_so_show",
    verb_future_tense: "some_prepositional_phrases_tell_when_action",
    pronouns_singular: "pronouns_take_the_place_of_nouns",
    prepositional_phrase: "some_prepositional_phrases_tell_when_action",
    simple_subject_predicate: "the_simple_subject_noun_pronoun_tells_predicate",
    relative_pronoun: "the_words_which_who_whose_whom"
}

const definitionTranscripts = [
    "in- can mean not.",
    "trans- means across or change.",
    "trans- means across or change.",
    "fore- means before or in front of.",
    "fore- means before or in front of.",
    "in- can mean not.",
]

const affixTranscripts = [
"in",
"trans",
"trans",
"fore",
"fore",
"in",
];

const group1Choice1 = [
"in-*",
"in-",
"trans-*",
"trans-",
"fore-*",
"fore-",
];

const group1Choice2 = [
"trans-",
"trans-*",
"fore-",
"fore-*",
"in-",
"in-*",
];

const group2Choice1 = [
    "not*",
    "not",
    "across or change*",
    "across or change",
    "before or in front of*",
    "before or in front of",
]

const group2Choice2 = [
    "across or change",
    "across or change*",
    "before or in front of",
    "before or in front of*",
    "not",
    "not*",
]

const kinds = [
    "prefix_in",
    "prefix_in",
    "prefix_trans",
    "prefix_trans",
    "prefix_fore",
    "prefix_fore",
];

const regions = [
"all",
"all",
"all",
"all",
"all",
"all",
]

export default class VocStratCardMatching extends Component {
    componentDidMount = () => {
        const taskData = kinds.map((kind, i) => {

            let taskData = {type: "MULTISELECT"};
            if( ["us","uk"].includes(regions[i].toLowerCase()) ) {
                taskData.region = [regions[i].toLowerCase()]
            }
            return Object.assign({},
                taskData,
                {
                contentId: uuid.v4(),
                kind,
                "groups": [
                    {
                        "heading": "PREFIX",
                        "group": 1
                    },
                    {
                        "heading": "MEANING",
                        "group": 2
                    }
                ],
                choices: this.makeChoices(i),
                audio: [
                    {
                        type: "affix",
                        path: `common_vocstrat.suffix_${affixTranscripts[i]}`
                    },
                    {
                        type: "definition",
                        path: `common_vocstrat.${affixTranscripts[i]}_definition`
                        // path: `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                    }
                ]
            });
        });
        let contentIds = taskData.map(({ contentId }) => contentId);
        const taskConfig = {
            type: "MultiSelect",
            params: {
                contentId: contentIds,
                "layout": "restricted",
                "stylePrefix": "vs1restricted",
                "shuffleChoices": true,
                "requiredClicks": 2,
                "isRestricted": true
            }
        };

        this.setState({
            taskData,
            taskConfig
        });
    };

    makeChoices = (ind) => {
        console.warn(ind,group1Choice1,group1Choice1[ind])
        return [
            {
                "choice": group1Choice1[ind].replace('*',''),
                "answer": group1Choice1[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group1Choice2[ind].replace('*',''),
                "answer": group1Choice2[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "dis",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice1[ind].replace('*',''),
                "answer": group2Choice1[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice2[ind].replace('*',''),
                "answer": group2Choice2[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "dis",
                "contentId": uuid.v4()
            }
        ]
    };

    makeNestedSpotlightText = (textArrays, audioKey) => {
        audioKey = audioKey ? audioKey + "." : "";
        return textArrays.map(textArr => {
            return textArr.map(t => {
                const slt = { text: t };
                return slt;
            });
        });
    };

    render() {
        if (this.state && this.state.taskData) {
            return (
                <div style={{ display: "flex" }}>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
                        <textarea
                            id="outputData"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
                        />
                    </div>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
                        <textarea
                            id="outputConfig"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
                        />
                    </div>
                </div>
            );
        }
        return <div />;
    }
}
