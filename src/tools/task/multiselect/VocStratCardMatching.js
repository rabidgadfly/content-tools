import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";


const wordTranscripts = [
    "in",
    "inter",
    "inter",
    "un",
    "personal",
    "Equality",
    "personality",
    "Sleepiness",
    "National",
    "Sleepiness",
    "inter",
    "un",
    "willingness",
    "global",
    "un",
    "inter",
]


const definitionTranscripts = [
    "in means not",
    "inter means together",
    "inter means together",
    "un means not",
    "Personal means having to do with one person.",
    "Equality means the state of being equal.",
    "Personality is the personal way each individual is.",
    "Sleepiness is the quality of being sleepy.",
    "National means having to do with a nation.",
    "Sleepiness is the quality of being sleepy.",
    "inter means together",
    "un means not",
    "Willingness is the quality of being willing.",
    "Global means having to do with the whole globe.",
    "un means not",
    "inter means together",
]

const affixTranscripts = [
    "in",
    "inter",
    "inter",
    "un",
    "al",
    "ity",
    "ity",
    "ness",
    "al",
    "ness",
    "inter",
    "un",
    "ness",
    "al",
    "un",
    "inter",
];

const group1Choice1 = [
    "in-",
    "in-",
    "inter-",
    "inter-",
    "-al|having to do with",
    "-al|having to do with",
    "-ity|state of being",
    "-ity|state of being",
    "-al|having to do with",
    "-al|having to do with",
    "inter-",
    "inter-",
    "-ness|names a quality",
    "-ness|names a quality",
    "un-",
    "un-",
];

const group1Choice2 = [
    "inter-",
    "inter-",
    "un-",
    "un-",
    "-ity|state of being",
    "-ity|state of being",
    "-ness|names a quality",
    "-ness|names a quality",
    "-ness|names a quality",
    "-ness|names a quality",
    "un-",
    "un-",
    "-al|having to do with",
    "-al|having to do with",
    "inter-",
    "inter-",
];
const group2Choice1 = [
    "not",
    "not",
    "together",
    "together",
    "personal",
    "personal",
    "personality",
    "personality",
    "national",
    "national",
    "together",
    "together",
    "willingness",
    "willingness",
    "not",
    "not",
]

const group2Choice2 = [
    "together",
    "together",
    "not",
    "not",
    "equality",
    "equality",
    "sleepiness",
    "sleepiness",
    "sleepiness",
    "sleepiness",
    "not",
    "not",
    "global",
    "global",
    "together",
    "together",
]

const kinds = [
    "prefix_in",
    "prefix_in",
    "prefix_inter1",
    "prefix_inter1",
    "suffix_al1",
    "suffix_al1",
    "suffix_ity",
    "suffix_ity",
    "suffix_al2",
    "suffix_al2",
    "prefix_inter2",
    "prefix_inter2",
    "suffix_ness",
    "suffix_ness",
    "prefix_un",
    "prefix_un",
];

const regions = [
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
    "all",
]

export default class VocStratCardMatching extends Component {
    componentDidMount = () => {
        const taskData = kinds.map((kind, i) => {
        const isPrefix = kinds[i].indexOf('prefix') > -1;
        const isSuffix = !isPrefix;
            let taskData = {type: "MULTISELECT"};
            if( ["us","uk"].includes(regions[i].toLowerCase()) ) {
                taskData.region = [regions[i].toLowerCase()]
            }
            return Object.assign({},
                taskData,
                {
                contentId: uuid.v4(),
                kind,
                "groups": [
                    {
                        "heading": kinds[i].indexOf('prefix') > -1 ? "PREFIX" : "SUFFIX",
                        "group": 1
                    },
                    {
                        "heading": kinds[i].indexOf('prefix') > -1 ? "MEANING" : "EXAMPLE",
                        "group": 2
                    }
                ],
                    /**
                     "prefix_dis1",
                     "prefix_re",
                     "suffix_ize",
                     "suffix_tion2",
                     "prefix_dis2",
                     "suffix_ive",
                     "suffix_tion3",
                     */
                choices: this.makeChoices(i),
                    correct: isPrefix
                        ? [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },{
                                    type: "sound",
                                    path: ['inter','un','in','ness','dis', 're','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                        ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                        : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                                }

                            ]
                        : [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },{
                                type: "sound",
                                path: ['inter','un','in','dis', 're','ize','ise','tion','ive','ness'].includes(wordTranscripts[i].toLowerCase())
                                    ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                    : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                            },
                            {
                                type: "sound",
                                path: "narrators.cmn_the_word"
                            },
                            {
                                type: "sound",
                                path: isPrefix
                                    ? `common_vocstrat.prefix_${wordTranscripts[i].toLowerCase()}`
                                    : ['ness','ize','ise','tion','ive','ness'].includes(wordTranscripts[i].toLowerCase())
                                        ? `common_vocstrat.suffix_${wordTranscripts[i].toLowerCase()}`
                                        : `words.${wordTranscripts[i].toLowerCase()}`
                            },
                            {
                                type: "sound",
                                path: "common_vocstrat.contains_the_suffix"
                            },
                            {
                                type: "sound",
                                path: `common_vocstrat.suffix_${affixTranscripts[i]}`
                            }

                        ],
                    solution:  isPrefix
                        ? [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },{
                                type: "sound",
                                path: ['inter','un','in','ness','dis', 're','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                    ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                    : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                            }
                        ]
                        : [
                            {
                                "type": "event.hideObjects",
                                "args": {
                                    "objects": [
                                        {
                                            "key": "incorrect"
                                        }
                                    ]
                                }
                            },
                            {
                                type: "sound",
                                path: ['inter','un','in','dis', 're','ize','ise','tion','ive','ness'].includes(wordTranscripts[i].toLowerCase())
                                    ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                                    : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                            },
                            {
                                type: "sound",
                                path: "narrators.cmn_the_word"
                            },
                            {
                                type: "sound",
                                path: isPrefix
                                    ? `common_vocstrat.prefix_${wordTranscripts[i].toLowerCase()}`
                                    : ['ness','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                        ? `common_vocstrat.suffix_${wordTranscripts[i].toLowerCase()}`
                                        : `words.${wordTranscripts[i].toLowerCase()}`
                            },
                            {
                                type: "sound",
                                path: "common_vocstrat.contains_the_suffix"
                            },
                            {
                                type: "sound",
                                path: `common_vocstrat.suffix_${affixTranscripts[i]}`
                            }

                        ],
                audio: [
                    {
                        type: "affix",
                        path: isPrefix ? `common_vocstrat.prefix_${affixTranscripts[i]}` : `common_vocstrat.suffix_${affixTranscripts[i]}`
                    },
                    {
                        type: "definition",
                        // path: `common_vocstrat.${affixTranscripts[i]}_definition`
                        // path: `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                        path: ['inter','un','in','ness','dis', 're','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                            ? `common_vocstrat.${wordTranscripts[i].toLowerCase()}_definition`
                            : `common_vocstrat.${filenameGenerator(definitionTranscripts[i])}`
                    },
                    {
                        type: "word",
                        path: isPrefix
                            ? `common_vocstrat.prefix_${wordTranscripts[i].toLowerCase()}`
                            : ['ness','ize','ise','tion','ive'].includes(wordTranscripts[i].toLowerCase())
                                ? `common_vocstrat.suffix_${wordTranscripts[i].toLowerCase()}`
                                : `words.${wordTranscripts[i].toLowerCase()}`
                    },
                    {
                        type: "intro",
                        "path": `common_vocstrat.${
                            isPrefix
                                ? 'prefixes_give_clue_about_words_meaning'
                                : 'suffixes_give_clue_about_words_meaning'
                            }`
                    },
                    {
                        type: "prompt1",
                        "path": `common_vocstrat.${
                            isPrefix
                                ? 'choose_the_prefix'
                                : 'choose_the_suffix'
                            }`
                    },
                    {
                        type: "prompt2",
                        "path": `common_vocstrat.${
                            isPrefix
                                ? 'and_its_meaning'
                                : 'and_the_word_that_contains_it'
                            }`
                    }
                ]
            });
        });
        let contentIds = taskData.map(({ contentId }) => contentId);
        const taskConfig = {
            type: "MultiSelect",
            params: {
                contentId: contentIds,
                "layout": "restricted",
                "stylePrefix": "vs1restricted",
                "shuffleChoices": true,
                "requiredClicks": 2,
                "isRestricted": true
            }
        };

        this.setState({
            taskData,
            taskConfig
        });
    };

    makeChoices = (ind) => {
        console.warn(ind,group1Choice1,group1Choice1[ind])
        return [
            {
                "choice": group1Choice1[ind].replace('*',''),
                "answer": group1Choice1[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group1Choice2[ind].replace('*',''),
                "answer": group1Choice2[ind].indexOf('*') > 1,
                "group": 1,
                // "kind": "dis",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice1[ind].replace('*',''),
                "answer": group2Choice1[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "inter",
                "contentId": uuid.v4()
            },
            {
                "choice": group2Choice2[ind].replace('*',''),
                "answer": group2Choice2[ind].indexOf('*') > 1,
                "group": 2,
                // "kind": "dis",
                "contentId": uuid.v4()
            }
        ]
    };

    makeNestedSpotlightText = (textArrays, audioKey) => {
        audioKey = audioKey ? audioKey + "." : "";
        return textArrays.map(textArr => {
            return textArr.map(t => {
                const slt = { text: t };
                return slt;
            });
        });
    };

    render() {
        if (this.state && this.state.taskData) {
            return (
                <div style={{ display: "flex" }}>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
                        <textarea
                            id="outputData"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
                        />
                    </div>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
                        <textarea
                            id="outputConfig"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
                        />
                    </div>
                </div>
            );
        }
        return <div />;
    }
}
