import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { makeSpotlightText } from "../../../util/makeSpotlightText";

const dropZones = [
  "In the early 1900s",
  "From the 1900s to the 1970s",
  "After 1970"
];

const choices = [
  "most African Americans lived in southern states.",
  "African Americans moved north for better jobs.",
  "the black population in the South was much smaller."
];

const kinds = ["1", "2", "3"];

const spotlightText = makeSpotlightText([
  "Think about the order of events in the Great Migration."
]);

const makeChoices = choices =>
  choices.map((c, i) => ({
    choice: c,
    kind: kinds[i],
    contentId: uuid.v4()
  }));

const makeDropZones = () =>
  dropZones.map((dz, i) => ({
    dropZone: kinds[i],
    label: dropZones[i]
  }));

console.log("makedz", makeDropZones());

const taskData = {
  type: "GRAPHICORGANIZER",
  contentId: uuid.v4(),
  kind: "t3",
  spotlightText: spotlightText,
  dropZones: makeDropZones(),
  choices: makeChoices(choices)
};

let contentIds = taskData.contentId;

const taskConfig = {
  type: "GraphicOrganizer",
  params: {
    contentId: contentIds,
    layout: [
      {
        column: ["text_spotlightText", "choiceColumnBorders"]
      }
    ],
    applyHighlight: false,
    isMultiRound: true,
    stylePrefix: "prefmng",
    shuffleChoices: true,
    useSubmitButton: false,
    useDropZoneNumbers: true,
    suppressIncorrectDefault: true
  }
};

export default class Txtcnnct2GraphicOrg extends Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
          <textarea
            id="outputData"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskData))}
          />
        </div>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
          <textarea
            id="outputConfig"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskConfig))}
          />
        </div>
      </div>
    );
  }
}
