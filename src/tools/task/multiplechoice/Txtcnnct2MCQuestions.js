import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

const spotlightText = makeSpotlightText([
  "What does the map help readers understand?",
  "What does the map help readers understand?",
  "Before the Great Migration, where did most African Americans live and work?",
  "Before the Great Migration, where did most African Americans live and work?",
  "Which best describes one reason why people moved from the South?",
  "Which best describes one reason why people moved from the South?"
]);

const choices = [
  [
    "which states were part of the Great Migration*",
    "why some people decided not to move",
    "how children felt about moving away from friends",
    "whether families took a train or drove a car"
  ],
  [
    "where people moved in the Great Migration*",
    "why some families decided not to move",
    "how people felt about leaving friends behind",
    "whether people drove in a car or took a train"
  ],
  [
    "on southern farms*",
    "in cities in the West",
    "on farms in the North",
    "in northern cities"
  ],
  [
    "on farms in the South*",
    "in western cities",
    "on northern farms",
    "in cities in the North"
  ],
  [
    "to find better paying jobs*",
    "to look for more farmland",
    "to stay in a familiar place",
    "to escape busy southern cities"
  ],
  [
    "to find jobs with better pay*",
    "to look for better farmland",
    "to stay in a familiar place",
    "to leave busy southern cities"
  ]
];

const kinds = [
  "image_1_A",
  "image_1_B",
  "detailinfo_2_A",
  "detailinfo_2_B",
  "causeff_3_A",
  "causeff_3_B"
];

const contentPool = ["A", "B", "A", "B", "A", "B"];

const makeChoices = choices =>
  choices.map(c => {
    const answer = c.includes("*");
    return {
      choice: c.replace("*", ""),
      answer,
      contentId: uuid.v4()
    };
  });

const taskData = choices.map((taskChoices, i) => {
  return {
    type: "MULTIPLECHOICE",
    contentId: uuid.v4(),
    // contentPool: contentPool[i],
    kind: kinds[i],
    spotlightText: spotlightText[i],
    choices: makeChoices(choices[i])
  };
});

let contentIds = taskData.map(({ contentId }) => contentId);

const taskConfig = {
  type: "MultipleChoice",
  params: {
    contentId: contentIds,
    layout: [
      {
        column: ["text_spotlightText", "choiceColumnBorders"]
      }
    ],
    stylePrefix: "txtcnnt2_mc",
    shuffleChoices: false,
    shuffleContentIds: true
  }
};

export default class Txtcnnct2MCQuestions extends Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
          <textarea
            id="outputData"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskData))}
          />
        </div>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
          <textarea
            id="outputConfig"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskConfig))}
          />
        </div>
      </div>
    );
  }
}
