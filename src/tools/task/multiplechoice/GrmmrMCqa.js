import React, {Component} from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import {filenameGenerator} from "../../../util/filenameGenerator";
import {makeSpotlightText} from "../../../util/makeSpotlightText";

let bubbleImages = {
    gives_a_choice: "grmmr_tag_choice",
    shows_difference: "grmmr_tag_difference",
    how: "grmmr_tag_how",
    how_many: "grmmr_tag_how_many",
    adds_information: "grmmr_tag_information",
    interjection: "grmmr_tag_interjection",
    simple_predicate: "grmmr_tag_predicate",
    whats_the_result: "grmmr_tag_result",
    simple_subject: "grmmr_tag_subject",
    tells_the_result: "grmmr_tag_tells_result",
    what_kind: "grmmr_tag_what_kind",
    when: "grmmr_tag_when",
    when_q: "grmmr_tag_when_2",
    where: "grmmr_tag_where",
    which_one: "grmmr_tag_which_one",
    why: "grmmr_tag_why"
};

const spotlightImages = [
    "why",
    "whats_the_result",
    "when",
    "why",
    "whats_the_result",
    "when",
    "why",
    "whats_the_result",
    "when",
    "why",
];

const spotlightTexts = [
"You can <h>either</h> read a book ____.",
"<h>Neither</h> my hands ____ are warm.",
"<h>No sooner</h> had I left the house ____.",
"She painted <h>both</h> the hall ____.",
"<h>Not</h> only is she successful, ____.",
"<h>Neither</h> the cow ____ is asleep.",
"Please choose <h>either</h> a sandwich ____.",
"Did you visit <h>both</h> your aunt ____?",
"<h>Not only</h> do we sell trucks, ____.",
"<h>No sooner</h> had I arrived to school ____.",
];

// const prompts = [
//     "The second sentence shows that different information comes next.",
//         "The second sentence shows that different information comes next.",
//         "The second sentence shows that different information comes next.",
//         "The second sentence adds information.",
//         "The second sentence adds information.",
//         "The second sentence adds information.",
//         "The second sentence tells the result.",
//         "The second sentence tells the result.",
//         "The second sentence tells the result.",
//         "The second sentence gives a choice.",
//         "The second sentence gives a choice.",
//         "The second sentence gives a choice.",
// ];

const choices = [
["<h>or</h> listen music*",	"<h>both</h> listen to music",	"<h>also</h> listen to music","<h>but</h> listen to music"],
["<h>nor</h> my feet*",	"<h>or</h> my feet",	"<h>and</h> my feet","<h>than</h> my feet"],
["<h>than</h> it began raining*",	"<h>or</h> it began raining",	"<h>also</h> it began raining","<h>but</h> it began raining"],
["<h>and</h> the door*",	"<h>or</h> the door",	"<h>nor</h> the door","<h>than</h> the door"],
["<h>but</h> also she is kind*",	"<h>nor</h> is she kind",	"<h>and</h> she is kind", "<h>or</h> she is kind"],
["<h>nor</h> its calf*",	"<h>or</h> its calf",	"<h>and</h> its calf", "<h>but also</h> its calf"],
["<h>or</h> some soup*",	"<h>and</h> some soup",	"<h>nor</h> some soup", "<h>but also</h> some soup"],
["<h>and</h> your cousin*",	"<h>or</h> your cousin",	"<h>nor</h> your cousin", "<h>but also</h> your cousin"],
["<h>but</h> also we sell cars*",	"<h>and</h> we sell cars",	"<h>nor</h> do we sell cars", "<h>or</h> we sell cars"],
["<h>than</h> the bell rang*",	"<h>or</h> the bell rang",	"<h>and</h> the bell rang", "<h>nor</h> the bell rang"],
];

const kinds = [
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
];

export default class GrmmrMCqa extends Component {
    componentDidMount = () => {
        const taskData = this.getTaskData();
        const contentIds = taskData.map(({contentId}) => contentId);

        this.setState({
            contentIds: contentIds,
            taskData: taskData,
            taskConfig: this.getTaskConfig(contentIds)
        });
    };

    makeChoices = choices => {
        return choices.map(c => {
            const answer = c.includes("*");
            const choiceMinusToken = c.replace("*", "");
            return {
                choice: choiceMinusToken,
                // text: choiceMinusToken.replace('<h>','').replace('</h>',''),
                answer,
                contentId: uuid.v4()
            };
        });
    };

    makeSpotlightTextWithHighlight = slt => {
        slt = slt.split("~");
        return [
            {
                text: slt[0].trim()
            },
            {
                text: slt[1].trim()
            },
            {
                text: slt[2].trim()
            }
        ];
    };

    getTaskData = () => {
        return choices.map((taskChoices, i) => {
            const spotlightText = [{text: spotlightTexts[i]}];

            // const spotlightText = this.makeSpotlightTextWithHighlight(
            //   spotlightTexts[i]
            // );
            return {
                type: "MULTIPLECHOICE",
                contentId: uuid.v4(),
                kind: kinds[i],
                // spotlightImage: [{image: `spotlight_tags.${spotlightImages[i]}`}],
                spotlightSentence: spotlightText,
                choices: this.makeChoices(choices[i]),
                // audio: [
                //   {
                //     type: "prompt",
                //     path: `common_grmmr.${filenameGenerator(prompts[i])}`
                //   }
                // ]
            };
        });
    };

    getTaskConfig = contentIds => {
        return {
            type: "MultipleChoice",
            params: {
                contentId: contentIds,
                layout: [
                    {
                        column: [
                            "spotlightImage",
                            "text_spotlightText",
                            "choiceColumnBorders"
                        ]
                    }
                ],
                stylePrefix: "grmmr1_mc",
                shuffleChoices: false,
                shuffleContentIds: true
            }
        };
    };

    render() {
        if (this.state && this.state.taskData) {
            return (
                <div style={{display: "flex"}}>
                    <div style={{width: "400px", padding: "10px"}}>
                        <h3 style={{fontFamily: "Helvetica"}}>Data</h3>
                        <textarea
                            id="outputData"
                            rows="40"
                            style={{width: "100%"}}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
                        />
                    </div>
                    <div style={{width: "400px", padding: "10px"}}>
                        <h3 style={{fontFamily: "Helvetica"}}>Config</h3>
                        <textarea
                            id="outputConfig"
                            rows="40"
                            style={{width: "100%"}}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
                        />
                    </div>
                </div>
            );
        }
        return <div/>;
    }
}
