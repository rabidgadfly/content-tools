import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

import { makeSpotlightText } from "../../../util/makeSpotlightText";
export const branchingAudio = {
    verb: "a_noun_part_speech_names_person_verb",
    verb_present_tense: "a_past_tense_verb_usually_ends_us",
    pronouns_plural: "pronouns_take_the_place_of_nouns",
    noun: "a_noun_part_speech_names_person_verb",
    verb_past_tense: "a_past_tense_verb_usually_ends_us",
    possessive_noun: "a_possessive_noun_shows_belonging_has",
    adjective: "adjectives_tell_more_about_nouns",
    adverb: "adverbs_are_words_describe_verb_some",
    interjection: "an_interjection_word_shows_strong_feeling",
    conjunction: "conjunctions_show_ideas_sentence_are_related",
    conjunction_noun_verb: "conjunctions_like_words_but_so_show",
    verb_future_tense: "some_prepositional_phrases_tell_when_action",
    pronouns_singular: "pronouns_take_the_place_of_nouns",
    prepositional_phrase: "some_prepositional_phrases_tell_when_action",
    simple_subject_predicate: "the_simple_subject_noun_pronoun_tells_predicate",
    relative_pronoun: "the_words_which_who_whose_whom"
}


const wordSpotlights = [
["Under the warm summer sun"],
["In the winter"],
["along the beach"],
["peacefully"],
["recently"],
["fast"],
["loud"],
["green"],
["Nine"],
["Several"],
["Those"],
["That"],
["nearby"],
["inside"],
["tomorrow"],
["now"],
["carefully"],
["beautifully"],
["No"],
["Brr"],
["Ick"],
["do"],
["cold"],
["terrible"],
["Greg and I"],
["The dog and I"],
["My friend and I"],
["The boy and girl"],
["The flowers"],
["The birds"],
["will pick"],
["will sing"],
["hatched"],
["sits"],
["picked"],
["yawns"],
["because"],
["plant"],
["watered"],
["because"],
["story"],
["fell"],
];

const sentences = [
"Under the warm summer sun, the kittens rest.",
"In the winter I ski.",
"They will run along the beach.",
"An owl rests peacefully.",
"I saw my friend recently.",
"The dolphins swim fast.",
"A loud bell rings.",
"A green frog jumps.",
"Nine bells ring.",
"Several frogs jump.",
"Those bells ring.",
"That frog jumps.",
"The foxes live nearby.",
"She went inside.",
"We will visit tomorrow.",
"The play begins now.",
"She types carefully.",
"He sings beautifully.",
"No! Don't do that!",
"Brr, it's cold in here.",
"Ick! This soup tastes terrible.",
"No! Don't do that!",
"Brr, it's cold in here.",
"Ick! This soup tastes terrible.",
"Greg and I eat.",
"The dog and I run.",
"My friend and I danced.",
"The boy and girl chat.",
"The flowers grow.",
"The birds flew.",
"They will pick some flowers.",
"Clem will sing quietly.",
"Those eggs hatched.",
"Bella sits outside at this moment.",
"She picked a snack.",
"He yawns right now.",
"The plant grew because we watered it.",
"The plant grew because we watered it.",
"The plant grew because we watered it.",
"The story ended because we fell asleep.",
"The story ended because we fell asleep.",
"The story ended because we fell asleep.",
];

// const prompts = [
//   "Who does the computer belong to?",
//   "What does the tail belong to?",
//   "Who does the poem belong to?",
//   "What do the leaves belong to?",
//   "What do the petals belong to?",
//   "Who does the joke belong to?",
//   "Who does the pie belong to?"
// ];

const solutions = [
"The prepositional phrase is UNDER THE WARM SUMMER SUN.",
"The prepositional phrase is IN THE WINTER.",
"The prepositional phrase is ALONG THE BEACH.",
"This sentence does not have a prepositional phrase.",
"This sentence does not have a prepositional phrase.",
"This sentence does not have a prepositional phrase.",
"LOUD tells what kind.",
"GREEN tells what kind.",
"NINE tells how many.",
"SEVERAL tells how many.",
"THOSE tells which one.",
"THAT tells which one.",
"NEARBY tells where.",
"INSIDE tells where.",
"TOMORROW tells when.",
"NOW tells when.",
"CAREFULLY tells how.",
"BEAUTIFULLY tells how.",
"NO is an interjection in this sentence.",
"BRR is an interjection in this sentence.",
"ICK is an interjection in this sentence.",
"DO is not an interjection in this sentence.",
"COLD is not an interjection in this sentence.",
"TERRIBLE is not an interjection in this sentence.",
"WE replaces Greg and I.",
"WE replaces the dog and I.",
"WE replaces my friend and I.",
"THEY replaces the boy and girl.",
"THEY replaces the flowers.",
"THEY replaces the birds.",
"This happens LATER in the future, so the verb is in the future tense.",
"This happens LATER in the future, so the verb is in the future tense.",
"This ALREADY happened, so the verb is in the past tense.",
"This happens NOW, so the verb is in the present tense.",
"This ALREADY happened, so the verb is in the past tense.",
"This happens NOW, so the verb is in the present tense.",
"BECAUSE is a conjunction that tells why.",
"PLANT and WE are nouns in this sentence.",
"GREW and WATERED are verbs in this sentence.",
"BECAUSE is a conjunction that tells why.",
"STORY and WE are nouns in this sentence.",
"ENDED and FELL are verbs in this sentence.",
];

const choices = [
    ["yes*",	"no"],
    ["yes*",	"no"],
    ["yes*",	"no"],
    ["yes",	"no*"],
    ["yes",	"no*"],
    ["yes",	"no*"],
    ["What Kind*", "How Many", "Which One"],
    ["What Kind*", "How Many", "Which One"],
    ["What Kind", "How Many*", "Which One"],
    ["What Kind", "How Many*", "Which One"],
    ["What Kind", "How Many", "Which One*"],
    ["What Kind", "How Many", "Which One*"],
    ["Where*","When","How"],
    ["Where*","When","How"],
    ["Where","When*","How"],
    ["Where","When*","How"],
    ["Where","When","How*"],
    ["Where","When","How*"],
    ["interjection*","not an interjection"],
    ["interjection*","not an interjection"],
    ["interjection*","not an interjection"],
    ["interjection","not an interjection*"],
    ["interjection","not an interjection*"],
    ["interjection","not an interjection*"],
    ["We*","They"],
    ["We*","They"],
    ["We*","They"],
    ["We","They*"],
    ["We","They*"],
    ["We","They*"],
    ["Past (Already)", "Present (Now)", "Future (Later)*"],
    ["Past (Already)", "Present (Now)", "Future (Later)*"],
    ["Past (Already)*", "Present (Now)", "Future (Later)"],
    ["Past (Already)", "Present (Now)*", "Future (Later)"],
    ["Past (Already)*", "Present (Now)", "Future (Later)"],
    ["Past (Already)", "Present (Now)*", "Future (Later)"],
    ["conjunction*", "noun", "verb"],
    ["conjunction", "noun*", "verb"],
    ["conjunction", "noun", "verb*"],
    ["conjunction*", "noun", "verb"],
    ["conjunction", "noun*", "verb"],
    ["conjunction", "noun", "verb*"],
];

const kinds = [
"prepositional_phrase",
"prepositional_phrase",
"prepositional_phrase",
"prepositional_phrase",
"prepositional_phrase",
"prepositional_phrase",
"adjective",
"adjective",
"adjective",
"adjective",
"adjective",
"adjective",
"adverb",
"adverb",
"adverb",
"adverb",
"adverb",
"adverb",
"interjection",
"interjection",
"interjection",
"interjection",
"interjection",
"interjection",
"pronouns_plural",
"pronouns_plural",
"pronouns_plural",
"pronouns_plural",
"pronouns_plural",
"pronouns_plural",
"verb_future_tense",
"verb_future_tense",
"verb_future_tense",
"verb_future_tense",
"verb_future_tense",
"verb_future_tense",
"conjunction_noun_verb",
"conjunction_noun_verb",
"conjunction_noun_verb",
"conjunction_noun_verb",
"conjunction_noun_verb",
"conjunction_noun_verb",
];

export default class GrmmrSortingBoxes extends Component {
    componentDidMount = () => {
        // const spotlightText = this.makeNestedSpotlightText(wordSpotlights);
        const spotlightSentence = makeSpotlightText(
            this.highlightWords(sentences, wordSpotlights)
        );
        const taskData = choices.map((taskChoices, i) => {
            let answerChoice = taskChoices
                .find(tc => tc.indexOf("*") > -1)
                .replace("*", "");
            return {
                type: "MULTIPLECHOICE",
                contentId: uuid.v4(),
                kind: kinds[i],
                spotlightSentence: spotlightSentence[i],
                // spotlightText: spotlightText[i],
                choices: this.makeChoices(taskChoices),
                audio: [
                    {
                        type: "intro",
                        path: `common_grmmr.${branchingAudio[kinds[i]]}`
                    },
                    {
                        type: "explanation",
                        path: `common_grmmr.${filenameGenerator(solutions[i])}`
                    }
                ]
            };
        });
        let contentIds = taskData.map(({ contentId }) => contentId);
        const taskConfig = {
            type: "MultipleChoice",
            params: {
                contentId: contentIds,
                layout: [
                    {
                        column: ["text_spotlightSentence", "sortingBoxes"]
                    }
                ],
                stylePrefix: "grmmr1_sort",
                shuffleChoices: false,
                shuffleContentIds: true
            }
        };

        this.setState({
            taskData,
            taskConfig
        });
    };

    highlightWords = (sentences, words) => {
        return sentences.map((s, i) => {
            let result = s;
            words[i].forEach(word => {
                result = result.replace(word, `<h>${word}</h>`);
            });
            return result;
        });
    };

    makeChoices = choices => {
        return choices.map(c => {
            const answer = c.includes("*");
            return {
                choice: c.replace("*", ""),
                answer,
                contentId: uuid.v4()
            };
        });
    };

    makeNestedSpotlightText = (textArrays, audioKey) => {
        audioKey = audioKey ? audioKey + "." : "";
        return textArrays.map(textArr => {
            return textArr.map(t => {
                const slt = { text: t };
                return slt;
            });
        });
    };

    render() {
        if (this.state && this.state.taskData) {
            return (
                <div style={{ display: "flex" }}>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
                        <textarea
                            id="outputData"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
                        />
                    </div>
                    <div style={{ width: "400px", padding: "10px" }}>
                        <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
                        <textarea
                            id="outputConfig"
                            rows="40"
                            style={{ width: "100%" }}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
                        />
                    </div>
                </div>
            );
        }
        return <div />;
    }
}
