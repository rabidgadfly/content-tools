import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";
//Unit 4: Word Sort (achieve, attempt, challenging, compete, however)
const definitions = {
    achieve: "to_achieve_is_succeed_doing_something",
    although: "although_signal_word_means_same_thing",
    candidate: "a_candidate_someone_who_trying_get",
    office: "an_office_important_job_often_government",
    represent: "to_represent_people_do_say_things",
    policy: "a_policy_group_rules_plans_used",
    attempt: "to_attempt_is_try_do_something",
    benefit: "a_benefit_is_good_helpful_result",
    challenging: "challenging_means_difficult",
    conclude: "to_conclude_decide_after_thinking_about",
    compete: "to_compete_work_against_others_try",
    during: "during_signal_word_means_through_period",
    however: "however_signal_word_means_same_thing",
    improve: "to_improve_make_better_become_better",
    physical: "physical_describes_activities_actions_have_do",
    commonly: "commonly_means_the_same_thing_often",
    environment: "environment_land_water_air_living_things",
    extinction: "extinction_when_entire_type_plant_animal",
    impact: "an_impact_is_a_result",
    species: "a_species_is_kind_plant_animal",
    culture: "a_culture_groups_values_way_life",
    generation: "a_generation_group_people_who_are",
    inaddition: "in_addition_phrase_means_same_thing",
    theme: "a_theme_central_message_lesson_story",
    traditional: "traditional_describes_traditions_old_ways_thinking",
    estimate: "to_estimate_guess_amount_value_using",
    habitat: "a_habitat_natural_place_where_plant",
    migration: "migration_when_many_animals_people_move",
    structure: "a_structure_something_built_putting_parts",
    therefore: "therefore_signal_word_means_same_thing",
    calories: "a_calorie_unit_measuring_much_energy",
    despite: "despite_signal_word_means_even_with",
    negative: "negative_describes_something_that_is_bad",
    nutrients: "nutrients_are_substances_help_plants_animals",
    process: "to_process_change_something_one_form",
    conflict: "a_conflict_struggle_between_people_things",
    issue: "an_issue_problem_important_topic_people",
    perspective: "a_perspective_way_looking_understanding_something",
    resolution: "the_resolution_story_final_part_when",
    witness: "to_witness_an_event_see_happens",
    consequence: "a_consequence_what_happens_result_action",
    productive: "productive_means_producing_lot_getting_lot",
    progress: "progress_means_improvement_getting_closer_achieving",
    innovation: "an_innovation_new_idea_tool_way",
    technological: "technological_means_having_do_with_technology",
};

const answers = [
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    false,
    true,
    true,
    true,
    false,
    true,
    true,
    false
];

let spotlightText1 = [
"Use a thermometer to find an <b>accurate</b> temperature.",
"I told my teacher an <b>accurate</b> description of the conflict.",
"I tried to paint an <b>accurate</b> copy of the famous picture.",
"He thinks older people are wise and is <b>biased</b> against the ideas of young people.",
"The teacher only heard one student's side so her opinion was <b>biased</b>.",
"The students were <b>biased</b> toward other children from their school.",
"Scientists are <b>investigating</b> the amazing, ancient pyramids.",
"Researchers are <b>investigating</b> the large crater in the ground.",
"Firefighters are <b>investigating</b> the cause of the fire.",
"Scientists are <b>investigating</b> the puzzling, ancient drawings.",
"Researchers are <b>investigating </b> what causes the disease.",
"The forest rangers are <b>investigating</b> the cause of the forest fire.",
"We called a <b>professional</b> to fix the shower.",
"The reading <b>professional</b> tutors students.",
"I talked to a fitness <b>professional</b> about new exercises.",
"My teacher <b>restricts</b> how much we talk during reading time.",
"Our principal <b>restricts</b> who can come in and visit our school.",
"Laws <b>restrict</b> what people are allowed to do.",
];
let spotlightText2 = [
"Use a thermometer to find a <b>precise</b> temperature.",
"I told my teacher an <b>exact</b> description of the conflict.",
"I tried to paint a <b>study</b> copy of the famous picture.",
"He thinks older people are wise and is <b>prejudiced</b> against the ideas of young people.",
"The teacher only heard one student's side so her opinion was <b>one-sided</b>.",
"The students were <b>precise</b> toward other children from their school.",
"Scientists are <b>studying</b> the amazing, ancient pyramids.",
"Researchers are <b>examining</b> the large crater in the ground.",
"Firefighters are <b>one-sided</b> the cause of the fire.",
"Scientists are <b>researching</b> the puzzling, ancient drawings.",
"Researchers are <b>expert</b> what causes the disease.",
"The forest rangers are <b>exploring</b> the cause of the forest fire.",
"We called an <b>expert</b> to fix the shower.",
"The reading <b>specialist</b> tutors students.",
"I talked to a fitness <b>explore</b> about new exercises.",
"My teacher <b>limits</b> how much we talk during reading time.",
"Our principal <b>regulates</b> who can come in and visit our school.",
"Laws <b>expert</b> what people are allowed to do.",
];

const kinds = [
"accurate_syn",
"accurate_syn",
"accurate_syn",
"biased_syn",
"biased_syn",
"biased_syn",
"investigate_syn1",
"investigate_syn1",
"investigate_syn1",
"investigate_syn2",
"investigate_syn2",
"investigate_syn2",
"professional_syn",
"professional_syn",
"professional_syn",
"restrict_syn",
"restrict_syn",
"restrict_syn",
];


const choices = answers.map(answer => [
    {
        choice: "trueButton",
        answer: answer,
        contentId: uuid.v4()
    },
    {
        choice: "falseButton",
        answer: !answer,
        contentId: uuid.v4()
    }
]);

const taskData = choices.map((taskChoices, i) => {
    const theKind = kinds[i];
    const theWord = theKind.substring(0,theKind.indexOf('_'));
  return {
    type: "MULTIPLECHOICE",
    contentId: uuid.v4(),
    kind: theKind,
    spotlightText1: [{text:spotlightText1[i]}],
    spotlightText2: [{text:spotlightText2[i]}],
    choices: choices[i],
    audio: [
        {
          type: "definition",
          path: `avocab.${definitions[theWord]}`
        },
        {
            type: "explanation",
            path: answers[i] ? "avocab.these_sentences_have_a_similar_meaning" : "avocab.these_sentences_have_different_meanings"
        },
        {
          type: "word",
          path: `words.${theWord}`
        }
    ]

    // audio: [
    //   { type: "prefix", path: `prefmng._${firstTrueChoice}_` },
    //   { type: "definition", path: `prefmng.${firstTrueChoice}_definition` }
    // ]
  };
});

let contentIds = taskData.map(({ contentId }) => contentId);

const taskConfig = {
  type: "MultipleChoice",
  params: {
    contentId: contentIds,
    layout: [
      {
        column: ["text_spotlightText", "choiceThumbs"]
      }
    ],
    stylePrefix: "presuffix",
    activityPrefix: "latin",
    shuffleChoices: false,
    shuffleContentIds: true
  }
};

export default class ThumbsVocStrat extends Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
          <textarea
            id="outputData"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskData))}
          />
        </div>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
          <textarea
            id="outputConfig"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskConfig))}
          />
        </div>
      </div>
    );
  }
}
