import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

export const branchingAudio = {
  verb: "a_noun_part_speech_names_person_verb",
  verb_present_tense: "a_past_tense_verb_usually_ends_us",
  pronouns_plural: "pronouns_take_the_place_of_nouns",
  noun: "a_noun_part_speech_names_person_verb",
  verb_past_tense: "a_past_tense_verb_usually_ends_us",
  possessive_noun: "a_possessive_noun_shows_belonging_has",
  adjective: "adjectives_tell_more_about_nouns",
  adverb: "adverbs_are_words_describe_verb_some",
  interjection: "an_interjection_word_shows_strong_feeling",
  conjunction: "conjunctions_show_ideas_sentence_are_related",
  conjunction_noun_verb: "conjunctions_like_words_but_so_show",
  verb_future_tense: "some_prepositional_phrases_tell_when_action",
  pronouns_singular: "pronouns_take_the_place_of_nouns",
  prepositional_phrase: "some_prepositional_phrases_tell_when_action",
  simple_subject_predicate: "the_simple_subject_noun_pronoun_tells_predicate",
  relative_pronoun: "the_words_which_who_whose_whom"
};

const wordSpotlights = [
["will pick"],
["will sing"],
["hatched"],
["sits"],
["picked"],
["yawns"],
["crawled"],
["ticked"],
["rings"],
["whistle"],
["breathes"],
["listen"],
["nearby"],
["inside"],
["tomorrow"],
["now"],
["carefully"],
["beautifully"],
["or"],
["Objects"],
["float"],
["and"],
["dolphins"],
["eat"],
["The prince"],
["That boy"],
["My sister"],
["Ann"],
["A cactus"],
["The telephone"],
["Greg and I"],
["The dog and I"],
["My friend and I"],
["The boy and girl"],
["The flowers"],
["The birds"],
["No"],
["Brr"],
["Ick"],
["do"],
["cold"],
["terrible"],
["in the swamp"],
["on the desk"],
["above his head"],
["at bedtime"],
["before the race"],
["during the summer"],
];

const sentences = [
"They will pick some flowers.",
"Clem will sing quietly.",
"Those eggs hatched.",
"Bella sits outside at this moment.",
"She picked a snack.",
"He yawns right now.",
"The insects crawled.",
"A clock ticked.",
"A phone rings.",
"The singers whistle.",
"The dolphin breathes.",
"My brothers listen.",
"The foxes live nearby.",
"She went inside.",
"We will visit tomorrow.",
"The play begins now.",
"She types carefully.",
"He sings beautifully.",
"Objects sink, or they float.",
"Objects sink, or they float.",
"Objects sink, or they float.",
"The dolphins swim, and they eat fish.",
"The dolphins swim, and they eat fish.",
"The dolphins swim, and they eat fish.",
"The prince kneels.",
"That boy rests.",
"My sister walks.",
"Ann pretends.",
"A cactus grows.",
"The telephone rings.",
"Greg and I eat.",
"The dog and I run.",
"My friend and I danced.",
"The boy and girl chat.",
"The flowers grow.",
"The birds flew.",
"No! Don't do that!",
"Brr, it's cold in here.",
"Ick! This soup tastes terrible.",
"No! Don't do that!",
"Brr, it's cold in here.",
"Ick! This soup tastes terrible.",
"The bugs live in the swamp.",
"A pencil sits on the desk.",
"A bird flew above his head.",
"The parents read at bedtime.",
"The runners stretched before the race.",
"The people swim during the summer.",
];

const solutions = [
"This happens LATER in the future, so the verb is in the future tense.",
"This happens LATER in the future, so the verb is in the future tense.",
"This ALREADY happened, so the verb is in the past tense.",
"This happens NOW, so the verb is in the present tense.",
"This ALREADY happened, so the verb is in the past tense.",
"This happens NOW, so the verb is in the present tense.",
"This ALREADY happened, so the verb is in the past tense.",
"This ALREADY happened, so the verb is in the past tense.",
"This happens NOW, so the verb is in the present tense.",
"This happens NOW, so the verb is in the present tense.",
"This happens NOW, so the verb is in the present tense.",
"This happens NOW, so the verb is in the present tense.",
"NEARBY tells where.",
"INSIDE tells where.",
"TOMORROW tells when.",
"NOW tells when.",
"CAREFULLY tells how.",
"BEAUTIFULLY tells how.",
"The conjunction OR gives a choice.",
"",
"",
"The conjunction AND adds information.",
"",
"",
"HE replaces the prince.",
"HE replaces that boy.",
"SHE replaces my sister.",
"SHE replaces Ann.",
"IT replaces a cactus.",
"IT replaces the telephone.",
"WE replaces Greg and I.",
"WE replaces the dog and I.",
"WE replaces my friend and I.",
"THEY replaces the boy and girl.",
"THEY replaces the flowers.",
"THEY replaces the birds.",
"NO is an interjection in this sentence.",
"BRR is an interjection in this sentence.",
"ICK is an interjection in this sentence.",
"DO is not an interjection in this sentence.",
"COLD is not an interjection in this sentence.",
"TERRIBLE is not an interjection in this sentence.",
"IN THE SWAMP tells where.",
"ON THE DESK tells where.",
"ABOVE HIS HEAD tells where.",
"AT BEDTIME tells when.",
"BEFORE THE RACE tells when.",
"DURING THE SUMMER tells when.",
];

const choices = [
["Past (Already)",	"Present (Now)", "Future (Later)*"],
["Past (Already)",	"Present (Now)", "Future (Later)*"],
["Past (Already)*",	"Present (Now)", "Future (Later)"],
["Past (Already)",	"Present (Now)*", "Future (Later)"],
["Past (Already)*",	"Present (Now)", "Future (Later)"],
["Past (Already)*",	"Present (Now)*", "Future (Later)"],
["Past (Already)*","Present (Now)"],
["Past (Already)",	"Present (Now)*"],
["Past (Already)",	"Present (Now)*"],
["Past (Already)",	"Present (Now)*"],
["Past (Already)",	"Present (Now)*"],
["Past (Already)",	"Present (Now)*"],
["Where*","When","How"],
    ["Where*","When","How"],
    ["Where","When*","How"],
    ["Where","When*","How"],
    ["Where","When","How*"],
    ["Where","When","How*"],
    ["conjunction*","noun","verb"],
    ["conjunction","noun*","verb"],
    ["conjunction","noun","verb*"],
    ["conjunction*","noun","verb"],
    ["conjunction","noun*","verb"],
    ["conjunction","noun","verb*"],
    ["He*","She","It"],
    ["He*","She","It"],
    ["He","She*","It"],
    ["He","She*","It"],
    ["He","She","It*"],
    ["He","She","It*"],
    ["We*","They"],
    ["We*","They"],
    ["We*","They"],
    ["We","They*"],
    ["We","They*"],
    ["We","They*"],
    ["interjection*","not an interjection"],
    ["interjection*","not an interjection"],
    ["interjection*","not an interjection"],
    ["interjection","not an interjection*"],
    ["interjection","not an interjection*"],
    ["interjection","not an interjection*"],
    ["Where*","When"],
    ["Where*","When"],
    ["Where*","When"],
    ["Where","When*"],
    ["Where","When*"],
    ["Where","When*"],
];

const kinds = [
    "verb_future_tense",
    "verb_future_tense",
    "verb_future_tense",
    "verb_future_tense",
    "verb_future_tense",
    "verb_future_tense",
    "verb_present_tense",
    "verb_present_tense",
    "verb_present_tense",
    "verb_present_tense",
    "verb_present_tense",
    "verb_present_tense",
    "adverb",
    "adverb",
    "adverb",
    "adverb",
    "adverb",
    "adverb",
    "conjunction",
    "conjunction_noun_verb",
    "conjunction_noun_verb",
    "conjunction_noun_verb",
    "conjunction_noun_verb",
    "conjunction_noun_verb",
    "pronouns_singular",
    "pronouns_singular",
    "pronouns_singular",
    "pronouns_singular",
    "pronouns_singular",
    "pronouns_singular",
    "pronouns_plural",
    "pronouns_plural",
    "pronouns_plural",
    "pronouns_plural",
    "pronouns_plural",
    "pronouns_plural",
    "interjection",
    "interjection",
    "interjection",
    "interjection",
    "interjection",
    "interjection",
    "prepositional_phrase",
    "prepositional_phrase",
    "prepositional_phrase",
    "prepositional_phrase",
    "prepositional_phrase",
    "prepositional_phrase",
];

export default class GrmmrSortingBoxes extends Component {
  componentDidMount = () => {
    const spotlightText = this.makeNestedSpotlightText(wordSpotlights);
    const spotlightSentence = makeSpotlightText(
      this.highlightWords(sentences, wordSpotlights)
    );
    const taskData = choices.map((taskChoices, i) => {
        console.warn(kinds[i],branchingAudio[kinds[i]])
      return {
        type: "MULTIPLECHOICE",
        contentId: uuid.v4(),
        kind: kinds[i],
        spotlightSentence: spotlightSentence[i],
        spotlightText: spotlightText[i],
        choices: this.makeChoices(taskChoices),
        audio: [
          {
            type: "intro",
            path: `common_grmmr.${branchingAudio[kinds[i]]}`
          },
          {
            type: "explanation",
            path: `common_grmmr.${filenameGenerator(solutions[i])}`
          },
            // {
            //   type: "word",
            //   path: `words.${wordSpotlights[i][0].toLowerCase()}`
            // },
            // {
            //   type: "wordType",
            //   path: `common_grmmr.${solutions[i]}`
            // }
        ]
      };
    });
    let contentIds = taskData.map(({ contentId }) => contentId);
    const taskConfig = {
      type: "MultipleChoice",
      params: {
        contentId: contentIds,
        layout: [
          {
            column: ["text_spotlightText", "sortingBoxes"]
          }
        ],
        stylePrefix: "grmmr1_sort",
        shuffleChoices: false,
        shuffleContentIds: true
      }
    };

    this.setState({
      taskData,
      taskConfig
    });
  };

  highlightWords = (sentences, words) => {
    return sentences.map((s, i) => {
      let result = s;
      words[i].forEach(word => {
        result = result.replace(word, `<h>${word}</h>`);
      });
      return result;
    });
  };

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      return {
        choice: c.replace("*", ""),
        answer,
        contentId: uuid.v4()
      };
    });
  };

  makeNestedSpotlightText = (textArrays, audioKey) => {
    audioKey = audioKey ? audioKey + "." : "";
    return textArrays.map(textArr => {
      return textArr.map(t => {
        const slt = { text: t };
        return slt;
      });
    });
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
