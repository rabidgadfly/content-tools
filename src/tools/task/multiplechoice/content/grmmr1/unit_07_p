import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

const spotlightImages = [
  "where",
  "where",
  "where",
  "where",
  "where",
  "where",
  "when",
  "when",
  "when",
  "when",
  "when",
  "when",
  "how",
  "how",
  "how",
  "how",
  "how",
  "how"
];

const prompts = [
  "On weekdays",
  "Before the match",
  "Throughout the day",
  "At night",
  "During that class",
  "After midnight",
  "Under the dock",
  "On the pole",
  "Near the stream",
  "Through the grass",
  "In the kitchen",
  "Over the valley"
];

const solutions = [
  "Nearby",
  "Inside",
  "There",
  "Away",
  "Outside",
  "Downstairs",
  "Yesterday",
  "Tomorrow",
  "Today",
  "Daily",
  "Monthly",
  "Often",
  "Powerfully",
  "Gracefully",
  "Slowly",
  "Happily",
  "Secretly",
  "Carefully"
];

const spotlightText = makeSpotlightText([
  "The thunder boomed _____.",
  "She performs _____.",
  "The ship sails ____.",
  "He skipped _____.",
  "The meeting happens ____. ",
  "The dentist works _____.",
  "The thunder boomed _____.",
  "She performs _____.",
  "The ship sails ____.",
  "He skipped _____.",
  "The meeting happens ____. ",
  "The dentist works _____.",
  "The thunder boomed _____.",
  "She performs _____.",
  "The ship sails ____.",
  "He skipped _____.",
  "The meeting happens ____. ",
  "The dentist works _____."
]);

const choices = [
  ["yesterday", "nearby*"],
  ["inside*", "tomorrow"],
  ["there*", "slowly"],
  ["happily", "away*"],
  ["monthly", "outside*"],
  ["downstairs*", "often"],
  ["yesterday*", "nearby"],
  ["inside", "tomorrow*"],
  ["there", "today*"],
  ["happily", "daily*"],
  ["monthly*", "outside"],
  ["downstairs", "often*"],
  ["yesterday", "powerfully*"],
  ["inside", "gracefully*"],
  ["there", "slowly*"],
  ["happily*", "away"],
  ["monthly", "secretly*"],
  ["downstairs", "carefully*"]
];

const kinds = [
  "adverb_where",
  "adverb_where",
  "adverb_where",
  "adverb_where",
  "adverb_where",
  "adverb_where",
  "adverb_when",
  "adverb_when",
  "adverb_when",
  "adverb_when",
  "adverb_when",
  "adverb_when",
  "adverb_how",
  "adverb_how",
  "adverb_how",
  "adverb_how",
  "adverb_how",
  "adverb_how"
];

export default class GrmmrMCqa extends Component {
  componentDidMount = () => {
    const taskData = this.getTaskData();
    const contentIds = taskData.map(({ contentId }) => contentId);

    this.setState({
      contentIds: contentIds,
      taskData: taskData,
      taskConfig: this.getTaskConfig(contentIds)
    });
  };

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      return {
        choice: c.replace("*", ""),
        answer,
        contentId: uuid.v4()
      };
    });
  };

  getTaskData = () => {
    return choices.map((taskChoices, i) => {
      return {
        type: "MULTIPLECHOICE",
        contentId: uuid.v4(),
        kind: kinds[i],
        spotlightImage: [{ image: `grmmr1_graphic.${spotlightImages[i]}` }],
        spotlightText: spotlightText[i],
        choices: this.makeChoices(choices[i]),
        audio: [
          {
            type: "prompt",
            path: `words.${spotlightImages[i]}`
          },
          {
            type: "solution",
            path: `words.${solutions[i].toLowerCase()}`
          }
        ]
      };
    });
  };

  getTaskConfig = contentIds => {
    return {
      type: "MultipleChoice",
      params: {
        contentId: contentIds,
        layout: [
          {
            column: [
              "spotlightImage",
              "text_spotlightText",
              "choiceColumnBorders"
            ]
          }
        ],
        stylePrefix: "grmmr1_mc",
        shuffleChoices: false,
        shuffleContentIds: true
      }
    };
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
