import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

let intros = {
  adjective: "Adjectives tell more about nouns.",
  adverb:
    "Adverbs are words that describe the verb. Some adverbs tell WHERE the action happens, some tell WHEN the action happens, and some tell HOW the action happens.",
  prepositional_phrase:
    "Some prepositional phrases tell WHEN the action happens, and some tell WHERE the action happens.",
  pronouns_singular: "Pronouns take the place of nouns.",
  conjunction:
    "Conjunctions, like the words AND, BUT, SO, and OR, show how ideas in a sentence are related.",
  pronouns_plural: "Pronouns take the place of nouns.",
  verb_future_tense:
    "Future tense verbs show action that will happen later. The word 'will' usually comes before the verb.",
  noun:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  verb:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  simple_subject_predicate:
    "The simple subject is the noun or pronoun that tells who or what the sentence is about. It answers the question, 'Who or what is doing the action?' The simple predicate is the verb that tells what the subject does or did. It answers the question, 'What is the action?'",
  relative_pronoun:
    "The words THAT, WHICH, WHO, WHOSE, and WHOM can be relative pronouns. They give more information about nouns.",
  verb_past_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  verb_present_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  possessive_noun:
    "A possessive noun, like the word STUDENT'S, shows belonging. It has an apostrophe.",
  interjection:
    "An interjection is a word that shows strong feeling or emphasis. It's interesting and sometimes fun but it is not necessary. It CAN be left out."
};

const wordSpotlights = [
  ["on the floor"],
  ["at night"],
  ["in the swamp"],
  ["on the desk"],
  ["above his head"],
  ["at bedtime"],
  ["before the race"],
  ["during the summer"]
];

const sentences = [
  "The dogs sleep on the floor.",
  "The dogs sleep at night.",
  "The bugs live in the swamp.",
  "A pencil sits on the desk. ",
  "A bird flew above his head.",
  "The parents read at bedtime.",
  "The runners stretched before the race. ",
  "The people swim during the summer."
];

const solutions = [
  "where",
  "when",
  "where",
  "where",
  "where",
  "when",
  "when",
  "when"
];

const choices = [
  ["Where*", "When"],
  ["Where", "When*"],
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where", "When*"],
  ["Where", "When*"],
  ["Where", "When*"]
];

const words = [
  "on the floor",
  "at night",
  "in the swamp",
  "on the desk",
  "above his head",
  "at bedtime",
  "before the race",
  "during the summer"
];

// const kinds = [
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense",
//   "verb_future_tense"
// ];

export default class GrmmrSortingBoxes extends Component {
  componentDidMount = () => {
    const spotlightText = this.makeNestedSpotlightText(wordSpotlights);
    const spotlightSentence = makeSpotlightText(
      this.highlightWords(sentences, wordSpotlights)
    );
    const taskData = choices.map((taskChoices, i) => {
      let answerChoice = taskChoices
        .find(tc => tc.indexOf("*") > -1)
        .replace("*", "");
      return {
        type: "MULTIPLECHOICE",
        contentId: uuid.v4(),
        // kind: kinds[i],
        spotlightSentence: spotlightSentence[i],
        spotlightText: spotlightText[i],
        choices: this.makeChoices(taskChoices),
        audio: [
          {
            type: "explanation",
            path: `grmmr1.${filenameGenerator(
              `${words[i]} tells ${solutions[i]}`
            )}`
          }
        ]
      };
    });
    let contentIds = taskData.map(({ contentId }) => contentId);
    const taskConfig = {
      type: "MultipleChoice",
      params: {
        contentId: contentIds,
        layout: [
          {
            column: ["text_spotlightSentence", "sortingBoxes"]
          }
        ],
        stylePrefix: "grmmr1_sort",
        shuffleChoices: false,
        shuffleContentIds: true
      }
    };

    this.setState({
      taskData,
      taskConfig
    });
  };

  highlightWords = (sentences, words) => {
    return sentences.map((s, i) => {
      let result = s;
      words[i].forEach(word => {
        result = result.replace(word, `<h>${word}</h>`);
      });
      return result;
    });
  };

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      return {
        choice: c.replace("*", ""),
        answer,
        contentId: uuid.v4()
      };
    });
  };

  makeNestedSpotlightText = (textArrays, audioKey) => {
    audioKey = audioKey ? audioKey + "." : "";
    return textArrays.map(textArr => {
      return textArr.map(t => {
        const slt = { text: t };
        return slt;
      });
    });
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
