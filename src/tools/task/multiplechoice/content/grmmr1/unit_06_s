import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

const spotlightImages = [
  "where",
  "when",
  "where",
  "when",
  "where",
  "when",
  "where",
  "when",
  "where",
  "when"
];

const spotlightText = makeSpotlightText([
  "The librarian read _____. ",
  "Soraya played ______.",
  "The quiet bunny rested ______. ",
  "A lonely cat wandered _____.",
  "The loud bell rings _____. ",
  "________, the hungry chipmunk eats.",
  "The volcano erupted ____.",
  "My uncle sleeps _____. ",
  "The cow walked _____. ",
  "____ I jogged."
]);

const choices = [
  ["at 3 o'clock", "after class", "in the classroom*"],
  ["at her house", "on Saturday*", "beside the tent"],
  ["throughout the morning", "before dinner", "under a blanket*"],
  ["toward the farm", "past midnight*", "into the street"],
  ["in my ear*", "at noon", "on weekdays"],
  ["Near the fence", "During the winter*", "On the tree branch"],
  ["before sunrise", "above the city*", "at 2:30"],
  ["at night*", "at the doctor's office", "on his bed"],
  ["after the rain", "in the afternoon", "over the grass*"],
  ["Over the weekend*", "Up the hill", "Around the track"]
];

const kinds = [
  "prepositional_phrase_where",
  "prepositional_phrase_when",
  "prepositional_phrase_where",
  "prepositional_phrase_when",
  "prepositional_phrase_where",
  "prepositional_phrase_when",
  "prepositional_phrase_where",
  "prepositional_phrase_when",
  "prepositional_phrase_where",
  "prepositional_phrase_when"
];

export default class GrmmrMCqa extends Component {
  componentDidMount = () => {
    const taskData = this.getTaskData();
    const contentIds = taskData.map(({ contentId }) => contentId);

    this.setState({
      contentIds: contentIds,
      taskData: taskData,
      taskConfig: this.getTaskConfig(contentIds)
    });
  };

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      return {
        choice: c.replace("*", ""),
        answer,
        contentId: uuid.v4()
      };
    });
  };

  getTaskData = () => {
    return choices.map((taskChoices, i) => {
      return {
        type: "MULTIPLECHOICE",
        contentId: uuid.v4(),
        kind: kinds[i],
        spotlightImage: [{ image: `grmmr1_graphic.${spotlightImages[i]}` }],
        spotlightText: spotlightText[i],
        choices: this.makeChoices(choices[i]),
        audio: [
          {
            type: "prompt",
            path: `words.${spotlightImages[i]}`
          }
        ]
      };
    });
  };

  getTaskConfig = contentIds => {
    return {
      type: "MultipleChoice",
      params: {
        contentId: contentIds,
        layout: [
          {
            column: [
              "spotlightImage",
              "text_spotlightText",
              "choiceColumnBorders"
            ]
          }
        ],
        stylePrefix: "grmmr1_mc",
        shuffleChoices: false,
        shuffleContentIds: true
      }
    };
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
