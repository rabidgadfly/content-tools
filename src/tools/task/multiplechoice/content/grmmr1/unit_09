let intros = {
  adjective: "Adjectives tell more about nouns.",
  adverb:
    "Adverbs are words that describe the verb. Some adverbs tell WHERE the action happens, some tell WHEN the action happens, and some tell HOW the action happens.",
  prepositional_phrase:
    "Some prepositional phrases tell WHEN the action happens, and some tell WHERE the action happens.",
  pronouns_singular: "Pronouns take the place of nouns.",
  conjunction:
    "Conjunctions, like the words AND, BUT, SO, and OR, show how ideas in a sentence are related.",
  pronouns_plural: "Pronouns take the place of nouns.",
  verb_future_tense:
    "Future tense verbs show action that will happen later. The word 'will' usually comes before the verb.",
  noun:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  verb:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  simple_subject_predicate:
    "The simple subject is the noun or pronoun that tells who or what the sentence is about. It answers the question, 'Who or what is doing the action?' The simple predicate is the verb that tells what the subject does or did. It answers the question, 'What is the action?'",
  relative_pronoun:
    "The words THAT, WHICH, WHO, WHOSE, and WHOM can be relative pronouns. They give more information about nouns.",
  verb_past_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  verb_present_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  possessive_noun:
    "A possessive noun, like the word STUDENT'S, shows belonging. It has an apostrophe.",
  interjection:
    "An interjection is a word that shows strong feeling or emphasis. It's interesting and sometimes fun but it is not necessary. It CAN be left out."
};
const wordSpotlights = [
  "loud",
  "green",
  "nine",
  "several",
  "those",
  "that",
  "nearby",
  "inside",
  "tomorrow",
  "now",
  "carefully",
  "beautifully",
  "in the swamp",
  "on the desk",
  "above his head",
  "at bedtime",
  "before the race",
  "during the summer",
  "prince",
  "boy",
  "sister",
  "Ann",
  "cactus",
  "telephone"
];

const spotlightText = makeSpotlightText(wordSpotlights);

const highlightWords = (sentences, words) => {
  return sentences.map((s, i) => {
    return s.replace(words[i], `<h>${words[i]}</h>`);
  });
};

const spotlightSentence = makeSpotlightText(
  highlightWords(
    [
      "A loud bell rings.",
      "A green frog jumps",
      "Nine bells ring.",
      "Several frogs jump.",
      "Those bells ring.",
      "That frog jumps.",
      "The foxes live nearby.",
      "She went inside.",
      "We will visit tomorrow.",
      "The play begins now.",
      "She types carefully.",
      "He sings beautifully.",
      "The bugs live in the swamp.",
      "A pencil sits on the desk. ",
      "A bird flew above his head.",
      "The parents read at bedtime.",
      "The runners stretched before the race. ",
      "The people swim during the summer.",
      "The prince kneels. ",
      "That boy rests.",
      "My sister walks.",
      "Ann pretends.",
      "A cactus grows.",
      "The telephone rings."
    ],
    wordSpotlights
  )
);

const solutions = [
  "LOUD tells what kind.",
  "GREEN tells what kind.",
  "NINE tells how many.",
  "SEVERAL tells how many.",
  "THOSE tells which one.",
  "THAT tells which one.",
  "NEARBY tells where.",
  "INSIDE tells where.",
  "TOMORROW tells when.",
  "NOW tells when.",
  "CAREFULLY tells how.",
  "BEAUTIFULLY tells how.",
  "IN THE SWAMP tells where.",
  "ON THE DESK tells where.",
  "ABOVE HIS HEAD tells where.",
  "AT BEDTIME tells when.",
  "BEFORE THE RACE tells when.",
  "DURING THE SUMMER tells when.",
  "HE replaces prince.",
  "HE replaces boy.",
  "SHE replaces sister.",
  "SHE replaces Ann.",
  "IT replaces cactus.",
  "IT replaces telephone."
];

const choices = [
  ["What Kind*", "How Many", "Which One"],
  ["What Kind*", "How Many", "Which One"],
  ["What Kind", "How Many*", "Which One"],
  ["What Kind", "How Many*", "Which One"],
  ["What Kind", "How Many", "Which One*"],
  ["What Kind", "How Many", "Which One*"],
  ["Where*", "When", "How"],
  ["Where*", "When", "How"],
  ["Where", "When*", "How"],
  ["Where", "When*", "How"],
  ["Where", "When", "How*"],
  ["Where", "When", "How*"],
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where", "When*"],
  ["Where", "When*"],
  ["Where", "When*"],
  ["He*", "She", "It"],
  ["He*", "She", "It"],
  ["He", "She*", "It"],
  ["He", "She*", "It"],
  ["He", "She", "It*"],
  ["He", "She", "It*"]
];

const kinds = [
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adverb",
  "adverb",
  "adverb",
  "adverb",
  "adverb",
  "adverb",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular"
];
