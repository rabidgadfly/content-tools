import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

let intros = {
  adjective: "Adjectives tell more about nouns.",
  adverb:
    "Adverbs are words that describe the verb. Some adverbs tell WHERE the action happens, some tell WHEN the action happens, and some tell HOW the action happens.",
  prepositional_phrase:
    "Some prepositional phrases tell WHEN the action happens, and some tell WHERE the action happens.",
  pronouns_singular: "Pronouns take the place of nouns.",
  conjunction:
    "Conjunctions, like the words AND, BUT, SO, and OR, show how ideas in a sentence are related.",
  pronouns_plural: "Pronouns take the place of nouns.",
  verb_future_tense:
    "Future tense verbs show action that will happen later. The word 'will' usually comes before the verb.",
  noun:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  verb:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  simple_subject_predicate:
    "The simple subject is the noun or pronoun that tells who or what the sentence is about. It answers the question, 'Who or what is doing the action?' The simple predicate is the verb that tells what the subject does or did. It answers the question, 'What is the action?'",
  relative_pronoun:
    "The words THAT, WHICH, WHO, WHOSE, and WHOM can be relative pronouns. They give more information about nouns.",
  verb_past_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  verb_present_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  possessive_noun:
    "A possessive noun, like the word STUDENT'S, shows belonging. It has an apostrophe.",
  interjection:
    "An interjection is a word that shows strong feeling or emphasis. It's interesting and sometimes fun but it is not necessary. It CAN be left out."
};

const wordSpotlights = [
  ["in the swamp"],
  ["on the desk"],
  ["above his head"],
  ["at bedtime"],
  ["before the race"],
  ["during the summer"],
  ["loud"],
  ["green"],
  ["nine"],
  ["several"],
  ["those"],
  ["that"],
  ["nearby"],
  ["inside"],
  ["tomorrow"],
  ["now"],
  ["carefully"],
  ["beautifully"],
  ["The prince"],
  ["That boy"],
  ["My sister"],
  ["Ann"],
  ["A cactus"],
  ["The telephone"],
  ["but"],
  ["Alton"],
  ["walked"],
  ["or"],
  ["Objects"],
  ["float"],
  ["Greg and I"],
  ["The dog and I"],
  ["My friend and I"],
  ["The boy and girl"],
  ["The flowers"],
  ["The birds"],
  ["will pick"],
  ["will sing"],
  ["hatched"],
  ["sits"],
  ["picked"],
  ["yawns"]
];

const sentences = [
  "The bugs live in the swamp.",
  "A pencil sits on the desk.",
  "A bird flew above his head.",
  "The parents read at bedtime.",
  "The runners stretched before the race.",
  "The people swim during the summer.",
  "A loud bell rings.",
  "A green frog jumps.",
  "Nine bells ring.",
  "Several frogs jump.",
  "Those bells ring.",
  "That frog jumps.",
  "The foxes live nearby.",
  "She went inside.",
  "We will visit tomorrow.",
  "The play begins now.",
  "She types carefully.",
  "He sings beautifully.",
  "The prince kneels.",
  "That boy rests.",
  "My sister walks.",
  "Ann pretends.",
  "A cactus grows.",
  "The telephone rings.",
  "Lisa ran, but Alton walked.",
  "Lisa ran, but Alton walked.",
  "Lisa ran, but Alton walked.",
  "Objects sink, or they float.",
  "Objects sink, or they float.",
  "Objects sink, or they float.",
  "Greg and I eat.",
  "The dog and I run.",
  "My friend and I danced.",
  "The boy and girl chat.",
  "The flowers grow.",
  "The birds flew.",
  "They will pick some flowers.",
  "Clem will sing quietly.",
  "Those eggs hatched.",
  "Bella sits outside at this moment.",
  "She picked a snack.",
  "He yawns right now."
];

const solutions = [
  "IN THE SWAMP tells where.",
  "ON THE DESK tells where.",
  "ABOVE HIS HEAD where.",
  "AT BEDTIME tells when.",
  "BEFORE THE RACE tells when.",
  "DURING THE SUMMER tells when.",
  "LOUD tells what kind.",
  "GREEN tells what kind.",
  "NINE tells how many.",
  "SEVERAL tells how many.",
  "THOSE tells which one.",
  "THAT tells which one.",
  "NEARBY tells where.",
  "INSIDE tells where.",
  "TOMORROW tells when.",
  "NOW tells when.",
  "CAREFULLY tells how.",
  "BEAUTIFULLY tells how.",
  "HE replaces prince.",
  "HE replaces boy.",
  "SHE replaces sister.",
  "SHE replaces Ann.",
  "IT replaces cactus.",
  "IT replaces telephone.",
  "The conjunction BUT shows that different information comes next.",
  "The conjunction BUT shows that different information comes next.",
  "The conjunction BUT shows that different information comes next.",
  "The conjunction OR gives a choice.",
  "The conjunction OR gives a choice.",
  "The conjunction OR gives a choice.",
  "WE replaces Greg and I.",
  "WE replaces the dog and I.",
  "WE replaces my friend and I.",
  "THEY replaces the boy and girl.",
  "THEY replaces the flowers.",
  "THEY replaces the birds.",
  "This happens LATER in the future, so the verb is in the future tense.",
  "This happens LATER in the future, so the verb is in the future tense.",
  "This ALREADY happened, so the verb is in the past tense.",
  "This happens NOW, so the verb is in the present tense.",
  "This ALREADY happened, so the verb is in the past tense.",
  "This happens NOW, so the verb is in the present tense."
];

const choices = [
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where", "When*"],
  ["Where", "When*"],
  ["Where", "When*"],
  ["What Kind*", "How Many", "Which One"],
  ["What Kind*", "How Many", "Which One"],
  ["What Kind", "How Many*", "Which One"],
  ["What Kind", "How Many*", "Which One"],
  ["What Kind", "How Many", "Which One*"],
  ["What Kind", "How Many", "Which One*"],
  ["Where*", "When", "how"],
  ["Where*", "When", "how"],
  ["Where", "When*", "how"],
  ["Where", "When*", "how"],
  ["Where", "When", "how*"],
  ["Where", "When", "how*"],
  ["He*", "She", "It"],
  ["He*", "She", "It"],
  ["He", "She*", "It"],
  ["He", "She*", "It"],
  ["He", "She", "It*"],
  ["He", "She", "It*"],
  ["conjunction*", "noun", "verb"],
  ["conjunction*", "noun", "verb"],
  ["conjunction", "noun*", "verb"],
  ["conjunction", "noun*", "verb"],
  ["conjunction", "noun", "verb*"],
  ["conjunction", "noun", "verb*"],
  ["We*", "They"],
  ["We*", "They"],
  ["We*", "They"],
  ["We", "They*"],
  ["We", "They*"],
  ["We", "They*"],
  ["Past (Already)", "Present (Now)", "Future (Later)*"],
  ["Past (Already)", "Present (Now)", "Future (Later)*"],
  ["Past (Already)*", "Present (Now)", "Future (Later)"],
  ["Past (Already)", "Present (Now)*", "Future (Later)"],
  ["Past (Already)*", "Present (Now)", "Future (Later)"],
  ["Past (Already)", "Present (Now)*", "Future (Later)"]
];

const kinds = [
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adverb",
  "adverb",
  "adverb",
  "adverb",
  "adverb",
  "adverb",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "conjunction",
  "conjunction",
  "conjunction",
  "conjunction",
  "conjunction",
  "conjunction",
  "pronouns_plural",
  "pronouns_plural",
  "pronouns_plural",
  "pronouns_plural",
  "pronouns_plural",
  "pronouns_plural",
  "verb_future_tense",
  "verb_future_tense",
  "verb_future_tense",
  "verb_future_tense",
  "verb_future_tense",
  "verb_future_tense"
];

export default class GrmmrSortingBoxes extends Component {
  componentDidMount = () => {
    const spotlightText = this.makeNestedSpotlightText(wordSpotlights);
    const spotlightSentence = makeSpotlightText(
      this.highlightWords(sentences, wordSpotlights)
    );
    const taskData = choices.map((taskChoices, i) => {
      return {
        type: "MULTIPLECHOICE",
        contentId: uuid.v4(),
        kind: kinds[i],
        spotlightSentence: spotlightSentence[i],
        spotlightText: spotlightText[i],
        choices: this.makeChoices(taskChoices),
        audio: [
          {
            type: "intro",
            path: `grmmr1.${filenameGenerator(intros[kinds[i]])}`
          },
          {
            type: "outro",
            path: `grmmr1.${filenameGenerator(solutions[i])}`
          }
          //   {
          //     type: "word",
          //     path: `words.${wordSpotlights[i].toLowerCase()}`
          //   },
          //   {
          //     type: "phrase",
          //     path: `grmmr1.${filenameGenerator(solutions[i])}`
          //   }
        ]
      };
    });
    let contentIds = taskData.map(({ contentId }) => contentId);
    const taskConfig = {
      type: "MultipleChoice",
      params: {
        contentId: contentIds,
        layout: [
          {
            column: ["text_spotlightText", "sortingBoxes"]
          }
        ],
        stylePrefix: "grmmr1_sort",
        shuffleChoices: false,
        shuffleContentIds: true
      }
    };

    this.setState({
      taskData,
      taskConfig
    });
  };

  highlightWords = (sentences, words) => {
    return sentences.map((s, i) => {
      let result = s;
      words[i].forEach(word => {
        result = result.replace(word, `<h>${word}</h>`);
      });
      return result;
    });
  };

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      return {
        choice: c.replace("*", ""),
        answer,
        contentId: uuid.v4()
      };
    });
  };

  makeNestedSpotlightText = (textArrays, audioKey) => {
    audioKey = audioKey ? audioKey + "." : "";
    return textArrays.map(textArr => {
      return textArr.map(t => {
        const slt = { text: t };
        return slt;
      });
    });
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
