import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";
//Unit 4: Word Sort (achieve, attempt, challenging, compete, however)
const definitions = {
    achieve: "to_achieve_is_succeed_doing_something",
    although: "although_signal_word_means_same_thing",
    candidate: "a_candidate_someone_who_trying_get",
    office: "an_office_important_job_often_government",
    represent: "to_represent_people_do_say_things",
    policy: "a_policy_group_rules_plans_used",
    attempt: "to_attempt_is_try_do_something",
    benefit: "a_benefit_is_good_helpful_result",
    challenging: "challenging_means_difficult",
    conclude: "to_conclude_decide_after_thinking_about",
    compete: "to_compete_work_against_others_try",
    during: "during_signal_word_means_through_period",
    however: "however_signal_word_means_same_thing",
    improve: "to_improve_make_better_become_better",
    physical: "physical_describes_activities_actions_have_do",
    commonly: "commonly_means_the_same_thing_often",
    environment: "environment_land_water_air_living_things",
    extinction: "extinction_when_entire_type_plant_animal",
    impact: "an_impact_is_a_result",
    species: "a_species_is_kind_plant_animal",
    culture: "a_culture_groups_values_way_life",
    generation: "a_generation_group_people_who_are",
    inaddition: "in_addition_phrase_means_same_thing",
    theme: "a_theme_central_message_lesson_story",
    traditional: "traditional_describes_traditions_old_ways_thinking",
    estimate: "to_estimate_guess_amount_value_using",
    habitat: "a_habitat_natural_place_where_plant",
    migration: "migration_when_many_animals_people_move",
    structure: "a_structure_something_built_putting_parts",
    therefore: "therefore_signal_word_means_same_thing",
    calories: "a_calorie_unit_measuring_much_energy",
    despite: "despite_signal_word_means_even_with",
    negative: "negative_describes_something_that_is_bad",
    nutrients: "nutrients_are_substances_help_plants_animals",
    process: "to_process_change_something_one_form",
    conflict: "a_conflict_struggle_between_people_things",
    issue: "an_issue_problem_important_topic_people",
    perspective: "a_perspective_way_looking_understanding_something",
    resolution: "the_resolution_story_final_part_when",
    witness: "to_witness_an_event_see_happens",
};

const answers = [
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    false,
    true,
    true,
    true,
    true,
    false,
    true,
    true,
    false,
];

let spotlightText1 = [
"The two brothers had a <b>conflict</b> about what game to play.",
"I helped my two friends end their <b>conflict</b>.",
"<b>Conflict</b> between countries makes life harder for people.",
"Our teacher asked us to talk about our different <b>perspectives</b> on the book.",
"Learning about the history of voting rights gave Chris a new <b>perspective</b> on voting.",
"There are different <b>perspectives</b> on whether lawmakers should limit unhealthy foods.",
"Sharon <b>witnessed</b> her daughter take all of the clothes out of the drawers.",
"Camila <b>witnessed</b> the bike crash into a tree.",
"Samuel <b>witnessed</b> a pink and orange sunset.",
"Our class discussed our different <b>perspectives</b> on the new school rule.",
"Learning about his culture gave Chris a new <b>perspective</b> on his family.",
"There are different <b>perspectives</b> on whether recess is a good use of time at school.",
"There are different <b>perspectives</b> on whether break time is a good use of time at school.",
"The friends talked about the <b>issues</b> they heard about on the news.",
"Droughts and floods are <b>issues</b> in towns across the country.",
"I care about safety <b>issues</b> at my school.",
"The story's <b>resolution</b> was that trying something new can be frightening, but worth it.",
"He didn't understand why the witch was evil until the story's <b>resolution</b>.",
"The folktale's <b>resolution</b> is when the fox and raven become friends again.",
];
let spotlightText2 = [
"The two brothers had a <b>disagreement</b> about what game to play.",
"I helped my two friends end their <b>fight</b>.",
"<b>Watch</b> between countries makes life harder for people.",
"Our teacher asked us to talk about our different <b>opinions</b> on the book.",
"Learning about the history of voting rights gave Chris a new <b>point of view</b> on voting.",
"There are different <b>observe</b> on whether lawmakers should limit unhealthy foods.",
"Sharon <b>observed</b> her daughter take all of the clothes out of the drawers.",
"Camila <b>watched</b> the bike crash into a tree.",
"Samuel <b>fight</b> a pink and orange sunset.",
"Our class discussed our different <b>stances</b> on the new school rule.",
"Learning about his culture gave Chris a new <b>ending</b> on his family.",
"There are different <b>viewpoints</b> on whether recess is a good use of time at school.",
"There are different <b>viewpoints</b> on whether break time is a good use of time at school.",
"The friends talked about the <b>problems</b> they heard about on the news.",
"Droughts and floods are <b>concerns</b> in towns across the country.",
"I care about safety <b>conclusions</b> at my school.",
"The story's <b>conclusion</b> was that trying something new can be frightening, but worth it.",
"He didn't understand why the witch was evil until the story's <b>ending</b>.",
"The folktale's <b>stance</b> is when the fox and raven become friends again.",
];

const kinds = [
"conflict_syn",
"conflict_syn",
"conflict_syn",
"perspective_syn1",
"perspective_syn1",
"perspective_syn1",
"witness_syn",
"witness_syn",
"witness_syn",
"perspective_syn2",
"perspective_syn2",
"perspective_syn2",
"perspective_syn2",
"issue_syn",
"issue_syn",
"issue_syn",
"resolution_syn",
"resolution_syn",
"resolution_syn",
];


const choices = answers.map(answer => [
    {
        choice: "trueButton",
        answer: answer,
        contentId: uuid.v4()
    },
    {
        choice: "falseButton",
        answer: !answer,
        contentId: uuid.v4()
    }
]);

const taskData = choices.map((taskChoices, i) => {
    const theKind = kinds[i];
    const theWord = theKind.substring(0,theKind.indexOf('_'));
  return {
    type: "MULTIPLECHOICE",
    contentId: uuid.v4(),
    kind: theKind,
    spotlightText1: [{text:spotlightText1[i]}],
    spotlightText2: [{text:spotlightText2[i]}],
    choices: choices[i],
    audio: [
        {
          type: "definition",
          path: `avocab.${definitions[theWord]}`
        },
        {
            type: "explanation",
            path: answers[i] ? "avocab.these_sentences_have_a_similar_meaning" : "avocab.these_sentences_have_different_meanings"
        },
        {
          type: "word",
          path: `words.${theWord}`
        }
    ]

    // audio: [
    //   { type: "prefix", path: `prefmng._${firstTrueChoice}_` },
    //   { type: "definition", path: `prefmng.${firstTrueChoice}_definition` }
    // ]
  };
});

let contentIds = taskData.map(({ contentId }) => contentId);

const taskConfig = {
  type: "MultipleChoice",
  params: {
    contentId: contentIds,
    layout: [
      {
        column: ["text_spotlightText", "choiceThumbs"]
      }
    ],
    stylePrefix: "presuffix",
    activityPrefix: "latin",
    shuffleChoices: false,
    shuffleContentIds: true
  }
};

export default class ThumbsVocStrat extends Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
          <textarea
            id="outputData"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskData))}
          />
        </div>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
          <textarea
            id="outputConfig"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskConfig))}
          />
        </div>
      </div>
    );
  }
}
