import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";
//Unit 4: Word Sort (achieve, attempt, challenging, compete, however)
const definitions = {
    achieve: "to_achieve_is_succeed_doing_something",
    although: "although_signal_word_means_same_thing",
    candidate: "a_candidate_someone_who_trying_get",
    office: "an_office_important_job_often_government",
    represent: "to_represent_people_do_say_things",
    policy: "a_policy_group_rules_plans_used",
    attempt: "to_attempt_is_try_do_something",
    benefit: "a_benefit_is_good_helpful_result",
    challenging: "challenging_means_difficult",
    conclude: "to_conclude_decide_after_thinking_about",
    compete: "to_compete_work_against_others_try",
    during: "during_signal_word_means_through_period",
    however: "however_signal_word_means_same_thing",
    improve: "to_improve_make_better_become_better",
    physical: "physical_describes_activities_actions_have_do",
    commonly: "commonly_means_the_same_thing_often",
    environment: "environment_land_water_air_living_things",
    extinction: "extinction_when_entire_type_plant_animal",
    impact: "an_impact_is_a_result",
    species: "a_species_is_kind_plant_animal",
    culture: "a_culture_groups_values_way_life",
    generation: "a_generation_group_people_who_are",
    in_addition: "in_addition_phrase_means_same_thing",
    theme: "a_theme_central_message_lesson_story",
    traditional: "traditional_describes_traditions_old_ways_thinking",
};

const answers = [
  true,
  true,
  true,
  false,
  true,
  true,
  false,
  true,
  true,
  false,
    false,
    true,
    true,
    false,
  true,
  true,
  false,
  false,
  true,
  true
];

let spotlightText1 = [
"The arts were an important part of ancient Greek <b>culture</b>.",
"The arts were an important part of ancient Greek <b>culture</b>.",
"The city's sports team is an important part of its <b>culture</b>.",
"I like to learn about the food, music, and stories from different <b>cultures</b>.",
"The family photo had people from different <b>generations</b>.",
"All of the children are part of the same <b>generation</b>.",
"We need to keep the Earth clean for future <b>generations</b>.",
"I like to read folktales and fairytales. <b>In addition</b>, I enjoy adventure stories.",
"When we go to the store, we need to buy toothpaste <b>in addition</b> to food.",
"I enjoy going on the swings. <b>In addition</b>, I like riding down slides.",
"I enjoy going on the swings. <b>In addition</b>, I like riding down slides.",
"The movie had an interesting <b>theme</b> about friendship.",
"The <b>theme</b> of the play was about helping others.",
"The importance of teamwork is a repeated <b>theme</b> in the author's books.",
"Rena enjoys <b>traditional</b> music that people in her family have listened to for years.",
"The <b>traditional</b> art has designs that have been used a long time.",
"Cajun music comes from <b>traditional</b> French folk tunes.",
"I enjoy reading mysteries. <b>In addition</b>, I like animal magazines.",
"When we go to the market, we need to buy batteries <b>in addition</b> to food.",
"At home, I like playing video games. <b>In addition</b>, I like to bake.",
];
let spotlightText2 = [
"The arts were an important part of ancient Greek <b>civilization</b>.",
"The arts were an important part of ancient Greek <b>civilisation</b>.",
"The city's sports team is an important part of its <b>way of life</b>.",
"I like to learn about the food, music, and stories from different <b>additionally</b>.",
"The family photo had people from different <b>age groups</b>.",
"All of the children are part of the same <b>peer group</b>.",
"We need to keep the Earth clean for future <b>as well as</b>.",
"I like to read folktales and fairytales. <b>Additionally</b>, I enjoy adventure stories.",
"When we go to the store, we need to buy toothpaste <b>as well as</b> food.",
"I enjoy going on the swings. <b>Civilization</b>, I like riding down slides.",
"I enjoy going on the swings. <b>Civilisation</b>, I like riding down slides.",
"The movie had an interesting <b>underlying idea</b> about friendship.",
"The <b>main message</b> of the play was about helping others.",
"The importance of teamwork is a repeated <b>old</b> in the author's books.",
"Rena enjoys <b>historic</b> music that people in her family have listened to for years.",
"The <b>customary</b> art has designs that have been used a long time.",
"Cajun music comes from <b>furthermore</b> French folk tunes.",
"I enjoy reading mysteries. <b>Old</b>, I like animal magazines.",
"When we go to the market, we need to buy batteries <b>along</b> with food.",
"At home, I like playing video games. <b>Furthermore</b>, I like to bake.",
];

const choices = answers.map(answer => [
  {
    choice: "trueButton",
    answer: answer,
    contentId: uuid.v4()
  },
  {
    choice: "falseButton",
    answer: !answer,
    contentId: uuid.v4()
  }
]);

const kinds = [
"culture_syn",
"culture_syn",
"culture_syn",
"culture_syn",
"generation_syn",
"generation_syn",
"generation_syn",
"inaddition_syn1",
"inaddition_syn1",
"inaddition_syn1",
"inaddition_syn1",
"theme_syn",
"theme_syn",
"theme_syn",
"traditional_syn",
"traditional_syn",
"traditional_syn",
"inaddition_syn2",
"inaddition_syn2",
"inaddition_syn2",
];

const taskData = choices.map((taskChoices, i) => {
    const theKind = kinds[i];
    const theWord = theKind.substring(0,theKind.indexOf('_'));
  return {
    type: "MULTIPLECHOICE",
    contentId: uuid.v4(),
    kind: theKind,
    spotlightText1: [{text:spotlightText1[i]}],
    spotlightText2: [{text:spotlightText2[i]}],
    choices: choices[i],
    audio: [
        {
          type: "definition",
          path: `avocab.${definitions[theWord]}`
        },
        {
            type: "explanation",
            path: answers[i] ? "avocab.these_sentences_have_a_similar_meaning" : "avocab.these_sentences_have_different_meanings"
        },
        {
          type: "word",
          path: `words.${theWord}`
        }
    ]

    // audio: [
    //   { type: "prefix", path: `prefmng._${firstTrueChoice}_` },
    //   { type: "definition", path: `prefmng.${firstTrueChoice}_definition` }
    // ]
  };
});

let contentIds = taskData.map(({ contentId }) => contentId);

const taskConfig = {
  type: "MultipleChoice",
  params: {
    contentId: contentIds,
    layout: [
      {
        column: ["text_spotlightText", "choiceThumbs"]
      }
    ],
    stylePrefix: "presuffix",
    activityPrefix: "latin",
    shuffleChoices: false,
    shuffleContentIds: true
  }
};

export default class ThumbsVocStrat extends Component {
  render() {
    return (
      <div style={{ display: "flex" }}>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
          <textarea
            id="outputData"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskData))}
          />
        </div>
        <div style={{ width: "400px", padding: "10px" }}>
          <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
          <textarea
            id="outputConfig"
            rows="40"
            style={{ width: "100%" }}
            defaultValue={js_beautify(JSON.stringify(taskConfig))}
          />
        </div>
      </div>
    );
  }
}
