import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

let intros = {
  adjective: "Adjectives tell more about nouns.",
  adverb:
    "Adverbs are words that describe the verb. Some adverbs tell WHERE the action happens, some tell WHEN the action happens, and some tell HOW the action happens.",
  prepositional_phrase:
    "Some prepositional phrases tell WHEN the action happens, and some tell WHERE the action happens.",
  pronouns_singular: "Pronouns take the place of nouns.",
  conjunction:
    "Conjunctions, like the words AND, BUT, SO, and OR, show how ideas in a sentence are related.",
  pronouns_plural: "Pronouns take the place of nouns.",
  verb_future_tense:
    "Future tense verbs show action that will happen later. The word 'will' usually comes before the verb.",
  noun:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  verb:
    "A noun is a part of speech that names a person, place, thing, or idea. A verb is a part of speech that shows an action.",
  simple_subject_predicate:
    "The simple subject is the noun or pronoun that tells who or what the sentence is about. It answers the question, 'Who or what is doing the action?' The simple predicate is the verb that tells what the subject does or did. It answers the question, 'What is the action?'",
  relative_pronoun:
    "The words THAT, WHICH, WHO, WHOSE, and WHOM can be relative pronouns. They give more information about nouns.",
  verb_past_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  verb_present_tense:
    "A PAST tense verb usually ends in the suffix -ed. It tells us that the action ALREADY happened. A PRESENT tense verb either does not have a suffix or it ends in the suffix -s. It tells us that the action is happening RIGHT NOW. ",
  possessive_noun:
    "A possessive noun, like the word STUDENT'S, shows belonging. It has an apostrophe.",
  interjection:
    "An interjection is a word that shows strong feeling or emphasis. It's interesting and sometimes fun but it is not necessary. It CAN be left out."
};

const wordSpotlights = [
  ["tree"],
  ["Apples"],
  ["She"],
  ["puppy"],
  ["slithered"],
  ["wrote"],
  ["whom I met yesterday"],
  ["that we saw"],
  ["who is Canadian"],
  ["which broke down"],
  ["whose tail is orange"],
  ["who is my friend"],
  ["The prince"],
  ["That boy"],
  ["My sister"],
  ["Ann"],
  ["A cactus"],
  ["The telephone"],
  ["and"],
  ["and"],
  ["and"],
  ["or"],
  ["or"],
  ["or"],
  ["tail"],
  ["poem"],
  ["leaves"],
  ["petals"],
  ["joke"],
  ["pie"],
  ["loud"],
  ["green"],
  ["Nine"],
  ["Several"],
  ["Those"],
  ["That"],
  ["in the swamp"],
  ["on the desk"],
  ["above his head"],
  ["at bedtime"],
  ["before the race"],
  ["during the summer"]
];
// const words = ["when", "month", "spot", "reason", "season", "moment", "idea"];

const sentences = [
  "A tree grew in the woods.",
  "Apples fall on the ground.",
  "She dove into the pool. ",
  "The puppy barks often.",
  "The snake slithered quietly.",
  "He wrote a story. ",
  "Allie, whom I met yesterday, is very kind.",
  "The band that we saw plays rock music.",
  "My mother, who is Canadian, speaks two languages.",
  "The red car, which broke down, is very old.",
  "The cat, whose tail is orange, drank milk.",
  "The boy, who is my friend, loves football.",
  "The prince kneels. ",
  "That boy rests.",
  "My sister walks.",
  "Ann pretends.",
  "A cactus grows.",
  "The telephone rings.",
  "The dolphins swim, and they eat fish.",
  "The dog ran, and it jumped.",
  "He likes music, and he likes art.",
  "Objects sink, or they float.",
  "The kittens play, or they nap.",
  "I eat a big breakfast, or I am hungry.",
  "The dog's tail wags.",
  "I like Sara's poem.",
  "The tree's leaves will fall.",
  "The flower's petals were bright pink.",
  "The woman's joke was hilarious.",
  "Marc's pie will taste delicious.",
  "A loud bell rings.",
  "A green frog jumps.",
  "Nine bells ring.",
  "Several frogs jump.",
  "Those bells ring.",
  "That frog jumps.",
  "The bugs live in the swamp.",
  "A pencil sits on the desk. ",
  "A bird flew above his head.",
  "The parents read at bedtime.",
  "The runners stretched before the race. ",
  "The people swim during the summer."
];

// const prompts = [
//   "",
//   "when we are going",
//   "where he hid the book",
//   "why she left",
//   "when it snows the most",
//   "when we first met",
//   "why you're hungry"
// ];

const solutions = [
  "TREE is a simple subject in this sentence.",
  "APPLES is a simple subject in this sentence.",
  "SHE is a simple subject in this sentence.",
  "PUPPY is a simple subject in this sentence.",
  "SLITHERED is a simple predicate in this sentence.",
  "WROTE is a simple predicate in this sentence.",
  "WHOM I MET YESTERDAY describes Allie.",
  "THAT WE SAW describes the band.",
  "WHO IS CANADIAN describes my mother.",
  "WHICH BROKE DOWN describes the car.",
  "WHOSE TAIL IS ORANGE describes the cat.",
  "WHO IS MY FRIEND describes boy.",
  "HE replaces the prince.",
  "HE replaces that boy.",
  "SHE replaces my sister.",
  "SHE replaces Ann.",
  "IT replaces a cactus.",
  "IT replaces the telephone.",
  "The conjunction AND adds information.",
  "The conjunction AND adds information.",
  "The conjunction AND adds information.",
  "The conjunction OR gives a choice.",
  "The conjunction OR gives a choice.",
  "The conjunction OR gives a choice.",
  "The tail belongs to the dog.",
  "The poem belongs to Sara.",
  "The leaves belong to the tree.",
  "The petals belong to the flower.",
  "The joke belongs to the woman.",
  "The pie belongs to Marc.",
  "LOUD tells what kind.",
  "GREEN tells what kind.",
  "NINE tells how many.",
  "SEVERAL tells how many.",
  "THOSE tells which one.",
  "THAT tells which one.",
  "IN THE SWAMP tells where.",
  "ON THE DESK tells where.",
  "ABOVE HIS HEAD tells where.",
  "AT BEDTIME tells when.",
  "BEFORE THE RACE tells when.",
  "DURING THE SUMMER tells when."
];

const choices = [
  ["Subject  (who/what)*", "Predicate  (action)"],
  ["Subject  (who/what)*", "Predicate  (action)"],
  ["Subject  (who/what)*", "Predicate  (action)"],
  ["Subject  (who/what)*", "Predicate  (action)"],
  ["Subject  (who/what)", "Predicate  (action)*"],
  ["Subject  (who/what)", "Predicate  (action)*"],
  ["Allie*", "very"],
  ["band*", "music"],
  ["mother*", "languages"],
  ["car*", "old"],
  ["cat*", "milk"],
  ["boy*", "football"],
  ["He*", "She", "It"],
  ["He*", "She", "It"],
  ["He", "She*", "It"],
  ["He", "She*", "It"],
  ["He", "She", "It*"],
  ["He", "She", "It*"],
  ["Adds Information*", "Gives a Choice"],
  ["Adds Information*", "Gives a Choice"],
  ["Adds Information*", "Gives a Choice"],
  ["Adds Information", "Gives a Choice*"],
  ["Adds Information", "Gives a Choice*"],
  ["Adds Information", "Gives a Choice*"],
  ["dog*", "wags"],
  ["Sara*", "like"],
  ["tree*", "fall"],
  ["flower*", "bright"],
  ["woman*", "hilarious"],
  ["Marc*", "delicious"],
  ["What Kind*", "How Many", "Which One"],
  ["What Kind*", "How Many", "Which One"],
  ["What Kind", "How Many*", "Which One"],
  ["What Kind", "How Many*", "Which One"],
  ["What Kind", "How Many", "Which One*"],
  ["What Kind", "How Many", "Which One*"],
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where*", "When"],
  ["Where", "When*"],
  ["Where", "When*"],
  ["Where", "When*"]
];

const kinds = [
  "simple_subject_predicate",
  "simple_subject_predicate",
  "simple_subject_predicate",
  "simple_subject_predicate",
  "simple_subject_predicate",
  "simple_subject_predicate",
  "relative_pronoun",
  "relative_pronoun",
  "relative_pronoun",
  "relative_pronoun",
  "relative_pronoun",
  "relative_pronoun",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "pronouns_singular",
  "conjunction",
  "conjunction",
  "conjunction",
  "conjunction",
  "conjunction",
  "conjunction",
  "possessive_noun",
  "possessive_noun",
  "possessive_noun",
  "possessive_noun",
  "possessive_noun",
  "possessive_noun",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "adjective",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase",
  "prepositional_phrase"
];

export default class GrmmrSortingBoxes extends Component {
  componentDidMount = () => {
    const spotlightText = this.makeNestedSpotlightText(wordSpotlights);
    const spotlightSentence = makeSpotlightText(
      this.highlightWords(sentences, wordSpotlights)
    );
    const taskData = choices.map((taskChoices, i) => {
      let answerChoice = taskChoices
        .find(tc => tc.indexOf("*") > -1)
        .replace("*", "");
      return {
        type: "MULTIPLECHOICE",
        contentId: uuid.v4(),
        kind: kinds[i],
        spotlightSentence: spotlightSentence[i],
        spotlightText: spotlightText[i],
        choices: this.makeChoices(taskChoices),
        audio: [
          {
            type: "intro",
            path: `grmmr.${filenameGenerator(intros[kinds[i]])}`
          },
          {
            type: "explanation",
            path: `grmmr.${filenameGenerator(solutions[i])}`
          }
        ]
      };
    });
    let contentIds = taskData.map(({ contentId }) => contentId);
    const taskConfig = {
      type: "MultipleChoice",
      params: {
        contentId: contentIds,
        layout: [
          {
            column: ["text_spotlightSentence", "sortingBoxes"]
          }
        ],
        stylePrefix: "grmmr1_sort",
        shuffleChoices: false,
        shuffleContentIds: true
      }
    };

    this.setState({
      taskData,
      taskConfig
    });
  };

  highlightWords = (sentences, words) => {
    return sentences.map((s, i) => {
      let result = s;
      words[i].forEach(word => {
        result = result.replace(word, `<h>${word}</h>`);
        result = result.toLowerCase().replace(word, `<h>${word}</h>`);
      });
      return result;
    });
  };

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      return {
        choice: c.replace("*", ""),
        answer,
        contentId: uuid.v4()
      };
    });
  };

  makeNestedSpotlightText = (textArrays, audioKey) => {
    audioKey = audioKey ? audioKey + "." : "";
    return textArrays.map(textArr => {
      return textArr.map(t => {
        const slt = { text: t };
        return slt;
      });
    });
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
