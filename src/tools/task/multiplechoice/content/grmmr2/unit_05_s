import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

let bubbleImages = {
  gives_a_choice: "grmmr_tag_choice",
  shows_difference: "grmmr_tag_difference",
  how: "grmmr_tag_how",
  how_many: "grmmr_tag_how_many",
  adds_information: "grmmr_tag_information",
  interjection: "grmmr_tag_interjection",
  simple_predicate: "grmmr_tag_predicate",
  whats_the_result: "grmmr_tag_result",
  simple_subject: "grmmr_tag_subject",
  tells_result: "grmmr_tag_tells_result",
  what_kind: "grmmr_tag_what_kind",
  when: "grmmr_tag_when",
  when_q: "grmmr_tag_when_2",
  where: "grmmr_tag_where",
  which_one: "grmmr_tag_which_one",
  why: "grmmr_tag_why"
};

const spotlightImages = [
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate"
];

// const prompts = [
//   "On weekdays",
//   "Before the match",
//   "Throughout the day",
//   "At night",
//   "During that class",
//   "After midnight",
//   "Under the dock",
//   "On the pole",
//   "Near the stream",
//   "Through the grass",
//   "In the kitchen",
//   "Over the valley"
// ];

const spotlightText = makeSpotlightText([
  "A young girl sat at the table.",
  "A young girl sat at the table.",
  "My old markers write poorly.",
  "My old markers write poorly.",
  "Three kittens nap in the barn.",
  "Three kittens nap in the barn.",
  "They jumped over the hurdles.",
  "They jumped over the hurdles.",
  "A tiny ant walked slowly.",
  "A tiny ant walked slowly.",
  "Some leaves flutter in the wind.",
  "Some leaves flutter in the wind."
]);

const choices = [
  ["young", "girl*", "sat"],
  ["young", "sat*", "at"],
  ["old", "markers*", "write"],
  ["My", "old", "write*"],
  ["kittens*", "nap", "barn"],
  ["nap*", "in", "	barn"],
  ["They*", "jumped", "over"],
  ["jumped*", "over", "the"],
  ["ant*", "walked", "slowly"],
  ["tiny", "walked*", "slowly"],
  ["Some", "leaves*", "flutter"],
  ["flutter*", "in", "the"]
];

const kinds = [
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate",
  "simple_subject",
  "simple_predicate"
];

export default class GrmmrMCqa extends Component {
  componentDidMount = () => {
    const taskData = this.getTaskData();
    const contentIds = taskData.map(({ contentId }) => contentId);

    this.setState({
      contentIds: contentIds,
      taskData: taskData,
      taskConfig: this.getTaskConfig(contentIds)
    });
  };

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      return {
        choice: c.replace("*", ""),
        answer,
        contentId: uuid.v4()
      };
    });
  };

  getTaskData = () => {
    return choices.map((taskChoices, i) => {
      return {
        type: "MULTIPLECHOICE",
        contentId: uuid.v4(),
        kind: kinds[i],
        spotlightImage: [{ image: `spotlight_tags.${spotlightImages[i]}` }],
        spotlightText: spotlightText[i],
        choices: this.makeChoices(choices[i]),
        audio: [
          {
            type: "prompt",
            path:
              kinds[i] === "simple_subject"
                ? "grmmr.choose_the_simple_subject_in_sentence"
                : "choose_the_simple_predicate_in_sentence"
          }
        ]
      };
    });
  };

  getTaskConfig = contentIds => {
    return {
      type: "MultipleChoice",
      params: {
        contentId: contentIds,
        layout: [
          {
            column: [
              "spotlightImage",
              "text_spotlightText",
              "choiceColumnBorders"
            ]
          }
        ],
        stylePrefix: "grmmr1_mc",
        shuffleChoices: false,
        shuffleContentIds: true
      }
    };
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
