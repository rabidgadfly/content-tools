import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

import { makeSpotlightText } from "../../../util/makeSpotlightText";

const spotlightText = [
      "treetop",
      "starlight",
      "night",
      "worldwide",
      "storyteller",
      "author",
      "stonecutter",
      "rosebuds",
      "flowers",
      "tightrope",
      "eyesight",
      "bravery",
  ];

const answers = [
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    true,
    false,
];

const kinds = [
    "compound5",
    "compound5",
    "compound5",
    "compound6",
    "compound6",
    "compound6",
    "compound7",
    "compound7",
    "compound7",
    "compound8",
    "compound8",
    "compound8",
];


export default class ThumbsVocStrat extends Component {
    componentDidMount = () => {
        const choices = answers.map(answer => [
            {
                choice: "trueButton",
                answer: answer,
                contentId: uuid.v4()
            },
            {
                choice: "falseButton",
                answer: !answer,
                contentId: uuid.v4()
            }
        ]);
        const taskData = this.getTaskData(choices);
        const contentIds = taskData.map(({contentId}) => contentId);

        this.setState({
            contentIds: contentIds,
            taskData: taskData,
            taskConfig: this.getTaskConfig(contentIds)
        });
    };

    getTaskData = choices => choices.map((taskChoices, i) => {
        return {
            type: "MULTIPLECHOICE",
            contentId: uuid.v4(),
            kind: kinds[i],
            spotlightText: [
                {
                    "text": spotlightText[i],
                    "audio": `words.${spotlightText[i]}`
                }
            ],
            choices: taskChoices
        };
    });

    getTaskConfig = contentIds => {
        return ({
            type: "MultipleChoice",
            params: {
                contentId: contentIds,
                "layout": [
                    {
                        "column": [
                            "text_spotlightText",
                            "choiceThumbs"
                        ]
                    }
                ],
                "stylePrefix": "vocstrat_thumbs",
                "activityPrefix": "latin",
                "shuffleChoices": false,
                "shuffleContentIds": true
            }
        })
    };

    render() {
        if (this.state && this.state.taskData) {
            return (
                <div style={{display: "flex"}}>
                    <div style={{width: "400px", padding: "10px"}}>
                        <h3 style={{fontFamily: "Helvetica"}}>Data</h3>
                        <textarea
                            id="outputData"
                            rows="40"
                            style={{width: "100%"}}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
                        />
                    </div>
                    <div style={{width: "400px", padding: "10px"}}>
                        <h3 style={{fontFamily: "Helvetica"}}>Config</h3>
                        <textarea
                            id="outputConfig"
                            rows="40"
                            style={{width: "100%"}}
                            defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
                        />
                    </div>
                </div>
            );
        }
        return <div/>;
    }
}
