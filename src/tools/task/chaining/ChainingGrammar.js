import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

const dropZoneBlueprints = [
"<insert> The <insert> student <insert> listened <insert*>.",
"<The> <student> <listened*> <carefully>.",
"<insert> The <insert> large <insert> pigs <insert> eat <insert*>.",
"<The large pigs*> <eat> <quickly>.",
"<insert> They <insert> eat <insert> quickly <insert*>.",
"<insert> They <insert> eat <insert> quickly <insert*> they <insert> are <insert> hungry <insert>.",
"<insert> I <insert> lost <insert> my <insert*> keys <insert>.",
"<insert> I <insert> lost <insert> my <insert> new <insert> keys <insert*>.",
"<insert*> I <insert> lost <insert> my <insert> new <insert> keys <insert> in the grass <insert>.",
"<We> <see*> <a> <play>.",
"<insert> We <insert> will see <insert> a <insert> play <insert*>.",
"<insert*> We <insert> will see <insert> a <insert> play <insert> at the theater <insert>.",
];

const choices = [
["carefully*",	"during class",	"thoughtful",	"inside"],
["listens*",	"will listen",	"will talk",	"talked"],
["quickly*",	"pink",	"today",	"busy"],
["They*",	"We",	"Them",	"Us"],
["in the field*",	"at dawn",	"the pigs",	"happily"],
["because*",	"so",	"very",	"Yum!"],
["new*",	"last week",	"those",	"Bill and"],
["in the grass*",	"yesterday",	"shiny",	"because"],
["Ugh!*",	"tall",	"on Tuesday",	"and Simon"],
["will see*",	"saw",	"funny",	"Yikes!"],
["at the theater*",	"yesterday",	"dramatic",	"with my family"],
["Yay!*",	"in two days",	"musical",	"downtown"],
];

let presentations = [
"Add an adverb that tells HOW the student listened.",
"Change the verb so that the sentence happens NOW.",
"Add an adverb that tells HOW to the end of the sentence.",
"Change the large pigs to the correct pronoun.",
"Add a prepositional phrase that tells WHERE to the end of the sentence.",
"Add a conjunction that tells WHY to make a complete sentence.",
"Add an adjective that tells WHAT KIND.",
"Add an adjective that tells WHERE.",
"Add an interjection to the beginning of the sentence.",
"Change the verb so that the sentence happens in the FUTURE.",
"Add a prepositional phrase that tells WHERE to the end of the sentence.",
"Add an interjection to the beginning of the sentence.",
];

let solutions = [
"The student listened carefully.",
"The student listens carefully.",
"The large pigs eat quickly.",
"They eat quickly.",
"They eat quickly in the field.",
"They eat quickly because they are hungry.",
"I lost my new keys.",
"I lost my new keys in the grass.",
"Ugh! I lost my new keys in the grass.",
"We will see a play.",
"We will see a play at the theater.",
"Yay! We will see a play at the theater.",
];

const kind = [
"",
"",
"adverb",
"pronouns_plural",
"prepositional_phrase",
"conjunction_noun_verb",
"adjective",
"prepositional_phrase",
"interjection",
"verb_future_tense",
"prepositional_phrase",
"interjection",
];

export default class ChainingGrammar extends Component {
  componentDidMount = () => {
    let allDropZones = dropZoneBlueprints.map((bp, i) => {
      let dropZones = [];
      let dropZoneEnd = 0;
      let chainingType = bp.indexOf("insert*") > -1 ? "insert" : "replace";
      let endlessLoopKillerCounter = 0;
      while (dropZoneEnd > -1 && endlessLoopKillerCounter < 999) {
        bp = bp.substring(dropZoneEnd).trim();
        let nextTagStart = bp.indexOf("<");
        let nextTagEnd = bp.indexOf(">");
        let isAnswer = false;

        if (nextTagStart > -1) {
          if (nextTagStart === 0) {
            let textBetweenTags = bp.substring(nextTagStart + 1, nextTagEnd);
            console.log("textBetweenTags", textBetweenTags);
            isAnswer = textBetweenTags.indexOf("*") > -1;
            // dropZone: textBetweenTags.replace("*", "").trim(),
            dropZones.push({
              dropZone: isAnswer
                ? choices[i].find(c => c.indexOf("*") > 1).replace("*", "")
                : textBetweenTags === "insert"
                ? ""
                : textBetweenTags,
              answer: isAnswer,
              type: chainingType
            });
            // console.warn("textBetweenTags", textBetweenTags);
            dropZoneEnd = nextTagEnd + 1;
          } else if (nextTagStart > 0) {
            let fragment = bp.substring(0, nextTagStart);
            // console.warn("fragment", fragment);
            dropZones.push({
              dropZone: fragment.trim(),
              answer: false,
              type: chainingType,
              preventDrops: true
            });
            dropZoneEnd = nextTagStart;
          }
        } else {
          let finalFragment = bp;
          // console.warn("final", finalFragment);
          dropZones.push({
            dropZone: finalFragment.trim(),
            answer: false,
            type: chainingType,
            preventDrops: true
          });
          dropZoneEnd = -1;
          return dropZones;
        }

        endlessLoopKillerCounter++;
      }
    });

    let allChoices = choices.map((taskChoices, i) => {
      const kind =
        dropZoneBlueprints[i].indexOf("insert*") > -1 ? "insert" : "replace";
      return taskChoices.map(c => {
        return {
          contentId: uuid.v4(),
          choice: c.replace("*", ""),
          answer: c.indexOf("*") > -1,
          kind
        };
      });
    });




    let allPrefills = dropZoneBlueprints.map((bp, i) => {
      let prefills = [];
      let chainingType = bp.indexOf("insert*") > -1 ? "insert" : "replace";
      let endlessLoopKillerCounter = 0;
      let processing = true;
      while (processing && endlessLoopKillerCounter < 999) {
        let nextTagStart = bp.indexOf("<");
        if (processing) {

          // if( nextTagStart < 2 ) {
          //   bp = bp.substring(nextTagStart);
          // }


        let isFragment = nextTagStart > 1;
          // bp = bp.substring(nextTagStart);

          let nextTagEnd = bp.indexOf(">");
          let isAnswer = false;
          if (bp.length) {

            if (nextTagStart === 0) {
              let textBetweenTags = bp.substring(nextTagStart + 1, nextTagEnd);

              if (textBetweenTags.indexOf("insert") === -1) {
                isAnswer = textBetweenTags.indexOf("*") > -1;
                prefills.push({
                  choice: textBetweenTags.replace("*", ""),
                  answer: isAnswer,
                  kind: chainingType,
                  contentId: uuid.v4()
                });
              }
            } else {
              let fragment = bp.substring(
                0,
                nextTagStart > -1 ? nextTagStart : bp.length
              );

              prefills.push({
                choice: fragment.trim(),
                answer: false,
                kind: chainingType,
                contentId: uuid.v4()
              });
              if (nextTagStart === -1) {
              }
            }

            if (nextTagEnd === bp.length - 1) {
              processing = false;
            } else {
              if (nextTagEnd > -1) {
                if( isFragment ) {
                  bp = bp.substring(nextTagStart).trim();
                } else {
                  bp = bp.substring(nextTagEnd + 1).trim();
                }

              } else {
                processing = false;
              }
            }
          }
        }
        endlessLoopKillerCounter++;
      }
      return prefills;
    });

    const rounds = allDropZones.map((dzs, i) => {
      return {
        type: "CHAINING",
        contentId: uuid.v4(),
        kind: kind[i],
        dropZones: dzs,
        choices: allChoices[i],
        preFills: allPrefills[i],
        solution: solutions[i],
        audio: [
          {
            type: "prompt",
            path: `words.${filenameGenerator(presentations[i])}`
          }
        ]
      };
    });

    let contentIds = rounds.map(({ contentId }) => contentId);

    let config = {
      type: "Chaining",
      params: {
        contentId: contentIds,
        borderedDropZone: true,
        layout: "evalOnSubmitFixed",
        showSpotlightHint: true,
        stylesuffix: "vocstrat_cmb",
        shuffleChoices: true,
        useSubmitButton: true,
        submitPosition: "side",
        suppressCorrectDefault: true,
        suppressIncorrectDefault: true
      }
    };

    this.setState({ rounds, config });
  };

  render() {
    if (this.state && this.state.rounds) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.rounds))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.config))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
