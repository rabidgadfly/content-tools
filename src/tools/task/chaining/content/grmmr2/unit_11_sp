import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

const dropZoneBlueprints = [
  "<insert*> bugs <insert> walked.",
  "<Five> <bugs> <walked*>.",
  "<Bo> <and> <I> <eat*> <salad>.",
  "<Bo and I*> <will eat> <salad>.",
  "<insert> We <insert> will eat <insert*> salad <insert>.",
  "<insert> We <insert> will eat <insert> healthy <insert> salad <insert*> junk food <insert>.",
  "<insert> The <insert*> sisters <insert> chatted.",
  "<insert> The <insert> twin <insert> sisters <insert> chatted <insert*>.",
  "<insert> The <insert> twins <insert> chatted, <insert*> they <insert> drank <insert> tea <insert>.",
  "<The> <frog*> <jumps>.",
  "<The> <cow> <jumps*>.",
  "<insert> The <insert> cow <insert> jumped <insert*>."
];

const choices = [
  ["Red", "Five*", "Those", "These"],
  ["will walk*", "walk", "crawled", "crawl"],
  ["will eat*", "ate", "crunchy", "tomorrow"],
  ["We*", "They", "Bo", "The player"],
  ["healthy*", "in the kitchen", "slowly", "ate"],
  ["or*", "so	delicious", "tomorrow"],
  ["twin*", "two", "my", "will chat"],
  ["at night*", "outside", "there", "at school"],
  ["and*", "but", "happily", "will chat"],
  ["cow*", "green", "slowly", "will jump"],
  ["jumped*", "will jump", "in the garden", "for friends"],
  ["over the moon*", "tomorrow", "quickly", "at night"]
];

let presentations = [
  "Add an adjective that tells HOW MANY bugs.",
  "Change the verb so that the sentence happens in the FUTURE.",
  "Change the verb so that the sentence happens in the FUTURE.",
  "Change Bo and I to the correct pronoun.",
  "Add an adjective that tells WHAT KIND of salad.",
  "Add a conjunction that gives a choice.",
  "Add an adjective that tells WHAT KIND of sisters.",
  "Add a prepositional phrase that tells WHEN to the end of the sentence.",
  "Add a conjunction that adds information.",
  "Change the simple subject so that the sentence is about a different simple subject.",
  "Change the present tense verb to a PAST tense verb.",
  "Add a prepositional phrase that tells WHERE to the end of the sentence."
];

let solutions = [
  "Five bugs walked.",
  "Five bugs will walk.",
  "Bo and I will eat salad.",
  "We will eat salad.",
  "We will eat healthy salad.",
  "We will eat healthy salad or junk food.",
  "The twin sisters chatted.",
  "The twin sisters chatted at night.",
  "The twins chatted, and they drank tea.",
  "The cow jumps.",
  "The cow jumped.",
  "The cow jumped over the moon."
];

const kind = [
  "",
  "",
  "verb_future_tense",
  "pronouns_plural",
  "adjective",
  "conjunction",
  "adjective",
  "prepositional_phrase",
  "conjunction",
  "simple_subject_predicate",
  "verb_past_tense",
  "prepositional_phrase"
];

export default class ChainingGrammar extends Component {
  componentDidMount = () => {
    let allDropZones = dropZoneBlueprints.map((bp, i) => {
      let dropZones = [];
      let dropZoneEnd = 0;
      let chainingType = bp.indexOf("insert*") > -1 ? "insert" : "replace";
      let endlessLoopKillerCounter = 0;
      while (dropZoneEnd > -1 && endlessLoopKillerCounter < 999) {
        bp = bp.substring(dropZoneEnd).trim();
        console.log("bp", bp);
        let nextTagStart = bp.indexOf("<");
        let nextTagEnd = bp.indexOf(">");
        let isAnswer = false;

        if (nextTagStart > -1) {
          if (nextTagStart === 0) {
            let textBetweenTags = bp.substring(nextTagStart + 1, nextTagEnd);
            console.log("textBetweenTags", textBetweenTags);
            isAnswer = textBetweenTags.indexOf("*") > -1;
            // dropZone: textBetweenTags.replace("*", "").trim(),
            dropZones.push({
              dropZone: isAnswer
                ? choices[i].find(c => c.indexOf("*") > 1).replace("*", "")
                : textBetweenTags === "insert"
                ? ""
                : textBetweenTags,
              answer: isAnswer,
              type: chainingType
            });
            // console.warn("textBetweenTags", textBetweenTags);
            dropZoneEnd = nextTagEnd + 1;
          } else if (nextTagStart > 0) {
            let fragment = bp.substring(0, nextTagStart);
            // console.warn("fragment", fragment);
            dropZones.push({
              dropZone: fragment.trim(),
              answer: false,
              type: chainingType,
              preventDrops: true
            });
            dropZoneEnd = nextTagStart;
          }
        } else {
          let finalFragment = bp;
          // console.warn("final", finalFragment);
          dropZones.push({
            dropZone: finalFragment.trim(),
            answer: false,
            type: chainingType,
            preventDrops: true
          });
          dropZoneEnd = -1;
          return dropZones;
        }

        endlessLoopKillerCounter++;
      }
    });

    let allChoices = choices.map((taskChoices, i) => {
      const kind =
        dropZoneBlueprints[i].indexOf("insert*") > -1 ? "insert" : "replace";
      return taskChoices.map(c => {
        return {
          contentId: uuid.v4(),
          choice: c.replace("*", ""),
          answer: c.indexOf("*") > -1,
          kind
        };
      });
    });

    let allPrefills = dropZoneBlueprints.map((bp, i) => {
      let prefills = [];
      let chainingType = bp.indexOf("insert*") > -1 ? "insert" : "replace";
      let endlessLoopKillerCounter = 0;
      let processing = true;
      while (processing && endlessLoopKillerCounter < 999) {
        let nextTagStart = bp.indexOf("<");
        if (processing) {
          // bp = bp.substring(nextTagStart);
          let nextTagEnd = bp.indexOf(">");
          let isAnswer = false;

          if (bp.length) {
            if (nextTagStart === 0) {
              let textBetweenTags = bp.substring(nextTagStart + 1, nextTagEnd);
              // console.warn("textBetweenTags", textBetweenTags);
              if (textBetweenTags.indexOf("insert") === -1) {
                isAnswer = textBetweenTags.indexOf("*") > -1;
                prefills.push({
                  choice: textBetweenTags.replace("*", ""),
                  answer: isAnswer,
                  kind: chainingType,
                  contentId: uuid.v4()
                });
              }
            } else {
              let fragment = bp.substring(
                0,
                nextTagStart > -1 ? nextTagStart : bp.length
              );
              // console.warn("fragment", fragment);
              prefills.push({
                choice: fragment.trim(),
                answer: false,
                kind: chainingType,
                contentId: uuid.v4()
              });
              if (nextTagStart === -1) {
              }
            }

            if (nextTagEnd === bp.length - 1) {
              processing = false;
            } else {
              if (nextTagEnd > -1) {
                bp = bp.substring(nextTagEnd + 1).trim();
              } else {
                processing = false;
              }
            }
          }
        }
        endlessLoopKillerCounter++;
      }
      return prefills;
    });

    const rounds = allDropZones.map((dzs, i) => {
      return {
        type: "CHAINING",
        contentId: uuid.v4(),
        kind: kind[i],
        dropZones: dzs,
        choices: allChoices[i],
        preFills: allPrefills[i],
        solution: solutions[i],
        audio: [
          {
            type: "presentation",
            path: `grmmr1.${filenameGenerator(presentations[i])}`
          }
        ]
      };
    });

    let contentIds = rounds.map(({ contentId }) => contentId);

    let config = {
      type: "Chaining",
      params: {
        contentId: contentIds,
        borderedDropZone: true,
        layout: "evalOnSubmitFixed",
        showSpotlightHint: true,
        stylesuffix: "vocstrat_cmb",
        shuffleChoices: true,
        useSubmitButton: true,
        submitPosition: "side",
        suppressCorrectDefault: true,
        suppressIncorrectDefault: true
      }
    };

    this.setState({ rounds, config });
  };

  render() {
    if (this.state && this.state.rounds) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.rounds))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.config))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
