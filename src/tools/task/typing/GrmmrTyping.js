import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";
import { makeSpotlightText } from "../../../util/makeSpotlightText";

// const spotlightText = [
//   "laughs, laughed",
//   "hop, hopped",
//   "paints, painted",
//   "needs, needed",
//   "tastes, tasted",
//   "float, floated",
// ];

const answers = [
  "ouch",
  "phew",
  "aha",
  "right",
  "well",
  "ahh",
  "oops",
  "ahem",
  "yay",
  "hmm",
  "wow",
];

const sentencesWithBlank = [
  "Ouch! That really hurt!",
      "I just finished cleaning my very dirty room. Phew!",
      "Aha! I found the missing papers!",
      "You're going to the circus on Saturday afternoon, right?",
  "Well, I can't be sure that your striped socks are clean.",
  "Ahh, the cool ocean feels so refreshing on this hot day.",
      "I left some books outside in the rain. Oops!",
      "Ahem, please don't eat all of the chips.",
  "Yay! Our new chairs will arrive tomorrow afternoon!",
      "Hmm, what are those oranges doing in the freezer?",
      "Wow! They climbed to the top of that mountain!",
];

// const prompts = [
//   "Which word tells WHAT KIND of foxes?",
//   "Which word tells HOW MANY foxes?",
//   "Which word or phrase tells HOW they played?",
//   "Which word or phrase tells WHEN they played?",
//   "Which word is a conjunction that tells WHAT HAPPENS?",
//   "Which word tells HOW Evan sobbed?",
//   "Which word tells WHEN Evan sobbed?",
//   "Which action word completes the sentence?",
//   "Which pronoun relates to Jen and I? ",
//   "Which word tells WHAT KIND of puzzle?"
// ];

const kinds = [
  "interjection",
  "interjection",
  "interjection",
  "interjection",
  "interjection",
  "interjection",
  "interjection",
  "interjection",
  "interjection",
  "interjection",
  "interjection",
];

const makeChoice = c => {
  return {
    choice: c,
    answer: true,
    audio: `words.${c}`,
    contentId: uuid.v4()
  };
};

const makeChoices = round => {
  // console.warn("round", sentencesWithBlank[round]);
  let arr = sentencesWithBlank[round].split("_____");

  return [
    {
      choice: arr[0].trim(),
      answer: false,
      contentId: uuid.v4()
    },
    {
      choice: answers[round],
      answer: true,
      contentId: uuid.v4()
    },
    {
      choice: arr[1].trim(),
      answer: false,
      contentId: uuid.v4()
    }
  ];
};

const getTaskConfig = contentIds => {
  return {
    type: "Typing",
    params: {
      contentId: contentIds,
      layout: [{ column: ["textBorders_spotlightText", "typing_choices"] }],
      stylePrefix: "grmmr1_typing",
      shuffleChoices: false,
      shuffleContentIds: true
    }
  };
};

export default class GrmmrTyping extends Component {
  makeSpotlightTextFromString = textString => {
    let arr = textString.split(", ");
    return arr.map(slt => ({ text: slt }));
  };

  componentDidMount = () => {
    // let slText = makeSpotlightTextFromString(spotlightText);
    let taskData = sentencesWithBlank.map((taskChoices, i) => ({
      type: "TYPING",
      contentId: uuid.v4(),
      kind: kinds[i],
      // spotlightImage: [{ image: `grmmr1_graphic.${kinds[i]}` }],
      spotlightImage: [{image: "spotlight_tags.interjection"}],
      // spotlightText: this.makeSpotlightTextFromString(spotlightText[i]),
      spotlightText: [{text: taskChoices}],
      // choices: makeChoices(i)
      choices: [makeChoice(answers[i])]
      // audio: [
      //   {
      //     type: "prompt",
      //     path: `grmmr.${filenameGenerator(prompts[i])}`
      //   }
      // ]
    }));
    let contentIds = taskData.map(({ contentId }) => contentId);
    let taskConfig = getTaskConfig(contentIds);
    this.setState({ taskData, contentIds, taskConfig });
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
