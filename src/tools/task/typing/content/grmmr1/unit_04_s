import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";
import { makeSpotlightText } from "../../../util/makeSpotlightText";

const spotlightText = [
  "seems, seemed",
  "seems, seemed",
  "stomp, stomped",
  "want, wanted",
  "paints, painted",
  "belong, belonged",
  "visits, visited",
  "needs, needed",
  "plants, planted",
  "sounds, sounded",
  "cleans, cleaned",
  "looks, looked"
];

const answers = [
  "seemed",
  "seems",
  "stomped",
  "want",
  "painted",
  "belong",
  "visited",
  "needs",
  "planted",
  "sounds",
  "cleaned",
  "looks"
];

const choices = [
  "bee",
  "pals",
  "match",
  "cars",
  "ant",
  "show",
  "flew",
  "play",
  "ended",
  "race",
  "ran",
  "began"
];

const sentencesWithBlank = [
  "Yesterday, the kitten _____ tired.",
  "At this moment, the kitten _____ happy.",
  "Yesterday, the angry elephants _____. ",
  "Right now, the students _____ lunch.  ",
  "Last weekend, my mother _____ the hallway. ",
  "At this moment, the dogs _____ inside. ",
  "Last night, my uncle _____ us. ",
  "At this moment, the flower _____ water. ",
  "Yesterday, Beth _____ a tree. ",
  "Right now, Tomas _____ happy. ",
  "Last weekend, Hiro _____ the attic. ",
  "Right now, the lake _____ peaceful. "
];

const kinds = [
  "simple_subject",
  "simple_subject",
  "simple_subject",
  "simple_subject",
  "simple_subject",
  "simple_subject",
  "simple_predicate",
  "simple_predicate",
  "simple_predicate",
  "simple_predicate",
  "simple_predicate",
  "simple_predicate"
];

const makeChoice = c => {
  return {
    choice: c,
    answer: true,
    audio: `words.${c}`,
    contentId: uuid.v4()
  };
};

const makeChoices = round => {
  // console.warn("round", sentencesWithBlank[round]);
  let arr = sentencesWithBlank[round].split("_____");
  console.warn("arr", arr[1]);
  return [
    {
      choice: arr[0].trim(),
      answer: false,
      contentId: uuid.v4()
    },
    {
      choice: answers[round],
      answer: true,
      contentId: uuid.v4()
    },
    {
      choice: arr[1].trim(),
      answer: false,
      contentId: uuid.v4()
    }
  ];
};

const getTaskConfig = contentIds => {
  return {
    type: "Typing",
    params: {
      contentId: contentIds,
      layout: [{ column: ["textBorders_spotlightText", "typing_choices"] }],
      stylePrefix: "grmmr1_typing",
      shuffleChoices: false,
      shuffleContentIds: true
    }
  };
};

export default class GrmmrTyping extends Component {
  makeSpotlightTextFromString = textString => {
    let arr = textString.split(", ");
    return arr.map(slt => ({ text: slt }));
  };

  componentDidMount = () => {
    // let slText = makeSpotlightTextFromString(spotlightText);
    let taskData = choices.map((taskChoices, i) => ({
      type: "TYPING",
      contentId: uuid.v4(),
      kind: kinds[i],
      // spotlightImage: [{ image: `grmmr1_graphic.${kinds[i]}` }],
      spotlightText: this.makeSpotlightTextFromString(spotlightText[i]),
      choices: makeChoices(i),
      audio: [
        {
          type: "prompt",
          path: `grmmr.type_the_${kinds[i]}`
        }
      ]
    }));
    let contentIds = taskData.map(({ contentId }) => contentId);
    let taskConfig = getTaskConfig(contentIds);
    this.setState({ taskData, contentIds, taskConfig });
  };

  render() {
    if (this.state && this.state.taskData) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
