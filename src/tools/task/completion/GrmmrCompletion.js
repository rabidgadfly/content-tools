import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

const spotlightTexts = [
  "My sister swims daily. ___ is very active.",
  "Tom ate breakfast. ___ was delicious.",
  "Greg watched a funny show. ___ laughed loudly.",
  "Maria plays golf. ___ can hit the ball far.",
  "The puppy chewed a sock. ___ ripped and fell apart.",
  "My helpful brother made my bed. ___ folded the sheet carefully.",
  "Isaac gave two plants to his aunt. ___ loved the gift.",
  "The angry bee stung Sofia. ___ flew away quickly.",
  "My son teaches Spanish. ___ enjoys talking with the students.",
  "The queen visited the hospital. ___ brought blankets for the patients.",
  "Some fans sent letters to the actress. ___ read each one."
];

const allChoices = [
  ["He", "She*", "It"],
  ["He", "She", "It*"],
  ["He*", "She", "It"],
  ["He", "She*", "It"],
  ["He", "She", "It*"],
  ["He*", "She", "It"],
  ["He", "She*", "It"],
  ["He", "She", "It*"],
  ["He*", "She", "It"],
  ["He", "She*", "It"],
  ["He", "She*", "It"]
];

export default class GrmmrCompletion extends Component {
  componentDidMount = () => {
    const rounds = allChoices.map((choices, i) => {
      let isAnswer = choice.indexOf("*") > -1;
      let theChoices = choices[i].map(choice => ({
        contentId: uuid.v4(),
        choice: choice.replace("*", ""),
        kind: isAnswer ? "pronouns_singular" : "0"
      }));

      return {
        type: "COMPLETION",
        contentId: uuid.v4(),
        kind: "pronouns_singular",
        dropZones: [
          {
            dropZone: "pronouns_singular"
          }
        ],
        spotlightText: this.makeSpotlightText(spotlightTexts[i]),
        choices: theChoices
      };
    });

    let contentIds = rounds.map(({ contentId }) => contentId);

    let config = {
      type: "Completion",
      params: {
        contentId: contentIds,
        useKindToEvaluate: true,
        layout: "singleSentence",
        outerChoices: true,
        stylePrefix: "clz_cmp",
        shuffleChoices: true
      }
    };

    this.setState({ rounds, config });
  };

  makeSpotlightText = slt => {
    let foo = slt.split("___");
    let result;
    foo.forEach((bar, i) => {
      if (i === 0) {
        result = [
          {
            text: bar
          },
          {
            text: "_"
          }
        ];
      } else {
        result.push({ text: bar });
      }
    });
  };

  render() {
    if (this.state && this.state.config) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.rounds))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.config))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
