import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

const definitions = {
  achieve: "to_achieve_is_succeed_doing_something",
  although: "although_signal_word_means_same_thing",
  candidate: "a_candidate_someone_who_trying_get",
  office: "an_office_important_job_often_government",
  represent: "to_represent_people_do_say_things",
  policy: "a_policy_group_rules_plans_used",
  attempt: "to_attempt_is_try_do_something",
  benefit: "a_benefit_is_good_helpful_result",
  challenging: "challenging_means_difficult",
  conclude: "to_conclude_decide_after_thinking_about",
  compete: "to_compete_work_against_others_try",
  during: "during_signal_word_means_through_period",
  however: "however_signal_word_means_same_thing",
  improve: "to_improve_make_better_become_better",
  physical: "physical_describes_activities_actions_have_do",
  commonly: "commonly_means_the_same_thing_often",
  environment: "environment_land_water_air_living_things",
  extinction: "extinction_when_entire_type_plant_animal",
  impact: "an_impact_is_a_result",
  species: "a_species_is_kind_plant_animal",
  culture: "a_culture_groups_values_way_life",
  generation: "a_generation_group_people_who_are",
  in_addition: "in_addition_phrase_means_same_thing",
  theme: "a_theme_central_message_lesson_story",
  traditional: "traditional_describes_traditions_old_ways_thinking",
  estimate: "to_estimate_guess_amount_value_using",
  habitat: "a_habitat_natural_place_where_plant",
  migration: "migration_when_many_animals_people_move",
  structure: "a_structure_something_built_putting_parts",
  therefore: "therefore_signal_word_means_same_thing",
  contribution: "a_contribution_something_done_contributed_help",
  previous: "previous_means_earlier_or_past",
  original: "original_means_new_not_like_anything",
  express: "to_express_show_feeling_idea_with",
  influence: "an_influence_is_effect_what_happens",
  destructive: "destructive_describes_someone_something_causes_damage",
  economic: "economic_means_relating_economy_specifically_buying",
  effect: "an_effect_is_result_what_happens",
  location: "a_location_specific_place_where_something",
  reduce: "to_reduce_is_make_smaller_less",
  calories: "a_calorie_unit_measuring_much_energy",
  despite: "despite_signal_word_means_even_with",
  negative: "negative_describes_something_that_is_bad",
  nutrients: "nutrients_are_substances_help_plants_animals",
  process: "to_process_change_something_one_form",
  conflict: "a_conflict_struggle_between_people_things",
  issue: "an_issue_problem_important_topic_people",
  perspective: "a_perspective_way_looking_understanding_something",
  resolution: "the_resolution_story_final_part_when",
  witness: "to_witness_an_event_see_happens",
  consequence: "a_consequence_what_happens_result_action",
  productive: "productive_means_producing_lot_getting_lot",
  progress: "progress_means_improvement_getting_closer_achieving",
  innovation: "an_innovation_new_idea_tool_way",
  technological: "technological_means_having_do_with_technology",
  accurate: "accurate_means_correct_or_precise",
  biased: "biased_means_unfairly_preferring_one_group",
  investigate: "to_investigate_try_find_out_truth",
  professional: "a_professional_someone_who_does_job",
  restrict: "to_restrict_is_to_limit_something",
};

const dropZones = [
  [
    { dropZone: "effect_syn", label: "effect" }
  ],
  [
    { dropZone: "effect_syn", label: "effect" },
    { dropZone: "reduce_syn1", label: "reduce"},
    { dropZone: "location_syn", label: "location"}
  ],
  [
    { dropZone: "destructive_syn", label: "destructive" },
    { dropZone: "economic_syn", label: "economic"},
    { dropZone: "reduce_syn2", label: "reduce"}
  ],

];

const choices = [
  [
    {kind: "effect_syn", choices: ["influence",	"impact"]}
  ],
  [
    {kind: "effect_syn", choices: ["influence",	"impact"]},
    {kind: "reduce_syn1", choices: ["decrease",	"lower"]},
    {kind: "location_syn", choices: ["setting",	"position"]}
  ],
  [
    {kind: "destructive_syn", choices: ["harmful",	"damaging"]},
    {kind: "economic_syn", choices: ["business",	"financial"]},
    {kind: "reduce_syn2", choices: ["lessen",	"diminish"]}
  ],

];

const kind = [
  "",
  "location_syn",
  "reduce_syn2",
];

const rounds = dropZones.map((dzs, i) => {
  let dzones = dzs.map(dz => ({
    dropZone: `${dz.dropZone}`,
    label: `${dz.label}`,
    audio: `avocab.${definitions[dz.label]}`,
  }));

  let theChoices = choices[i].reduce((acc,kindSet,i) => {
    let ksArray = kindSet.choices.map(choice => ({
      contentId: uuid.v4(),
      choice: choice,
      audio: `${choice.indexOf(" ") > -1 ? 'avocab' : 'words'}.${choice.replace(/ /g, "_")}`,
      kind: `${kindSet.kind}`,
      kindAudio: `words.${kindSet.kind.replace(/(_sy.+)/g,"")}`
    }));

    return acc.concat(ksArray)
  },[]);
  let regex = / /gi;
  return {
    type: "CATEGORIZE",
    contentId: uuid.v4(),
    kind: kind[i],
    dropZones: dzones,
    choices: theChoices
  };
});

let contentIds = rounds.map(({ contentId }) => contentId);

let config = {
  type: "Categorize",
  params: {
    contentId: contentIds,
    "choicePresentation": "group",
    "layout": "group",
    "dropZoneLayout": "VerticalList",
    "borderedDropZone": true,
    "stylePrefix": "catwrd2",
    "hasCategorySummary": false,
    "suppressCorrectDefault": true,
    "suppressIncorrectDefault": true,
    "choiceFormat": "text"
  }
};

export default class CombiningVocStrat extends Component {
  render() {
    return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
                id="outputData"
                rows="40"
                style={{ width: "100%" }}
                defaultValue={js_beautify(JSON.stringify(rounds))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
                id="outputConfig"
                rows="40"
                style={{ width: "100%" }}
                defaultValue={js_beautify(JSON.stringify(config))}
            />
          </div>
        </div>
    );
  }
}
