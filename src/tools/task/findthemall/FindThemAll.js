import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";

const spotlightImage = [];

const choices = [
  ["eyelid*",	"spotlight*",	"snowball*",	"escape",	"happy",	"circle"],
  ["stopwatch*",	"racetrack*",	"raincoat*",	"frighten",	"sunny",	"chocolate"],
  ["paycheck*",	"railroad*",	"haircut*",	"forest",	"building",	"second"],
  ["paycheck*",	"railway*",	"haircut*",	"forest",	"building",	"second"],
  ["earthquakes*",	"seacoast*",	"eyeglasses*",	"pencil",	"animals",	"jumping"],
];

const kinds = [
  "compound5",
  "compound6",
  "compound7",
  "compound8",
];


export default class CardMatchingVocStrat extends Component {
  componentDidMount = () => {
    const taskData = this.getTaskData(choices);
    const contentIds = taskData.map(({contentId}) => contentId);

    this.setState({
      contentIds: contentIds,
      taskData: taskData,
      taskConfig: this.getTaskConfig(contentIds)
    });
  };

  getTaskData = () => choices.map((taskChoices, i) => {
    return {
      type: "MULTISELECT",
      roundKey: `fta0${i+1}`,
      contentId: uuid.v4(),
      kind: kinds[i],
      spotlightImage: [
        {
          "image": "vocstrat1_graphic.vocstrat1_intro_compound"
        }
      ],
      choices: this.makeChoices(taskChoices)
    };
  });

  makeChoices = choices => {
    return choices.map(c => {
      const answer = c.includes("*");
      const choiceMinusToken = c.replace("*", "");
      return {
        choice: choiceMinusToken,
        answer,
        contentId: uuid.v4()
      };
    });
  };

  getTaskConfig = contentIds => {
    return ({
        type: "MultiSelect",
        params: {
          contentId: contentIds,
          layout: "findThemAll",
          stylePrefix: "grmmr2c",
          numColumns: 2,
          shuffleChoices: true,
          requiredClicks: 3,
          isFta: true
        }
      })
    };

  render() {
    if (this.state && this.state.taskData) {
      return (
          <div style={{display: "flex"}}>
            <div style={{width: "400px", padding: "10px"}}>
              <h3 style={{fontFamily: "Helvetica"}}>Data</h3>
              <textarea
                  id="outputData"
                  rows="40"
                  style={{width: "100%"}}
                  defaultValue={js_beautify(JSON.stringify(this.state.taskData))}
              />
            </div>
            <div style={{width: "400px", padding: "10px"}}>
              <h3 style={{fontFamily: "Helvetica"}}>Config</h3>
              <textarea
                  id="outputConfig"
                  rows="40"
                  style={{width: "100%"}}
                  defaultValue={js_beautify(JSON.stringify(this.state.taskConfig))}
              />
            </div>
          </div>
      );
    }
    return <div/>;
  }
}
