import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

let answerChoices = [];

let s_prompt = [
"Highlight the conjunction that tells why.",
"Highlight the conjunction that tells why.",
"Highlight the conjunction that tells why.",
"Highlight the conjunction that tells why.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that tells when.",
"Highlight the conjunction that tells when.",
"Highlight the conjunction that tells when.",
"Highlight the conjunction that tells when.",
];

let p_prompt = [
"",
"Highlight the conjunction that tells why.",
"Highlight the conjunction that tells why.",
"Highlight the conjunction that tells why.",
"Highlight the conjunction that tells why.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that means given that.",
"Highlight the conjunction that tells when.",
"Highlight the conjunction that tells when.",
"Highlight the conjunction that tells when.",
"Highlight the conjunction that tells when.",
];

const highlightText = [
"Neither the library nor the museum is open.",
"Neither the dog nor the cat is hungry.",
"They have either caramel or strawberry.",
"No sooner had Becca sat down than the film began.",
"Not only do they speak Spanish, but also they speak Hindi.",
"Serena plays both golf and rugby.",
"Neither the avocados nor the apples were ripe.",
"We will either play games or eat dinner.",
"No sooner had she gone inside than the storm ended.",
"Not only do we raise chickens, but also we keep rabbits.",
"I like both mild foods and spicy foods.",
"Neither the computer nor the television is working.",
"Either we will hike or we will swim.",
];

const clickableChoices = [
["Neither*",	"nor*"],
["Neither*",	"nor*",	"hungry"],
["either*",	"or*",	"strawberry"],
["No sooner*",	"than*",	"began"],
["Not only*",	"but", "also*	Hindi"],
["both*",	"and*",	"rugby"],
["Neither*",	"nor*",	"were"],
["either*",	"or*",	"eat"],
["No sooner*",	"than*",	"inside"],
["Not only*",	"but also*",	"rabbits"],
["both*",	"and*",	"spicy"],
["Neither*",	"nor*",	"working"],
["Either*",	"or*",	"swim"],
];

let assignments = [
  { noun: ["Emma", "she"], verb: ["won","studied"], conjunction: [], adjective: [] },
  { noun: ["mother", "I"], verb: ["took","played"], conjunction: [], adjective: [] },
  { noun: ["Prasanna", "she"], verb: ["loves","reads"], conjunction: [], adjective: [] },
  { noun: ["I", "teacher"], verb: ["completed", "gave"], conjunction: [], adjective: [] },
  {
    noun: ["banana", "Marcy"],
    verb: ["is","will eat"],
    adjective: [""],
    conjunction: []
  },
  { noun: ["Angela", "she"], verb: ["cleans","makes"], conjunction: [], adjective: [] },
  { noun: ["anyone", "watchdog"], verb: ["knocks", "barks"], conjunction: [], adjective: [] },
  { noun: ["Rosie", "she"], verb: ["disagrees","tries"], conjunction: [], adjective: [] },
  {
    noun: ["she", "Clara"],
    verb: ["completed","smiled"],
    conjunction: [],
    adjective: []
  },
  { noun: ["I", "head"], verb: ["will fall", "hits"], conjunction: [], adjective: [] },
  { noun: ["Usain", "he"], verb: ["will stretch", "finishes"], conjunction: [], adjective: [] },
  { noun: ["tourists", "they"], verb: ["felt","saw"], conjunction: [], adjective: [] },
  { noun: ["bell","students"], verb: ["rings", "line"], conjunction: [], adjective: [] }
];
let kind = [
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
"paired_conjunction",
];

const getLegendKind = kind => {
  switch (kind) {
    case "subject":
    case "simple_subject":
    case "compound_subject":
    case "pronoun":
    case "direct_object":
    case "possessive_noun":
    case "owned_noun":
    case "relative_pronoun":
      return "noun";
    case "predicate":
    case "simple_predicate":
    case "compound_predicate":
    case "verb_linking":
      return "verb";
    case "coordinating_conjunction":
    case "subordinating_conjunction":
    case "paired_conjunction":
    case "conjunction_and":
    case "conjunction_so":
    case "conjunction_but":
    case "conjunction_or":
    case "conjunction_because":
    case "conjunction_if":
    case "conjunction_as_soon_as":
      return "conjunction";
    case "relative_adverb":
      return "adverb";
    case "predicate_adjective":
      return "adjective";
    default:
      return kind;
  }
};

export default class ChainingGrammar extends Component {
  componentDidMount = () => {
    let rounds = this.getRounds(this.getChoices());
    let contentIds = rounds.map(({ contentId }) => contentId);
    let config = {
      type: "Highlighting",
      params: {
        contentId: contentIds,
        stylePrefix: "grmmr1",
        allWordsClickable: true,
        shuffleContentIds: false,
        isExample: false
      }
    };
    this.setState({ rounds, config });
  };

  getChoices = () => {
    return highlightText.map((text, i) => {
      let newAnswerChoices = [];

      const arr = text.split(" ");
      let result = arr.map(word => {
        let compareWord = word.replace(".", "");
        compareWord = compareWord.replace("!", "");
        compareWord = compareWord.replace("?", "");
        compareWord = compareWord.replace(",", "");
        compareWord = compareWord.replace("*", "");

        const isAnswer = clickableChoices[i].includes(`${compareWord}*`);
        const isClickable =
          isAnswer || clickableChoices[i].includes(compareWord);
        const theChoice = word.replace("*", "");
        let choiceStub = {
          choice: theChoice,
          contentId: uuid.v4()
        };

        if (isAnswer) {
          choiceStub.answer = true;
          newAnswerChoices.push(choiceStub);
        }
        if (isClickable) {
          choiceStub.clickable = true;
        }

        // for( var p in assignments[i] ) {
        //   assignments[i][p].forEach(n => {
        //     if (n === compareWord) {
        //       choiceStub.assignment = p;
        //     }
        //   });
        // }

        return choiceStub;
      });

      answerChoices = answerChoices.concat(newAnswerChoices);

      return result;
    });
  };

  getRounds = choices => {
    return choices.map((c, i) => {
      return {
        type: "HIGHLIGHTING",
        contentId: uuid.v4(),
        kind: kind[i],
        legend: ["conjunction"],
        // legend: ["noun", "verb", "adjective", "adverb", "conjunction"],
        choices: choices[i],
        // audio: [
        //   {
        //     type: "prompt",
        //     path: `common_grmmr.${filenameGenerator(s_prompt[i])}`
        //   },
          // {
          //   type: "practice_prompt",
          //   path: `common_grmmr.${filenameGenerator(p_prompt[i])}`
          // }
        // ]
      };
    });
  };

  render() {
    if (this.state && this.state.rounds) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.rounds))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.config))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
