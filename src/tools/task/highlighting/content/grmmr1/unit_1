import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

let answerChoices = [];

const kind = [
  "",
  "",
  "noun",
  "verb",
  "noun",
  "verb",
  "noun",
  "verb",
  "noun",
  "verb",
  "noun",
  "verb",
  "noun",
  "verb"
];

// const nouns = [
//   ["Lukas", "he"],
//   ["I", "I"],
//   ["She", "she"],
//   ["ducks", "frogs"],
//   ["I", "I"],
//   ["clock", "I"],
//   ["Clem", "she"],
//   ["Evelyn", "Bryce"],
//   ["dog", "he"],
//   ["He", "he"],
//   ["We", "we"],
//   ["We", "we"],
//   ["artist", "she"]
// ];
//
// const verbs = [
//   ["runs", "swims"],
//   ["made", "ate"],
//   ["plays", "swims"],
//   ["waddle", "hop"],
//   ["like", "grow"],
//   ["broke", "am"],
//   ["won", "feels"],
//   ["is", "is"],
//   ["is", "is"],
//   ["likes", "dislikes"],
//   ["played", "ate"],
//   ["will walk", "will run"],
//   ["will draw", "will paint"]
// ];

const highlightText = [
  "A brown mouse sleeps peacefully.",
  "A brown mouse sleeps peacefully.",
  "The tired elephant walked slowly.",
  "The tired elephant walked slowly.",
  "A small, ripe apple dropped.",
  "A small, ripe apple dropped.",
  "A few snowflakes fall softly.",
  "A few snowflakes fall softly.",
  "A kind student speaks politely.",
  "A kind student speaks politely.",
  "The tall, athletic players jogged.",
  "The tall, athletic players jogged.",
  "The leaky pen writes sloppily.",
  "The leaky pen writes sloppily."
];

const clickableChoices = [
  ["mouse*"],
  ["sleeps*"],
  ["elephant*", "walked", "slowly"],
  ["tired", "walked*", "slowly"],
  ["ripe", "apple*", "dropped"],
  ["small", "ripe", "dropped*"],
  ["snowflakes*", "fall", "softly"],
  ["few", "fall*", "softly"],
  ["kind", "student*", "speaks"],
  ["kind", "speaks*", "politely"],
  ["athletic", "players*", "jogged"],
  ["tall", "athletic", "jogged*"],
  ["pen*", "writes", "sloppily"],
  ["leaky", "writes*", "sloppily"]
];

const getLegendKind = kind => {
  switch (kind) {
    case "subject":
    case "simple_subject":
    case "compound_subject":
    case "pronoun":
    case "direct_object":
    case "possessive_noun":
    case "owned_noun":
    case "relative_pronoun":
      return "noun";
    case "predicate":
    case "simple_predicate":
    case "compound_predicate":
    case "verb_linking":
      return "verb";
    case "coordinating_conjunction":
    case "subordinating_conjunction":
    case "paired_conjunction":
    case "conjunction_and":
    case "conjunction_so":
    case "conjunction_but":
    case "conjunction_or":
      return "conjunction";
    case "relative_adverb":
      return "adverb";
    case "predicate_adjective":
      return "adjective";
    default:
      return kind;
  }
};

let s_prompt = [
  "",
  "",
  "Highlight the noun.",
  "Highlight the verb.",
  "Highlight the noun.",
  "Highlight the verb.",
  "Highlight the noun.",
  "Highlight the verb.",
  "Highlight the noun.",
  "Highlight the verb.",
  "Highlight the noun.",
  "Highlight the verb.",
  "Highlight the noun.",
  "Highlight the verb."
];

let words = [
  "",
  "",
  "Elephant",
  "Walked",
  "Apple",
  "Dropped",
  "Snowflakes",
  "Fall",
  "Student",
  "Speaks",
  "Players",
  "Jogged",
  "Pen",
  "Writes"
];

export default class ChainingGrammar extends Component {
  componentDidMount = () => {
    let rounds = this.getRounds(this.getChoices());
    let contentIds = rounds.map(({ contentId }) => contentId);
    let config = {
      type: "Highlighting",
      params: {
        contentId: contentIds,
        stylePrefix: "grmmr1",
        allWordsClickable: true,
        shuffleContentIds: false,
        isExample: false
      }
    };
    this.setState({ rounds, config });
  };

  getChoices = () => {
    return highlightText.map((text, i) => {
      let newAnswerChoices = [];
      const sameAsLastRound = i > 0 && text == highlightText[i - 1];
      const arr = text.split(" ");
      let result = arr.map(word => {
        let compareWord = word.replace(".", "");
        compareWord = compareWord.replace("!", "");
        compareWord = compareWord.replace("?", "");
        compareWord = compareWord.replace(",", "");
        // compareWord = compareWord.replace("*", "");

        const isAnswer = clickableChoices[i].includes(`${compareWord}*`);
        const isClickable =
          isAnswer || clickableChoices[i].includes(compareWord);
        const theChoice = word.replace("*", "");
        const choiceStub = {
          choice: theChoice,
          contentId: uuid.v4()
        };

        if (isAnswer) {
          choiceStub.answer = true;
          newAnswerChoices.push(choiceStub);
        }
        if (isClickable) {
          choiceStub.clickable = true;
        }
        // nouns[i].forEach(n => {
        //   if (theChoice.toLowerCase() === n.toLowerCase()) {
        //     choiceStub.assignment = "noun";
        //   }
        // });
        // verbs[i].forEach(n => {
        //   if (theChoice.toLowerCase() === n.toLowerCase()) {
        //     choiceStub.assignment = "verb";
        //   }
        // });
        return choiceStub;
      });

      if (sameAsLastRound) {
        answerChoices.forEach(ac => {
          let match = result.find(c => c.choice === ac.choice);
          match.assignment = getLegendKind(kind[i - 1]);
          match.clickable = false;
        });
      } else {
        answerChoices = [];
      }

      answerChoices = answerChoices.concat(newAnswerChoices);

      return result;
    });
  };

  getRounds = choices => {
    return choices.map((c, i) => {
      return {
        type: "HIGHLIGHTING",
        contentId: uuid.v4(),
        kind: kind[i],
        legend: ["noun", "verb"],
        choices: choices[i],
        audio: [
          {
            type: "prompt",
            path: s_prompt[i] ? `grmmr.${filenameGenerator(s_prompt[i])}` : ""
          },
          {
            type: "word",
            path: `words.${words[i].toLowerCase()}`
          },
          {
            type: "pos",
            path: `words.${kind[i].toLowerCase()}`
          }
        ]
      };
    });
  };

  render() {
    if (this.state && this.state.rounds) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.rounds))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.config))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
