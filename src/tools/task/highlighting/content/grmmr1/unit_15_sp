import React, { Component } from "react";
import uuid from "uuid";
import js_beautify from "js-beautify";
import { filenameGenerator } from "../../../util/filenameGenerator";

let answerChoices = [];

const kind = [
  "compound_conjunction",
  "compound_subject",
  "compound_predicate",
  "compound_conjunction",
  "compound_subject",
  "compound_predicate",
  "compound_conjunction",
  "compound_subject",
  "compound_predicate",
  "compound_conjunction",
  "compound_subject",
  "compound_predicate",
  "compound_conjunction",
  "compound_subject",
  "compound_predicate"
];

const highlightText = [
  "The old dog played quietly, and she slept soundly.",
  "The old dog played quietly, and she slept soundly.",
  "The old dog played quietly, and she slept soundly.",
  "The young girl ran through the woods, and her brother ran behind.",
  "The young girl ran through the woods, and her brother ran behind.",
  "The young girl ran through the woods, and her brother ran behind.",
  "The new window opens easily, but it closes tightly.",
  "The new window opens easily, but it closes tightly.",
  "The new window opens easily, but it closes tightly.",
  "Those people cook often, or they eat out.",
  "Those people cook often, or they eat out.",
  "Those people cook often, or they eat out.",
  "That glass tipped, so the sweet juice spilled everywhere.",
  "That glass tipped, so the sweet juice spilled everywhere.",
  "That glass tipped, so the sweet juice spilled everywhere."
];

const clickableChoices = [
  ["and*"],
  ["dog*", "she*"],
  ["played*", "slept*"],
  ["through", "ran", "and*"],
  ["girl*", "woods", "brother*"],
  ["ran*", "ran*", "behind"],
  ["The", "but*", "it"],
  ["window*", "easily", "it*"],
  ["opens*", "closes*", "tightly"],
  ["Those", "often", "or*"],
  ["people*", "cook", "they*"],
  ["cook*", "eat*", "out"],
  ["That", "so*", "the"],
  ["glass*", "juice*", "everywhere"],
  ["tipped*", "spilled*", "everywhere"]
];

const getLegendKind = kind => {
  switch (kind) {
    case "subject":
    case "simple_subject":
    case "compound_subject":
    case "pronoun":
    case "direct_object":
    case "possessive_noun":
    case "owned_noun":
    case "relative_pronoun":
      return "noun";
    case "predicate":
    case "simple_predicate":
    case "compound_predicate":
    case "verb_linking":
      return "verb";
    case "coordinating_conjunction":
    case "subordinating_conjunction":
    case "paired_conjunction":
      return "conjunction";
    case "relative_adverb":
      return "adverb";
    case "predicate_adjective":
      return "adjective";
    default:
      return kind;
  }
};

let s_prompt = [
  "Conjunctions connect ideas in a sentence. Highlight the conjunction.",
  "The simple subject tells who or what is doing the action. Highlight the TWO simple subjects.",
  "The simple predicate tells what the action is. Highlight the TWO simple predicates.",
  "Conjunctions connect ideas in a sentence. Highlight the conjunction.",
  "The simple subject tells who or what is doing the action. Highlight the TWO simple subjects.",
  "The simple predicate tells what the action is. Highlight the TWO simple predicates.",
  "Conjunctions connect ideas in a sentence. Highlight the conjunction.",
  "The simple subject tells who or what is doing the action. Highlight the TWO simple subjects.",
  "The simple predicate tells what the action is. Highlight the TWO simple predicates.",
  "Conjunctions connect ideas in a sentence. Highlight the conjunction.",
  "The simple subject tells who or what is doing the action. Highlight the TWO simple subjects.",
  "The simple predicate tells what the action is. Highlight the TWO simple predicates.",
  "Conjunctions connect ideas in a sentence. Highlight the conjunction.",
  "The simple subject tells who or what is doing the action. Highlight the TWO simple subjects.",
  "The simple predicate tells what the action is. Highlight the TWO simple predicates."
];

let p_prompt = [
  "the conjunction.",
  "the TWO simple subjects.",
  "the TWO simple predicates.",
  "the conjunction.",
  "the TWO simple subjects.",
  "the TWO simple predicates.",
  "the conjunction.",
  "the TWO simple subjects.",
  "the TWO simple predicates.",
  "the conjunction.",
  "the TWO simple subjects.",
  "the TWO simple predicates.",
  "the conjunction.",
  "the TWO simple subjects.",
  "the TWO simple predicates."
];

export default class ChainingGrammar extends Component {
  componentDidMount = () => {
    let rounds = this.getRounds(this.getChoices());
    let contentIds = rounds.map(({ contentId }) => contentId);
    let config = {
      type: "Highlighting",
      params: {
        contentId: contentIds,
        stylePrefix: "grmmr1",
        allWordsClickable: true,
        shuffleContentIds: false,
        isExample: false
      }
    };
    this.setState({ rounds, config });
  };

  getChoices = () => {
    return highlightText.map((text, i) => {
      let newAnswerChoices = [];
      const sameAsLastRound = i > 0 && text == highlightText[i - 1];
      const arr = text.split(" ");
      let result = arr.map(word => {
        let compareWord = word.replace(".", "");
        compareWord = compareWord.replace("!", "");
        compareWord = compareWord.replace("?", "");
        compareWord = compareWord.replace(",", "");
        // compareWord = compareWord.replace("*", "");

        const isAnswer = clickableChoices[i].includes(`${compareWord}*`);
        const isClickable =
          isAnswer || clickableChoices[i].includes(compareWord);
        const choiceStub = {
          choice: word.replace("*", ""),
          contentId: uuid.v4()
        };

        if (isAnswer) {
          choiceStub.answer = true;
          newAnswerChoices.push(choiceStub);
        }
        if (isClickable) {
          choiceStub.clickable = true;
        }
        return choiceStub;
      });

      if (sameAsLastRound) {
        answerChoices.forEach(ac => {
          let match = result.find(c => c.choice === ac.choice);
          match.assignment = getLegendKind(kind[i - 1]);
          match.clickable = false;
        });
      } else {
        answerChoices = [];
      }

      answerChoices = answerChoices.concat(newAnswerChoices);

      return result;
    });
  };

  getRounds = choices => {
    return choices.map((c, i) => {
      return {
        type: "HIGHLIGHTING",
        contentId: uuid.v4(),
        kind: kind[i],
        legend: ["noun", "verb", "conjunction"],
        choices: choices[i],
        audio: [
          {
            type: "practiceQuestion",
            path: s_prompt[i] ? `grmmr.${filenameGenerator(s_prompt[i])}` : ""
          },
          {
            type: "standardQuestion",
            path: p_prompt[i] ? `grmmr.${filenameGenerator(p_prompt[i])}` : ""
          }
        ]
      };
    });
  };

  render() {
    if (this.state && this.state.rounds) {
      return (
        <div style={{ display: "flex" }}>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Data</h3>
            <textarea
              id="outputData"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.rounds))}
            />
          </div>
          <div style={{ width: "400px", padding: "10px" }}>
            <h3 style={{ fontFamily: "Helvetica" }}>Config</h3>
            <textarea
              id="outputConfig"
              rows="40"
              style={{ width: "100%" }}
              defaultValue={js_beautify(JSON.stringify(this.state.config))}
            />
          </div>
        </div>
      );
    }
    return <div />;
  }
}
