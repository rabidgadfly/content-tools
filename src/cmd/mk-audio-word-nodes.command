# Creates audio nodes for words
arr=("eyelid"
"spotlight"
"stopwatch"
"racetrack"
"paycheck"
"railroad"
"railway"
"earthquakes"
"seacoast") ;
for i in "${arr[@]}" ;
	do
		echo "{
    \"key\": \"${i}\",
    \"path\": \"${i:0:1}/${i}.mp3\",
    \"transcript\": \"${i}\"
},"
done
