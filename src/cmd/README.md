## Bash command tools

A place to collect bash command scripts that help in the creation of content.
See mk-audio.command for examples of frequently used actions.
