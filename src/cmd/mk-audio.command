# Creates MP3 audio files for each item in array.
#
# ============================================================================
# NOTE: lame is required for mp3 generation.
# Install using brew:
# brew install ffmpeg --with-libvpx --with-libvorbis --with-fdk-aacc
# More info: https://blog.lerk.io/how-to-convert-aiff-to-mp3-on-command-line/
# ============================================================================
#
# - Make sure lame is installed (see note above)
# - Enter audio transcripts in the transcripts array (1)
# - Enter matching filenames in the filenames array. (1)
# - Set isDirectorySorted if necessary (2)
# - Open a terminal and cd to this directory [cd /"/content-tools/src/cmd]
# - Execute command [ bash mk-audio.command ]
#   * If you have permissions issues run [ chmod -x mk-audio.command ]
#
# MP3 files will be created in "content-tools/src/cmd/temp" and objects
# will echo to the terminal window.
#
# (1) CreateAudioNodes creates the filename and transcript arrays for you!
#
# (2) If isDirectorySorted is set to true, audio paths will include a
# directory with the first letter of the filename.
#

filenames=("eloise_greenfield_among_millions_african_americans"
)

transcripts=("Eloise Greenfield was among millions of African Americans who were part of the Great Migration - the journey from the southern United States for better opportunities in northern cities. Her poem about this experience is divided into sections, each narrated by a different speaker with a different point of view."
)


# Set to true if audio is sorted into folders (words, for example)
isDirectorySorted=true;

# create temp directory if needed and switch to it
if [ -d "temp" ] ; then
    cd temp
else
    mkdir temp
    cd temp
fi

# Create aiff audio files
for i in ${!filenames[*]} ; do
    say -v Samantha ${transcripts[i]} -o ${filenames[i]}.aiff;
done

# Convert aiff to mp3
for i in "${filenames[@]}" ; do
    ffmpeg -i ${i}.aiff -f mp3 -acodec libmp3lame -ab 320000 -ar 44100 ${i}.mp3
done

# Delete aiff files
for i in "${filenames[@]}" ; do
    rm ${i}.aiff;
done

# Echo audio nodes
if [ "$isDirectorySorted" = true ] ; then
    for i in ${!filenames[*]}; do
	echo "{
        \"key\": \"${filenames[i]}\",
        \"path\": \"${filenames[i]:0:1}/${filenames[i]}.mp3\",
        \"transcript\": \"${filenames[i]}\"
    },"
    done
else
    for i in ${!filenames[*]}; do
	echo "{
        \"key\": \"${filenames[$i]}\",
        \"path\": \"${filenames[$i]}.mp3\",
        \"transcript\": \"${transcripts[$i]}\"
    },"
    done
fi
