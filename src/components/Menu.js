import React, { Component } from "react";
import { Link } from "react-router-dom";
import { menuLinks } from "./menuLinks";

const linkStyle = {
  color: "#666",
  fontFamily: "Helvetica",
  fontSize: "14px",
  textDecoration: "none"
};

const sectionTitleStyle = {
  color: "darkcyan",
  fontFamily: "Helvetica",
  fontSize: "16px",
  fontWeight: "bold",
  textDecoration: "none",
  marginTop: "15px",
  marginBottom: "2px"
};

const ulStyle = {
  listStyleType: "none",
  padding: 0,
  marginTop: "2px",
  marginBottom: "2px"
};

export default class Menu extends Component {
  state = { hovered: "", activeLink: "" };

  getMenuLink = (to, text) => {
    let style =
      this.state.activeLink === to
        ? Object.assign({}, linkStyle, { fontWeight: "bold" })
        : this.state.hovered === to
        ? Object.assign({}, linkStyle, { color: "orange" })
        : linkStyle;

    return (
      <Link
        key={to}
        onMouseOver={() => {
          this.setState({ hovered: to });
        }}
        onMouseOut={() => {
          this.setState({ hovered: "" });
        }}
        onClick={() => {
          this.setState({ activeLink: to });
        }}
        style={style}
        to={to}
      >
        {text}
      </Link>
    );
  };

  getLinkStyle = to => {
    return this.state.hovered === to
      ? Object.assign({}, linkStyle, { color: "orange" })
      : linkStyle;
  };

  render() {
    return (
      <div
        key="sidemenu"
        style={{
          padding: "10px",
          width: "200px",
          background: "#f0f0f0"
        }}
      >
        <Link to="/">Home</Link>

        {menuLinks.map(menuLink => {
          return (
            <div key={`${menuLink.section}`}>
              <p key={`${menuLink.section}_title`} style={sectionTitleStyle}>
                {menuLink.section}
              </p>
              <ul key={`${menuLink.section}_links`} style={ulStyle}>
                {menuLink.links.map(({ to, text }) => {
                  return (
                    <li key={`${to}_link`}>{this.getMenuLink(to, text)}</li>
                  );
                })}
              </ul>
            </div>
          );
        })}
      </div>
    );
  }
}
