/**
 * To add your own tool to the sidebar, first add an entry in /routes.js.
 * Once you have a route, add an entry in the appropriate section below
 * using the "link" you created in routes as the "to" in the object below.
 * The "text" will be the link text in the menu.
 **/

export const menuLinks = [
  {
    section: "Task Tools",
    links: [
      { to: "/categorize-group", text: "Categorize (Group)" },
      { to: "/chn-Grammar", text: "Chaining (grmmr)" },
      { to: "/cmb-vocstrat", text: "Combining (vs)" },
      { to: "/div-division", text: "Division" },
      { to: "/hl-grammar", text: "Highlighting (grmmr)" },
      { to: "/mc-grmmr1", text: "MC QA (grmmr)" },
      { to: "/mc-sorting", text: "MC Sorting (grmmr)" },
      { to: "/txtcnnct2-mc-qa", text: "MC Questions (txtcnnct2)" },
      { to: "/txtcnnct2-go", text: "Graphic Org (txtcnnct2)" },
      { to: "/cm-vocstrat", text: "CardMatch (vocstrat)" },
      { to: "/thumbs-vocstrat", text: "Thumbs (vocstrat)" },
      { to: "/thumbs-avocab", text: "Thumbs (avocab)" },
      { to: "/typing_grmmr1", text: "Typing (grmmr)" },
      { to: "/fta", text: "FindThemAll (new)" }
    ]
  },
  {
    section: "Audio Tools",
    links: [
      { to: "/alpha-audio-nodes", text: "Alpha audio nodes" },
      { to: "/create-audio-nodes", text: "Create audio nodes" }
    ]
  },
  {
    section: "Other Tools",
    links: [{ to: "/create-bash-audio", text: "Create Bash audio" }]
  }
];
