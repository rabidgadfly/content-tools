import { filenameGenerator } from "./filenameGenerator";

export const makeSpotlightText = (textArr, audioKey) => {
  audioKey = audioKey ? audioKey + "." : "";
  return textArr.map(t => {
    const slt = { text: t };
    if (audioKey) {
      slt.audio = `${audioKey}${filenameGenerator(t)}`;
    }
    return [slt];
  });
};
