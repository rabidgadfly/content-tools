/**
 * Converts javascript array into a bash array
 **/
export const arrToBash = (arr, varName = "bashArray") => {
  const result = arr.reduce((acc, nm) => `${acc}"${nm}"\n`, `${varName}=(`);
  return result + ")";
};

/**
 * Converts javascript filename and transcript object arrays into
 * arrays that can be iterated over in a bash script.
 **/
export const audioNodeToBashArray = (nodes, nodeProp, assignmentName) => {
  if (!assignmentName) {
    assignmentName = nodeProp;
  }
  let propArr = nodes.map(n => (n.hasOwnProperty(nodeProp) ? n[nodeProp] : ""));
  return arrToBash(propArr, assignmentName);
};

/**
 * Converts javascript filename and transcript arrays into
 * arrays that can be iterated over in a bash script.
 * For example, these arrays may be used in /cmd/mk-audio.command
 **/
export const audioNodeToBashArrays = nodes => {
  const filenames = audioNodeToBashArray(nodes, "key", "filenames");
  const transcripts = audioNodeToBashArray(nodes, "transcript", "transcripts");
  return `${filenames}\n\n${transcripts}`;
};
