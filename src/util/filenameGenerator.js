export const filenameGenerator = (text, extension, options) => {
  if (!text) {
    return "";
  }

  let opt = options || { maxWords: 6, maxChars: 128 };
  let result = text;

  // DEFAULT STOP WORDS
  if (!opt.stopWords) {
    opt.stopWords = [
      "i",
      "a",
      "an",
      "and",
      "as",
      "at",
      "be",
      "by",
      "for",
      "from",
      "how",
      "in",
      "is",
      "it",
      "of",
      "on",
      "or",
      "that",
      "the",
      "this",
      "to",
      "was"
    ];
  }

  // DEFAULT DELIMITER
  if (opt.delimiter === undefined) {
    opt.delimiter = "_";
  }

  // DEFAULT MAX CHARS
  if (!opt.maxChars) {
    opt.maxChars = 0;
  }

  // DEFAULT MAX WORDS
  if (!opt.maxWords) {
    opt.maxWords = 0;
  }

  // DEFAULT TRUNCATE WORDS
  if (!opt.maxWordLength) {
    opt.maxWordLength = 0;
  }

  // DEFAULT STATIC DELIMITER
  if (!opt.staticFileDelimiter) {
    opt.staticFileDelimiter = "=";
  }

  // LOWER CASE
  result = result.toLowerCase();

  // CHECK FOR STATIC NAME
  if (result.indexOf(opt.staticFileDelimiter) > -1) {
    result = result.substr(0, result.indexOf(opt.staticFileDelimiter));
    result = result.trim();
    if (result.lastIndexOf(extension) !== result.length - extension.length) {
      result = trimPeriods(result) + "." + trimPeriods(extension);
    }
    return result;
  }

  // REMOVE INVALID CHARACTERS
  result = result.replace(
    /\.|<|>|:|\/|\\|\||\*|\?|'|’|%|@|\(|\)|\[|]|\{|}|…|,|“|”/g,
    ""
  );

  // WORD LIST
  let words = result.match(/\w+/g);

  if (opt.maxWordLength > 0) {
    words = words.map(item => {
      return item.substring(0, opt.maxWordLength);
    });
  }

  // limit to max words
  if (opt.maxWords > 0 && words.length > opt.maxWords) {
    // limit words
    let isStopWordsMap = words.map(
      (item, index) => opt.stopWords.indexOf(item) === -1 || index === 0
    );
    let standardWordCount = isStopWordsMap.reduce((prev, current) =>
      current ? prev + 1 : prev
    );
    if (standardWordCount >= opt.maxWords) {
      // use only standards words
      words = words.filter((item, index) => isStopWordsMap[index]);
    } else {
      // use mix of standard and stop words
      let count = opt.maxWords - standardWordCount;
      words = words.filter((item, index) => {
        if (isStopWordsMap[index]) {
          return true;
        } else {
          count--;
          return count > -1;
        }
      });
    }

    words = words.filter((item, index) => opt.maxWords > index);
  }

  // limit to max chars by word
  if (opt.maxChars > 0 && words.join(opt.delimiter).length > opt.maxChars) {
    let filterLength = 0;
    words = words.filter(item => {
      filterLength += item.length + 1;
      return filterLength <= opt.maxChars;
    });
  }

  result = words.join(opt.delimiter);

  // APPEND EXTENSION
  if (extension) {
    extension = extension.toLowerCase();
    result = trimPeriods(result) + "." + trimPeriods(extension);
  }

  return result;
};

const trimPeriods = str => {
  return str.replace(/^\.+|\.+$/g, "");
};
