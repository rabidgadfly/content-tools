/**
 * Once you've created a tool you can make it accessible by adding
 * a route for it here. To make it appear in the side menu, add the path to
 * /components/menuLinks.js
 **/

import React from "react";
import CategorizeGroup from "./tools/task/categorize/CategorizeGroup";
import ChainingGrammar from "./tools/task/chaining/ChainingGrammar";
import CombiningVocStrat from "./tools/task/combining/CombiningVocStrat";
import ThumbsVocStrat from "./tools/task/multiplechoice/ThumbsVocStrat";
import ThumbsAvocab from "./tools/task/multiplechoice/ThumbsAvocab";
import Division from "./tools/task/division/Division";
import GrmmrTyping from "./tools/task/typing/GrmmrTyping";
import CreateAudioNodes from "./tools/audio/CreateAudioNodes";
import AlphaAudioNodes from "./tools/audio/AlphaAudioNodes";
import CreateAudioNodeArrays from "./tools/bash/CreateAudioNodeArrays";
import GrmmrMCqa from "./tools/task/multiplechoice/GrmmrMCqa";
import Txtcnnct2MCQuestions from "./tools/task/multiplechoice/Txtcnnct2MCQuestions";
import GrmmrSortingBoxes from "./tools/task/multiplechoice/GrmmrSortingBoxes";
import Txtcnnct2GraphicOrg from "./tools/task/graphicorg/Txtcnnct2GraphicOrg";
import HighlightingGrammar from "./tools/task/highlighting/HighlightingGrammar";
import FindThemAll from "./tools/task/findthemall/FindThemAll";
import VocStratCardMatching from "./tools/task/multiselect/VocStratCardMatching";

export const routes = [
  {
    path: "/",
    exact: true,
    main: () => (
      <div>
        <h2>Content Tools</h2>
        <p>Select a tool from the menu</p>
      </div>
    )
  },
  {
    path: "/categorize-group",
    main: () => <CategorizeGroup />
  },
  {
    path: "/chn-grammar",
    main: () => <ChainingGrammar />
  },
  {
    path: "/cmb-vocstrat",
    main: () => <CombiningVocStrat />
  },
  {
    path: "/thumbs-avocab",
    main: () => <ThumbsAvocab />
  },
  {
    path: "/cm-vocstrat",
    main: () => <VocStratCardMatching />
  },
  {
    path: "/thumbs-vocstrat",
    main: () => <ThumbsVocStrat />
  },
  {
    path: "/div-division",
    main: () => <Division />
  },
  {
    path: "/mc-grmmr1",
    main: () => <GrmmrMCqa />
  },
  {
    path: "/mc-sorting",
    main: () => <GrmmrSortingBoxes />
  },
  {
    path: "/hl-grammar",
    main: () => <HighlightingGrammar />
  },
  {
    path: "/txtcnnct2-mc-qa",
    main: () => <Txtcnnct2MCQuestions />
  },
  {
    path: "/txtcnnct2-go",
    main: () => <Txtcnnct2GraphicOrg />
  },
  {
    path: "/typing_grmmr1",
    main: () => <GrmmrTyping />
  },
  {
    path: "/fta",
    main: () => <FindThemAll />
  },
  {
    path: "/create-audio-nodes",
    main: () => <CreateAudioNodes />
  },
  {
    path: "/alpha-audio-nodes",
    main: () => <AlphaAudioNodes />
  },
  {
    path: "/create-bash-audio",
    main: () => <CreateAudioNodeArrays />
  }
];
