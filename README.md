## Content tools

A simple collection of tools to make creating content easier.

## How to add a tool

### `Create your tool in the appropriate '/tools' subdirectory.`

### `Add an entry for your tool to /routes.js`

### `Add an entry for your tool to /components/menuLinks.js`

## How to use tools

This is simply a collection of javascript scripts. Most are intended
to be executed by clicking their link from the menu. All data is defined and
manipulated within the code. Visit the menu link to execute the code.

## Structure

Tools are currently separated into three categories: content, audio and other.
Content tools generate unit content, audio tools generate audio, and other
is whatever doesn't fit in the other two categories. The tools reside in ###/tools

## Bash commands

The tools collection includes a bash command that generates MP3s and audio nodes
for asset files. It is not accessible through the menu because it needs to be
executed via the terminal. See ###/cmd/mk-audio.command for more detailed usage information.

## Start the app

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Other

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
